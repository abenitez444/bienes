@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12">
              <center><h4><b>Datos de los Tipos de los Componentes de los Bienes Muebles del Órgano o Ente</b></h4></center>
            </div>
        </div>
    <hr>
        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>  

        <div id="tabla" class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="overflow-x:auto;">
        
         <table id="tablaT1" class="table-striped dt-responsive nowrap">

                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código</td>
                       <td id="letrasb" class="text-center">Denominación del Componente</td>
                       <td id="letrasb" class="text-center">Código del Bien mueble</td>
                       <td id="letrasb" class="text-center">Fecha y Hora del Registro</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>
            <tbody>
          
               @foreach($verT7 as $im)
                  <tr> 
                        <td class="text-center"><a href="#" hidden>{{$im->id}}</a><a href="seleccionComponentes/{{$im->id}}">{{$im->codigo}}</a></td>
                
                      <td class="text-center">{{$im->denComponente}}</td>
                      <td class="text-center">{{$im->codBienMueble}}</td>
                      <td class="text-center">{{$im->created_at->format('d/m/Y - h:i A')}}</td>
                      <td class="text-center"><a href="seleccionComponentes/{{$im->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
               @endforeach
           </tbody>
        </table>
    </div>
</div>

@endsection