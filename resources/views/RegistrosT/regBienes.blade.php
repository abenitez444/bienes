@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12 separar">
                <center><h4><b>Datos de los Bienes Muebles del Órgano o Ente</b></h4></center>
                <center><h5>• Maquinarias y demás equipos de construcción, campo, industria y taller...<br></center>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
        
        <table id="tablaT1" class="table-striped table-bordered table-hover" >
              
                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código Origen Bien</td>
                       <td id="letrasb" class="text-center">Código según Catalogo</td>
                       <td id="letrasb" class="text-center">Dependencia Administrativa</td>
                       <td id="letrasb" class="text-center">Sede del Órgano </td>
                       <td id="letrasb" class="text-center">Código Responsable Administrativo</td>
                       <td id="letrasb" class="text-center">Código Responsable Bien </td>
                       <td id="letrasb" class="text-center">Código Interno Bien</td>
                       <td id="letrasb" class="text-center">Estatus del uso Bien</td>
                       <td id="letrasb" class="text-center">Fecha Ingreso</td>
                       <!--<td id="letrasb" class="text-center">Fecha Registro</td>-->
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>

            <tbody>
          
               @foreach($verT8 as $reg8)
                  
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg8->id}}</a><a href="seleccionBienes/{{$reg8->id}}"> {{$reg8->codOt2_1}}</a> </td>
                        
                        @if($reg8->codCata == '1')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$reg8->selectCatalogo->codigo}}</td>
                        @endif
                        
                        <td class="text-center">{{$reg8->selectUnidadbienes->codUnidad}}</td>

                        <td class="text-center">{{$reg8->selectOrgano->codSede}}</td>

                        <td class="text-center">{{$reg8->selectResponsable->codResp}}</td>

                        <td class="text-center">{{$reg8->ResponsableDirecto->codResp}}</td>

                        @if($reg8->codInterno == '1')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$reg8->codInterno}}</td>
                        @endif

                        <td class="text-center">{{$reg8->selectEstatus->opcion}}</td>

                        @if($reg8->feIngBien == '1111-11-11')
                        <td class="text-center">11111111</td>
                        @else
                        <td class="text-center">{{$reg8->feIngBien}}</td>
                        @endif

                        <td class="text-center"><a href="seleccionBienes/{{$reg8->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
              @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
