@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">

      <div class="row separar">
          <div class="col-md-12">
              <center><h4><b>Datos de los Orígenes (Formas de Adquisición) de los Bienes Muebles e Inmuebles del Órgano o Ente</b></h4></center>
              <center><h5>E) Aplicable solo para la forma de adquisición de Donación.</h5></center>
          </div>
      </div> 

    <hr>
        
        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
    
        <table id="tablaT1" class="tabla table-striped table-responsive table-bordered table-hover">
              
                <thead style="font-size:12px;">
                    <tr>
                       <td id="letrasb" class="text-center">Código Origen</td>
                       <td id="letrasb" class="text-center">Nombre Donante</td>
                       <td id="letrasb" class="text-center">Nombre Beneficiario</td>
                       <td id="letrasb" class="text-center">Número Contrato o Acta</td>
                       <td id="letrasb" class="text-center">Nombre Registro o Notaría</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>

            <tbody style="font-size:12px;">
          
                <!--SI EL revisadot1 ES 0 EL REGISTRO ES NUEVO SI NO , EL REGISTRO SE ABRIO-->
        @foreach($verT24 as $reg24)

                @if($reg24->codOt2_4 == '') 
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg24->id}}</a><a href="seleccionDonacion/{{$reg24->id}}"> E-1</a> </td>
                @else
                        <td class="text-center"><a href="#" hidden>{{$reg24->id}}</a><a href="seleccionDonacion/{{$reg24->id}}">{{$reg24->codOt2_4}}</a></td>
                @endif
                      
                       @if($reg24->nomDona == '1')
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center">{{$reg24->nomDona}}</td>
                       @endif

                       @if($reg24->nomBen == '1')
                        <td class="text-center">noaplica</td>
                       @else
                        <td class="text-center">{{$reg24->nomBen}}</td>
                       @endif

                       @if($reg24->numConac == '0')
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center">{{$reg24->numConac}}</td>
                       @endif

                       @if($reg24->nomRegn == '1')
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center">{{$reg24->nomRegn}}</td>
                       @endif

                        <td class="text-center"><a href="seleccionDonacion/{{$reg24->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
              @endforeach
           </tbody>
        </table>
    </div>
</div>
@endsection
