@extends('layouts.app')

@section('content')

  <div class="row">
     <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
           <div class="col-md-12">
              <center><h4><b>Datos de los Orígenes (Formas De Adquisición) de Los Bienes Muebles e Inmuebles del Órgano o Ente</b></h4></center>
              <center><h5>C) Aplicable solo para las formas de adquisición de confiscación.</h5></center>
           </div>
        </div> 

    
    <div class="row">
        <div class="col-md-12 desvanecer">
        @if(session()->has('msj'))
            <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
               @endif

               @if(session()->has('errormsj'))
            <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
        @endif
        </div>
    </div>    
 
        <div id="tabla" class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="overflow-x:auto;">
        
        <table id="tablaT1" class="table-striped dt-responsive nowrap">

                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código Origen</td>
                       <td id="letrasb" class="text-center">Nombre propietario anterior</td>
                       <td id="letrasb" class="text-center">Nombre Beneficiario</td>
                       <td id="letrasb" class="text-center">Nombre Autoridad</td>
                       <td id="letrasb" class="text-center">Número Setencia Confiscación</td>
                       <td id="letrasb" class="text-center">Nombre Registro Notaría</td>
                       <td id="letrasb" class="text-center">Fecha de Registro</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>

            <tbody>
          
                <!--SI EL revisadot1 ES 0 EL REGISTRO ES NUEVO SI NO , EL REGISTRO SE ABRIO-->
            @foreach($verT22 as $reg22)

                @if($reg22->codOt2_2 == '') 
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg22->id}}</a><a href="seleccionConfiscacion/{{$reg22->id}}"> C-1</a> </td>
                @else
                        <td class="text-center"><a href="#" hidden>{{$reg22->id}}</a><a href="seleccionConfiscacion/{{$reg22->id}}">{{$reg22->codOt2_2}}</a></td>
                @endif

                       @if($reg22->nomPa == '1')  
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center">{{$reg22->nomPa}}</td>
                       @endif

                       @if($reg22->nomBen == '1')
                        <td class="text-center">noaplica</td>
                       @else
                        <td class="text-center">{{$reg22->nomBen}}</td>
                       @endif

                       @if($reg22->nomAuto == '1')
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center" >{{$reg22->nomAuto}}</td>
                       @endif

                       @if($reg22->numSenc == '0')
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center">{{$reg22->numSenc}}</td>
                       @endif

                       @if($reg22->nomRegno == '1')
                        <td class="text-center">xxx</td>
                       @else
                        <td class="text-center">{{$reg22->nomRegno}}</td>
                       @endif

                       @if($reg22->feReg == '1111-11-11')
                        <td class="text-center">11111111</td>
                       @else
                        <td class="text-center">{{$reg22->feReg}}</td>
                       @endif

                        <td class="text-center"><a href="seleccionConfiscacion/{{$reg22->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
              @endforeach
           </tbody>
        </table>
    </div>
  </div>
</div>
@endsection
