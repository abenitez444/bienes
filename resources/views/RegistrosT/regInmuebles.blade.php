@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12 separar">
               <center><h4><b>Datos de los Bienes Inmuebles del Órgano o Ente</b></h4></center>
               <center><h5>• Edificaciones, Tierras y Terrenos.</h5></center>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
    
        <table id="tablaT1" class="table-striped table-bordered table-hover" >
              
                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código del Origen del Bien</td>
                       <td id="letrasb" class="text-center">Código Según Catalogo</td>
                       <td id="letrasb" class="text-center">Dependencia Administrativa</td>
                       <td id="letrasb" class="text-center">Código del Responsable Administrativo</td>
                       <td id="letrasb" class="text-center">Corresponde el Bien a alguna Sede</td>
                       <td id="letrasb" class="text-center">Código del Ente donde Corresponde el Bien</td>
                       <td id="letrasb" class="text-center">Estatus del Uso del Bien</td>
                       <td id="letrasb" class="text-center">Localización</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>

            <tbody>
          
               @foreach($verT12 as $reg12)
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg12->id}}</a><a href="seleccionInmuebles/{{$reg12->id}}"> {{$reg12->codBien}}</a> </td>
                        
                        <td class="text-center">{{$reg12->selectCatalogoinm->codigo}}</td>
                        
                        <td class="text-center">{{$reg12->selectUnidadinmu->codUnidad}}</td>

                        <td class="text-center">{{$reg12->selectResponsableinmu->codResp}}</td>
                        
                        <td class="text-center">{{$reg12->selectCorresinmu->opcion}}</td>

                        <td class="text-center">{{$reg12->selectOrganoinmu->codSede}}</td>

                        <td class="text-center">{{$reg12->selectEstatuinmu->opcion}}</td>
                        
                        <td class="text-center">{{$reg12->selectLocalinmu->opcion}}</td>

                        <td class="text-center"><a href="seleccionInmuebles/{{$reg12->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
              @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
