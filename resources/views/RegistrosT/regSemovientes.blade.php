@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12 separar">
               <center><h4><b>Datos de los Bienes Muebles del Órgano o Ente</b></h4></center>
               <center><h5>• Semovientes.</h5></center>  
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
    
         <table id="tablaT1" class="table-striped table-bordered table-hover">
              
                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código Origen del Bien</td>
                       <td id="letrasb" class="text-center">Código según Catalogo</td>
                       <td id="letrasb" class="text-center">Dependencia Administrativa</td>
                       <td id="letrasb" class="text-center">Sede del Ente</td>
                       <td id="letrasb" class="text-center">Código Responsable Administrativo</td>
                       <td id="letrasb" class="text-center">Código Responsable del Bien </td>
                       <td id="letrasb" class="text-center">Código Interno del Bien</td>
                       <td id="letrasb" class="text-center">Estatus uso del Bien</td>
                       <td id="letrasb" class="text-center">Fecha de Nacimiento</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>

            <tbody>
          
               @foreach($verT10 as $reg10)
                  
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg10->id}}</a><a href="seleccionSemovientes/{{$reg10->id}}"> {{$reg10->codBien}}</a> </td>
                        
                        @if($reg10->codCata == '1')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$reg10->selectCatalogosem->codigo}}</td>
                        @endif
                        
                        <td class="text-center">{{$reg10->selectUnidadsemo->codUnidad}}</td>

                        <td class="text-center">{{$reg10->selectOrganosemo->codSede}}</td>

                        <td class="text-center">{{$reg10->selectResponsables->codResp}}</td>
              
                        <td class="text-center">{{$reg10->ResponsableDirectosemo->codResp}}</td>

                        @if($reg10->codInterno == '1')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$reg10->codInterno}}</td>
                        @endif

                        <td class="text-center">{{$reg10->selectEstatusemo->opcion}}</td>
                        
                        @if($reg10->feNacimiento == '1111-11-11')
                        <td class="text-center">11111111</td>
                        @else
                        <td class="text-center">{{$reg10->feNacimiento}}</td>
                        @endif

                        <td class="text-center"><a href="seleccionSemovientes/{{$reg10->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
              @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
