@extends('layouts.app')

@section('content')

  <div class="row">
      <div class="col-xs-8 col-sm-6 col-md-12">
          <div class="row separar40">
              <div class="col-md-12">
                 <center><h4><b>Datos del Seguro de los Bienes Muebles e Inmuebles del Órgano o Ente</b></h4></center>
              </div>
          </div>
            
          <div class="row">
              <div class="col-md-12 desvanecer">
                @if(session()->has('msj'))
                  <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                       @endif

                       @if(session()->has('errormsj'))
                  <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
                @endif
              </div>
            </div>    
    
        <table id="tablaT1" class="table table-striped table-responsive table-bordered table-hover">
              
                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código Registro</td>
                       <td id="letrasb" class="text-center">Compañía aseguradora</td>
                       <td id="letrasb" class="text-center">Otra Compañía aseguradora</td>
                       <td id="letrasb" class="text-center">Número Póliza</td>
                       <td id="letrasb" class="text-center">Tipo Póliza</td>
                       <td id="letrasb" class="text-center">Fecha Inicio de Póliza</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                      
                    </tr>
                </thead>

            <tbody>
          
                <!--SI EL revisadot3 ES 0 EL REGISTRO ES NUEVO SI NO , EL REGISTRO SE ABRIO-->
        @foreach($verT3 as $reg3)

                @if($reg3->codRegT3 == '0') 
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg3->id}}</a><a href="seleccionSeguros/{{$reg3->id}}"> 99</a> </td>
                @else
                        <td class="text-center"><a href="#" hidden>{{$reg3->id}}</a><a href="seleccionSeguros/{{$reg3->id}}">{{$reg3->codRegT3}}</a></td>
                @endif

                        <td class="text-center">{{$reg3->selectSeguros->opcion}}</td>

                      @if($reg3->otraCom == '1')
                        <td class="text-center">noaplica</td>
                      @else
                        <td class="text-center">{{$reg3->otraCom}}</td>
                      @endif

                      @if($reg3->numPoli == '0')
                        <td class="text-center">xxx</td>
                      @else
                        <td class="text-center">{{$reg3->numPoli}}</td>
                      @endif
                        
                      @if($reg3->tipPoli == '1')
                        <td class="text-center">I</td>
                      @else
                        <td class="text-center">C</td>
                      @endif

                      @if($reg3->feiniPoli == '1111-11-11')
                        <td class="text-center">11111111</td>
                      @else
                        <td class="text-center">{{$reg3->feiniPoli}}</td>
                      @endif

                       <td class="text-center"><a href="seleccionSeguros/{{$reg3->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>

                  </tr>     
              @endforeach
           </tbody>
        </table>
    </div>
</div>
@endsection
