@extends('layouts.app')

@section('content')

 <div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12">
              <center><h4><b>Datos de los Proveedores de los Bienes Públicos del Órgano o Ente</b></h4></center>
            </div>
        </div>
    <hr>
        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
 
       <div id="tabla" class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="overflow-x:auto;">
        
        <table id="tablaT1" class="table-striped dt-responsive nowrap">
             
              <thead>
                  <tr>
                    <th id="letrasb" class="text-center">Código Proveedor</th>
                    <th id="letrasb" class="text-center">Descripción Proveedor</th>
                    <th id="letrasb" class="text-center">Tipo Proveedor</th>
                    <th id="letrasb" class="text-center">Rif</th>
                    <th id="letrasb" class="text-center">Otra Descripción</th>
                    <th id="letrasb" class="text-center">Ver más</th>
                  </tr>
              </thead>
     
          <tbody>
          
            @foreach($verT1 as $registro)
                @if($registro->otraDesc == '0')
                    <tr>
                        <!--SI EL revisadot1 ES 0 EL REGISTRO ES NUEVO SI NO , EL REGISTRO SE ABRIO-->
                     
                        <td class="text-center"><a href="#" hidden>{{$registro->id}}</a><a href="seleccionProveedores/{{$registro->id}}">{{$registro->codProvee}}</a></td>

                        <td class="text-center">{{$registro->descProvee}}</td>
                        @if($registro->tipProvee == '1')
                        <td class="text-center">N</td>
                        @else
                        <td class="text-center">I</td>
                        @endif
                        <td class="text-center">{{$registro->rifProvee}}</td>
                        <td class="text-center">xxx</td>
                        <td class="text-center"><a href="seleccionProveedores/{{$registro->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                      
                    </tr>
                @else
                    <tr>

                        <td class="text-center"><a href="seleccionProveedores/{{$registro->id}}">{{$registro->codProvee}}</a></td>
                  
                        @if($registro->descProvee == '0')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$registro->descProvee}}</td>
                        @endif
                        
                        @if($registro->tipProvee == '1')
                        <td class="text-center">N</td>
                        @else
                        <td class="text-center">I</td>
                        @endif

                        <td class="text-center">{{$registro->rifProvee}}</td>
                        
                        @if($registro->otraDesc == '0')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$registro->otraDesc}}</td>
                        @endif
                  
                        <td class="text-center"><a href="seleccionProveedores/{{$registro->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                       
                    </tr>
                @endif
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
</div>      
@endsection
