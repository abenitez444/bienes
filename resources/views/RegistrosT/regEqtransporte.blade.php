@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12 separar">
               <center><h4><b>Datos de los Bienes Muebles del Órgano o Ente</b></h4></center>
                <center><h5>• Equipos de transporte, tracción y elevación</h5></center>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
    
        
        <table id="tablaT1" class="table-striped table-bordered table-hover">
              
                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código del Origen del Bien</td>
                       <td id="letrasb" class="text-center">Código según Catalogo</td>
                       <td id="letrasb" class="text-center">Dependencia Administrativa</td>
                       <td id="letrasb" class="text-center">Sede del Órgano o ente</td>
                       <td id="letrasb" class="text-center">Código del Responsable Administrativo</td>
                       <td id="letrasb" class="text-center">Código del Responsable del Bien </td>
                       <td id="letrasb" class="text-center">Código Interno del Bien</td>
                       <td id="letrasb" class="text-center">Estatus del uso del Bien</td>
                       <td id="letrasb" class="text-center">Fecha Ingreso</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>

            <tbody>
          
               @foreach($verT9 as $reg9)
                  
                  <tr>
                        <td class="text-center"><a href="#" hidden>{{$reg9->id}}</a><a href="seleccionEqtransporte/{{$reg9->id}}"> {{$reg9->codBien}}</a> </td>
                     
                        <td class="text-center">{{$reg9->selectCatalogotran->codigo}}</td>
                      
                        <td class="text-center">{{$reg9->selectUnidadtran->codUnidad}}</td>
                      
                        <td class="text-center">{{$reg9->selectOrganotran->codSede}}</td>

                        <td class="text-center">{{$reg9->selectResponsabletran->codResp}}</td>
                        
                        <td class="text-center">{{$reg9->ResponsableDirectotran->codResp}}</td>
                 
                        @if($reg9->codInterno == '1')
                        <td class="text-center">xxx</td>
                        @else
                        <td class="text-center">{{$reg9->codInterno}}</td>
                        @endif

                        <td class="text-center">{{$reg9->selectEstatustr->opcion}}</td>
                        
                        @if($reg9->feIngBien == '1111-11-11')
                        <td class="text-center">11111111</td>
                        @else
                        <td class="text-center">{{$reg9->feIngBien}}</td>
                        @endif


                        <td class="text-center"><a href="seleccionEqtransporte/{{$reg9->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr>     
              @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
