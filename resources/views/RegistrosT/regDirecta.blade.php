@extends('layouts.app')

@section('content')

  <div class="row separar">
    <div class="col-xs-8 col-sm-6 col-md-12 separar">
         
         <div class="row separar">
           <div class="col-md-12">
              <center><h4><b>Datos de los Orígenes (Formas de Adquisición) de los Bienes Muebles e Inmuebles del Órgano o Ente</b></h4></center>
              <center><h5>B) Aplicable solo para las formas de adquisición de compra por Consulta de Precios.</h5></center>
           </div>
         </div> 

        
   <hr>
    <div class="row">
        <div class="col-md-12 desvanecer">
        @if(session()->has('msj'))
            <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
               @endif

               @if(session()->has('errormsj'))
            <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
        @endif
        </div>
    </div>    

     <div id="tabla" class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="overflow-x:auto;">

   
    
        <table id="tablaT1" class="table table-striped dt-responsive nowrap">
                <thead>
                    <tr>
                       <td id="letrasb" class="text-center">Código Origen</td>
                       <td id="letrasb" class="text-center">Código del Proveedor</td>
                       <td id="letrasb" class="text-center">Fecha Orden Compra</td>
                       <td id="letrasb" class="text-center">Ver más</td>
                    </tr>
                </thead>
            <tbody>
          
                <!--SI EL revisadot1 ES 0 EL REGISTRO ES NUEVO SI NO , EL REGISTRO SE ABRIO-->
                @foreach($verT21 as $reg21)
                    @if($reg21->codOt2_1 == "")
                      <tr>
                          <td class="text-center"><a href="#" hidden>{{$reg21->id}}</a><a href="seleccionDirecta/{{$reg21->id}}"> B-1</a> </td>
                    @else   
                          <td class="text-center"><a href="#" hidden>{{$reg21->id}}</a><a href="seleccionDirecta/{{$reg21->id}}">{{$reg21->codOt2_1}}</a></td>
                    @endif
                        
                        <td class="text-center">{{$reg21->selectCodProvee->codProvee}}</td>
                      
                      <!--OJO REVISAR 10 DE AGOSTO ESTAN GUARDADANDO 9 VECES EL NUMERO 1 EN LOS CAMPOS DATE REVISAR TABLAS--> 
                        @if($reg21->feCom == '1111-11-11')
                        <td class="text-center">11111111</td>
                        @else
                        <td class="text-center">{{$reg21->feCom}}</td>
                        @endif

                        <td class="text-center"><a href="seleccionDirecta/{{$reg21->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                  </tr> 
                @endforeach
           </tbody>
        </table>
    </div>
  </div>
</div>
@endsection
