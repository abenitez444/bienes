@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div id="panelTitu" class="panel-heading text-center separar"><h5 id="h5Titu"><b> <i class="fa fa-file-o" aria-hidden="true"></i> DATOS DE UBICACIÓN / DATOS DE UBICACIÓN DE LAS UNIDADES ADMINISTRATIVAS DEL ORGANO O ENTE.</b></h5></div>
        </div>
            
        <div class="row"> 
            <div class="col-md-12 li moverIzq">
                <ul class="js-errors li"></ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
             @if(session()->has('msj'))
                <center><div  class="col-md-12  alert alert-success" role="alert">{{session('msj')}}</div></center>
                  @endif

                  @if(session()->has('errormsj'))
                <center><div  class="col-md-12  alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
             @endif
            </div>
        </div>
    <hr>
        <form role="form" id="formValidaS6" name="formValidaS6" method="POST" action="{{url('ubicacion')}}">
        {{ csrf_field() }}

      <!--ARRAY DE INPUT PERTENECIENTE AL CONTROLADORS6 -->

      <div class="row">
            <div class="col-md-11 col-md-offset-1">

                 @foreach($array as $posicion => $valor)

                    <div class="col-md-5 form-group separar">
                      <label for="{{$array[$posicion][0]}}">{{$array[$posicion][2]}}</label>
                          <select name="{{$array[$posicion][0]}}" id="{{$array[$posicion][0]}}" class="{{$array[$posicion][1]}}">
                              <option value="0" disabled selected>Seleccione</option>
                            @foreach($cod_sede as $traeSelect)
                             <option value="{{$traeSelect->id}}">{{$traeSelect->codSede}}</option> 
                            @endforeach
                          </select>
                    </div>
                  @endforeach
    
                  @foreach($array2 as $posicion => $valor)
                    <div class="col-md-5 form-group separar">
                      <label for="{{$array2[$posicion][0]}}">{{$array2[$posicion][2]}}</label>
                          <select name="{{$array2[$posicion][0]}}" id="{{$array2[$posicion][0]}}" class="{{$array2[$posicion][1]}}">
                             <option value="0" disabled selected>Seleccione</option>
                            @foreach($cod_unidad as $traeSelect)
                             <option value="{{$traeSelect->id}}">{{$traeSelect->codUnidad}}</option> 
                            @endforeach
                          </select>
                    </div>
                  @endforeach
              </div>
      </div>  
             
            <div class="row text-center">
                <div class="col-md-12 form-group"><br>
                           
                  <center><button type="submit" class="btn btn-md btn-success" name="#" title="Guardar Registro"><i class="fa fa-check-square-o" aria-hidden="true"></i><b> Enviar</b></button>
                          
                  <a href="{{url('home')}}" class="btn btn-md btn-danger" title="Salir" > <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> <b>Salir</b></a></center>  

                </div>
            </div>
      </form>
   </div>
</div>

@endsection
