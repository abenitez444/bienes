@extends('layouts.app')

@section('content')

 <div class="row">
    <div class="col-xs-8 col-sm-6 col-md-12">
        <div class="row separar">
            <div class="col-md-12">
              <center><h4><b>Datos de las Unidades Administrativas del Órgano o Ente</b></h4></center>
            </div>
        </div>
    <hr>
        <div class="row">
            <div class="col-md-12 desvanecer">
            @if(session()->has('msj'))
                <center><div  class="col-md-12 alert alert-success" role="alert">{{session('msj')}}</div></center>
                   @endif

                   @if(session()->has('errormsj'))
                <center><div  class="col-md-12 alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
            @endif
            </div>
        </div>    
 
        <table id="tablaT1" class="tabla table-striped table-responsive table-bordered table-hover">
             
              <thead style="font-size:12px;">
                  <tr>
                    <th id="letrasb" class="text-center">Código de la Unidad</th>
                    <th id="letrasb" class="text-center">Descripción de la Unidad</th>
                    <th id="letrasb" class="text-center">Código Categoría de Unidad</th>
                    <th id="letrasb" class="text-center">Especifique Denominación de la Categoría</th>
                    <th id="letrasb" class="text-center">Código Unidad a la cual está Adscrita</th>
                    <th id="letrasb" class="text-center">Ver más</th>
                  </tr>
              </thead>
     
          <tbody style="font-size:12px;">
            @foreach($verS5 as $registro)
                
                <tr>
                        <td class="text-center"><a href="#" hidden>{{$registro->id}}</a><a href="seleccionUnidades/{{$registro->id}}">{{$registro->codUnidad}}</a></td>
                     
                        <td class="text-center">{{$registro->descUnidad}}</td>
                     
                        <td class="text-center">{{$registro->selectCategoria->categoria}}</td>
                        
                        @if($registro->espeCatego == '1')
                        <td class="text-center">noaplica</td>
                        @else
                        <td class="text-center">{{$registro->espeCatego}}</td>
                        @endif

                        @if($registro->codAdscrita == '1')
                        <td class="text-center">0</td>
                        @else
                        <td class="text-center">{{$registro->codAdscrita}}</td>
                        @endif
                  
                        <td class="text-center"><a href="seleccionUnidades/{{$registro->id}}"><i style="color:#8E2121;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a></td>
                       
                    </tr>
             
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
      
@endsection
