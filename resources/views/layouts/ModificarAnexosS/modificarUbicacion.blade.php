<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Modificar Ubicación</title>

     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('css/appstilo.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/general.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/tablas.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('img/bandera.png') }}" rel="icon">
   
  </head>
<body>

<div class="container" id="sha">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="panel">
                     <img src="{{URL::asset('/img/cintillo.jpg')}}" id="banner" alt="Cintillo Web">
                   <div class="panel-heading text-center"><h5><b>MODIFICAR DATOS DE UBICACIÓN DE LAS UNIDADES ADMINISTRATIVAS DEL ORGANO O ENTE.<i class="fa fa-pencil-square-o" aria-hidden="true"></i></b></h5></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Modificar Registro <b id="colorInstruccion">N°# {{$form_s6->id}}</b></h4>
                </div>
            </div>
            
          <hr>

            <div class="row">
                <div class="col-md-12 desvanecer">
                  @if(session()->has('msj'))
                      <center><div  class="col-md-12  alert alert-success" role="alert">{{session('msj')}}</div></center>
                         @endif

                         @if(session()->has('errormsj'))
                      <center><div  class="col-md-12  alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
                  @endif
                </div>
            </div>

    <form role="form" method="POST" action="{{route('ubicacion.update', $form_s6->id)}}">
        <input type="hidden" name="_method" value="PUT">
           {{ csrf_field() }}
  
                   
            <div class="row separar">
                <div class="col-md-10 col-md-offset-2">
                    <div class="col-md-5 form-group">
                      <label for="codSede">Código de la Sede o Lugar de Ubicación </label>
                         <select name="codSede" id="codSede" class="form-control">
                              <option value="{{$form_s6->id}}">{{$form_s6->selectSedeS6->codSede}}</option>
                            @foreach($selectS4 as $form)
                              @if($form->id != $form_s6->selectSedeS6->id)
                              <option value="{{$form->id}}">{{$form->codSede}}</option> 
                              @endif
                            @endforeach
                         </select>
                    </div>

                    <div class="col-md-5 form-group">
                      <label for="codUnidad">Código de la Unidad Administrativa  </label>
                         <select name="codUnidad" id="codUnidad" class="form-control">
                              <option value="{{$form_s6->id}}">{{$form_s6->selectUnidadS6->codUnidad}}</option>
                            @foreach($selectS5 as $form)
                              @if($form->id != $form_s6->id)
                              <option value="{{$form->id}}">{{$form->codUnidad}}</option> 
                              @endif
                            @endforeach
                         </select>
                    </div>
                </div>
            </div>
           
            <div class="row">
                <div class="col-md-12 form-group"><br>
                    <center><button type="submit" class="btn btn-sm btn-info" name="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modificar</b></button>
                     <a href="{{ url('/regUbicacion') }}"  class="btn btn-sm btn-danger" ><i class="fa fa-reply " aria-hidden="true" title="Regresar"></i> <b>Regresar</b></a></center>  
                </div>
            </div>
        </div>
    </div>
</div>
</form>
</body>
</html>

    <script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/validate.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('js/inputDinamicoanexosS.js') }}"></script>
   
