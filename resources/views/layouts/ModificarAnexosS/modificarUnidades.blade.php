<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Modificar Unidades</title>

     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('css/appstilo.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/general.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/tablas.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('img/bandera.png') }}" rel="icon">
   
  </head>
<body>

<div class="container" id="sha">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="panel">
                     <img src="{{URL::asset('/img/cintillo.jpg')}}" id="banner" alt="Cintillo Web">
                   <div class="panel-heading text-center"><h5><b>MODIFICAR DATOS DE LAS UNIDADES ADMINISTRATIVAS DEL ÓRGANO O ENTE<i class="fa fa-pencil-square-o" aria-hidden="true"></i></b></h5></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Modificar Registro <b id="colorInstruccion">N°# {{$form_s5->id}}</b></h4>
                </div>
            </div>
            
          <hr>

            <div class="row">
                <div class="col-md-12 desvanecer">
                  @if(session()->has('msj'))
                      <center><div  class="col-md-12  alert alert-success" role="alert">{{session('msj')}}</div></center>
                         @endif

                         @if(session()->has('errormsj'))
                      <center><div  class="col-md-12  alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
                  @endif
                </div>
            </div>

    <form role="form" method="POST" action="{{route('unidades.update', $form_s5->id)}}">
        <input type="hidden" name="_method" value="PUT">
           {{ csrf_field() }}

            <div class="row separar">
                <div class="col-md-12 col-md-offset-4 separar40">
                    <div class="col-md-4">
                       <label for="codUnidad"><b class="requiredV">*</b>Código de la Unidad</label>
                   
                          <input type="text" class="form-control" name="codUnidad" id="codUnidad" value="{{$form_s5->codUnidad}}" maxlength="10">
                    </div>
                </div>
            </div> 

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-10 form-estilo  col-md-offset-1 separar40">
                          <li><b class="requiredV">*</b>Descripción de la Unidad</li>
                        
                    <textarea name="descUnidad" id="descUnidad" value="{{$form_s5->descUnidad}}" class="form-control" maxlength="255" rows="4">{{$form_s5->descUnidad}}</textarea>
                    <div id="negro" for="contador">Caracteres: <div class="rojo" id="conbienes">0/255</div> </div>
                       
                   </div>
                </div>
            </div>       
                   
            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                      <label for="categoria"><b class="requiredV">*</b>Código de la Categoría de la Unidad</label>
                         <select name="categoria" id="categoria" class="form-control">
                              <option value="{{$form_s5->selectCategoria->id}}">{{$form_s5->selectCategoria->categoria}}</option>
                            @foreach($categoria as $form)
                              @if($form->id != $form_s5->selectCategoria->id)
                              <option value="{{$form->id}}">{{$form->categoria}}</option> 
                              @endif
                            @endforeach
                         </select>
                    </div>

                    <div class="col-md-4 separar">
                       <label for="espeCatego">Especifique la Denominación de la Categoría</label> 
                        @if($form_s5->espeCatego == '1')  
                          <input type="text" class="form-control" name="espeCatego" id="espeCatego" value="noaplica" maxlength="100" disabled> 
                        @else
                          <input type="text" class="form-control" name="espeCatego" id="espeCatego" value="{{$form_s5->espeCatego}}" maxlength="100">
                        @endif
                    </div>

                    <div class="col-md-4 separar">
                       <label for="codAdscrita">Código de Unidad a la cual está Adscrita </label> 
                        @if($form_s5->codAdscrita == '1')  
                          <input type="text" class="form-control" name="codAdscrita" id="codAdscrita" value="0" maxlength="10"> 
                        @else
                          <input type="text" class="form-control" name="codAdscrita" id="codAdscrita" value="{{$form_s5->codAdscrita}}" maxlength="10">
                        @endif
                    </div>
                </div>
            </div>
           
            <div class="row">
                <div class="col-md-12 form-group"><br>
                    <center><button type="submit" class="btn btn-sm btn-info" name="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modificar</b></button>
                     <a href="{{ url('/regUnidades') }}"  class="btn btn-sm btn-danger" ><i class="fa fa-reply " aria-hidden="true" title="Regresar"></i> <b>Regresar</b></a></center>  
                </div>
            </div>
        </div>
    </div>
</div>
</form>
</body>
</html>

    <script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/validate.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('js/inputDinamicoanexosS.js') }}"></script>
   
