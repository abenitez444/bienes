<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('', 'Bienes Nacionales') }}</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('img/bandera.png') }}" rel="icon">
        <link href="{{ asset('css/login.css') }}" rel="stylesheet">  
        <link href="{{ asset('css/menu.css') }}" rel="stylesheet">  
        <link href="{{ asset('css/general.css') }}" rel="stylesheet">  
        <link href="{{ asset('css/appstilo.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/tablas.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap-submenu.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('img/bandera.png') }}" rel="icon">
        <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">

    </head>
<body>

    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        <img id="logo" class="subir-10 img-responsive" id="cintilloWeb" src="{{ asset('img/logo.png') }}" alt="Logo"> 
                    </a>
                </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Registrar <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <!--SUB-MENU DE ANEXOS T-->
                                <li class="dropdown-submenu"><a href="">Datos Generales de Organos y Entes</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="basicos">Datos Básicos del Ente</a></li>
                                        <li><a href="maxima">Datos de la Máxima Autoridad</a></li>
                                        <li><a href="patrimonial">Datos del Responsable Patrimonial</a></li>
                                        <li><a href="sedes">Datos de las Sedes y Similares del Ente</a></li>
                                        <li><a href="unidades">Unidades Administrativas</a></li>
                                       <!-- <li><a href="ubicacion">Datos de Ubicación - Unidades Administrativas</a></li> -->
                                    </ul>
                                </li>
                                           
                                <li class="dropdown-submenu"><a href="">Datos Generales de los Inventarios</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="proveedores">Proveedores</a></li>

                                    <li class="dropdown-submenu"><a href="">Modalidades</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="concurso">Compra por Concurso</a></li>
                                            <li><a href="directa">Compra Directa</a></li>
                                            <li><a href="confiscacion">Confiscación</a></li>
                                            <li><a href="dacion">Dación en Pago</a></li>
                                            <li><a href="donacion">Donación</a></li>
                                            <li><a href="expropiacion">Expropiación</a></li>
                                            <li><a href="permuta">Permuta</a></li>
                                            <li><a href="transferencia">Transferencia</a></li>
                                            <li><a href="adjudicacion">Adjudicación</a></li>
                                        </ul>
                                    </li>
                                            <li><a href="seguros">Seguros</a></li>
                                            <li><a href="responsables">Responsables de los Bienes</a></li>
                                            <li><a href="marcas">Marcas</a></li>
                                            <li><a href="modelos">Modelos</a></li>
                                            <li><a href="componentes">Componentes</a></li>
                                            <li><a href="bienes">Bienes</a></li>
                                            <li><a href="transporte">Equipo de Transporte</a></li>
                                            <li><a href="semovientes">Semovientes</a></li>
                                            <li><a href="datosbienes">Bienes Muebles e Inmuebles</a></li>
                                            <li><a href="datosinmuebles">Bienes de Inmuebles del Ente</a></li>
                                        </ul>
                                    </li>
                    
                                                                        
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Consultar <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="dropdown-submenu"><a href="">Datos Generales de Organos y Entes</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="regBasicos">Datos Básicos</a></li>
                                        <li><a href="regMaxima">Datos de la Máxima Autoridad</a></li>
                                        <li><a href="regPatrimonial">Datos del Responsable Patrimonial</a></li>
                                        <li><a href="regSedes">Datos de las Sedes y Similares</a></li>
                                        <li><a href="regUnidades">Unidades Administrativas</a></li>
                                        <!-- <li><a href="regUbicacion">Datos de Ubicación - Unidades Administrativas</a></li> -->
                                    </ul>
                                </li>

                                <li class="dropdown-submenu"><a href="">Datos Generales de los Inventarios</a>
                                    <ul class="dropdown-menu">
                                      <li><a href="regProveedores">Proveedores</a></li>

                                      <li class="dropdown-submenu"><a href="">Modalidades</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="regConcurso">Compra por Concurso</a></li>
                                            <li><a href="regDirecta">Compra Directa</a></li>
                                            <li><a href="regConfiscacion">Confiscación</a></li>
                                            <li><a href="regDacion">Dación en Pago</a></li>
                                            <li><a href="regDonacion">Donación</a></li>
                                            <li><a href="regExpropiacion">Expropiación</a></li>
                                            <li><a href="regPermuta">Permuta</a></li>
                                            <li><a href="regTransferencia">Transferencia</a></li>
                                            <li><a href="regAdjudicacion">Adjudicación</a></li>
                                        </ul>
                                       </li>

                                       <li><a href="regSeguros">Seguros</a></li>
                                       <li><a href="regResponsables">Responsables de los Bienes</a></li>
                                       <li><a href="histoMarcas">Marcas</a></li>
                                       <li><a href="histoModelos">Modelos</a></li>
                                       <li><a href="regComponentes">Componentes</a></li>
                                       <li><a href="regBienes">Bienes</a></li>
                                       <li><a href="regTransporte">Equipo de Transporte</a></li>
                                       <li><a href="regSemovientes">Semovientes</a></li>
                                       <li><a href="regDatosbienes">Bienes Muebles e Inmuebles</a></li>
                                       <li><a href="regInmuebles">Bienes de Inmuebles del Ente</a></li>
                                    </ul>
                                </li> 
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Exportar   <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-submenu"><a href="">Datos Generales de Organos y Entes</a>
                                        <ul class="dropdown-menu">

                                            <li><a href="export-basicos">Datos Básicos <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-maxima">Datos de la Máxima Autoridad <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                           
                                            <li><a href="export-patrimonial">Datos del Responsable Patrimonial <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                            
                                            <li><a href="export-sedes">Datos de las Sedes <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-unidades">Unidades Administrativas <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                            <li><a href="export-ubicacion">Datos de Ubicación - Unidades Administrativas <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                            
                                        </ul>
                                    </li>

                                    <li class="dropdown-submenu"><a href="">Datos Generales de los Inventarios</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="export-provee">Proveedores  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                        
                                            <li class="dropdown-submenu"><a href="">Modalidades</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="export-concu">Compra por Concurso  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-directa">Compra Directa <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-confiscacion">Confiscación <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-dacion">Dación en Pago <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-donacion">Donación <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-expro">Expropiación <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-permuta">Permuta <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                                    <li><a href="export-transfe">Transferencia <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                                    
                                                    <li><a href="export-adjud">Adjudicación <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                                </ul>
                                            </li>
                                            <li><a href="export-seguro">Seguros  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-respon">Responsables de los Bienes  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-marcas">Marcas  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-modelos">Modelos  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-compo">Componentes  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-bienes">Bienes  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-trans">Equipo de Transporte  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>

                                            <li><a href="export-semo">Semovientes  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                            
                                            <li><a href="export-muebles">Bienes Muebles e Inmuebles  <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                            
                                            <li><a href="export-inmuebles">Bienes de Inmuebles del Ente <img id="imgExcel" src="{{ asset('img/excel.png') }}"></a></li>
                                        </ul>
                                    </li>

                                  
                            </ul>
                        </li>
                    </ul>


                         <!-- Right Side Of Navbar -->
                         <ul class="nav navbar-nav navbar-right">
                         <!-- Authentication Links-->
                      
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                 <i class="fa fa-user" aria-hidden="true"></i> {{ $_SESSION['nombre'] }}<span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}">
                                           Cerrar Sesión
                                        </a>
                                   
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        @if($_SESSION['rol'] == 1)
                        <ul class="nav navbar-nav navbar-right">
                         <!-- Authentication Links-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                 <i class="fa fa-cog" aria-hidden="true"></i>
                                </a>
                            
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                         
                                    <a href="{{ url('/addUser') }}">
                                        Agregar Usuario   <i class="fa fa-plus"></i>
                                    </a> 

                                    <a href="{{ url('/bitacora') }}">
                                        Bitácora  <i class="fa fa-archive" aria-hidden="true"></i>  
                                    </a> 

                                    </li>
                                </ul>
                            </li>
                        </ul>
                        @endif

                </div>
            </div>
        </nav>
   </div>
    
 <div class="container" id="sha">
    <div class="row">
       <div class="col-xs-8 col-sm-6 col-md-12" >
           <img src="{{URL::asset('/img/cintillo.jpg')}}" id="banner" alt="Cintillo Web">
       </div>
    </div>
         @yield('content')
</div>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/dropdown.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/validateGeneral.js') }}"></script> <!-- Valida los campos de las (Tabla Proveedores=T1 a la Tabla bienes de Inmuebles Organos=T12) -->
    <script src="{{ asset('js/validateModalidades.js') }}"></script> <!-- Valida los campos de la Tabla compra por concurso=T2 a la  Tabla adjudicación=T28 (Modalidades)) -->
    
    <script src="{{ asset('js/validateDatepicker.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-hover-dropdown.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-submenu.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/tabla.datatable.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('js/funcion.mask.decimal.js') }}"></script>
    <script src="{{ asset('js/inputDinamicoanexosT.js') }}"></script>
    <script src="{{ asset('js/inputDinamicoanexosS.js') }}"></script>
    <script src="{{ asset('js/contadorTextarea.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/selectBuscador.js') }}"></script>
    <script src="{{ asset('js/validaSelect2.js') }}"></script>
    <script src="{{ asset('js/addUser.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.min.js') }}"></script>

<script>

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script>
 
  function validatePass(pass){
    $.ajax({
      url: '{{url("/validatePass")}}',
      method: 'POST',
      data: {
        pass
      },
      success: function(response) {
        
        $.ajax({
          url: $('#formAddUser').attr("action"),
          method: $('#formAddUser').attr("method"),
          data: $('#formAddUser').serialize(),
          success: function(result){

            Swal.fire({
              type: 'success',
              title: "¡Usuario registrado exitosamente!",
              showConfirmButton: false,
              timer: 2000
            })

            setTimeout(function(){
              location.reload(); 
            }, 2000); 

          },error: function(error){
            Swal.fire({
              type: 'error',
              title: error.responseJSON.message,
            })    
          }
        })

      },error: function(error) {
        
        Swal.fire({
          type: 'error',
          title: error.responseJSON.message,
        })

      }
    })
  }
</script>
    
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <center><footer id="pie">
                    <h6>Vicepresidencia de la República Bolivariana de Venezuela</h6>
                    <h6>Dirección General de Tecnología de la Información - &copy; 2019 Todos los Derechos Reservados</h6>
                </footer></center>
            </div>
        </div>
    </div>
</body>
</html>
