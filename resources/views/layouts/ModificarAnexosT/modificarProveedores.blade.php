<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Modificar Proveedores</title>

     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('css/appstilo.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/general.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/tablas.css') }}" rel="stylesheet">  
     <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

  </head>
<body>

<div class="container" id="sha">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="panel">
                     <img src="{{URL::asset('/img/cintillo.jpg')}}" id="banner" alt="Cintillo Web">
                   <div class="panel-heading text-center"><h5><b>MODIFICAR PROVEEDORES <i class="fa fa-pencil-square-o" aria-hidden="true"></i></b></h5></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Modificar Registro <b id="colorInstruccion">N°# {{$form_t1->id}}</b></h4>
                </div>
            </div>
            
          <hr>

            <div class="row">
                <div class="col-md-12 desvanecer">
                  @if(session()->has('msj'))
                      <center><div  class="col-md-12  alert alert-success" role="alert">{{session('msj')}}</div></center>
                         @endif

                         @if(session()->has('errormsj'))
                      <center><div  class="col-md-12  alert alert-danger" role="alert">{{session('errormsj')}}</div></center>
                  @endif
                </div>
            </div>

    <form role="form" method="POST" action="{{route('proveedores.update', $form_t1->id)}}" id="formValidaT1" name="formValidaT1">
        <input type="hidden" name="_method" value="PUT">
           {{ csrf_field() }}

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4">
                       <label for="codProvee"><b class="requiredV">*</b> Código del Proveedor</label>
                          <input type="text" class="form-control" name="codProvee" id="codProvee" value="{{$form_t1->codProvee}}"  placeholder="Introduzca nombres" maxlength="30" > 
                    </div>
               
                    <div class="col-md-4">
                       <label for="descProvee"><b class="requiredV">*</b> Descripción del Proveedor</label> 
                          <input type="text" class="form-control" name="descProvee" id="descProvee" value="{{$form_t1->descProvee}}" maxlength="100">
                    </div>
              
                    <div class="pull-right col-md-4 form-group">
                      <label for="tipProvee"><b class="requiredV">*</b> Tipo de Proveedor</label>
                         <select name="tipProvee" id="tipProvee" class="form-control">
                              <option value="{{$form_t1->selectProvee->id}}".00>{{$form_t1->selectProvee->opcion}}</option>
                            @foreach($sel_proveedores as $form)
                              @if($form->id != $form_t1->selectProvee->id)
                              <option value="{{$form->id}}">{{$form->opcion}}</option> 
                              @endif
                            @endforeach
                         </select>
                    </div>
                </div>
            </div>
<!--FIN DEL PRIMER ROW DE INPUT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-1 form-group ">
                      <label><b class="requiredV">*</b> </label>
                         <select name="grupo" id="grupo" class="form-control">
                            <option value="1" >J</option>
                            <option value="2" >V</option>
                            <option value="3" >G</option>
                         </select>
                    </div>

                    <div class="col-md-3">
                      <label for="rifProvee">RIF</label>   
                       <input type="text" class="form-control" name="rifProvee" id="rifProvee" value="{{$form_t1->rifProvee}}" maxlength="20"> 
                    </div>
                  
                    <div class="col-md-4 separar">
                      <label for="otraDesc">Otra Descripción</label>

                    @if($form_t1->otraDesc == '0')
                      <input type="text" class="form-control" name="otraDesc" id="otraDesc" value="xxx"  placeholder="Introduzca nombres" maxlength="200" disabled>
                    </div>

                    @else
                    <input type="text" class="form-control" name="otraDesc" id="otraDesc" value="{{$form_t1->otraDesc}}"  placeholder="Introduzca otra descripción" maxlength="200">
                </div> 
            </div>
                    @endif

<!--FIN DEL SEGUNDO ROW DE INPUT-->
              <div class="row">
                  <div class="col-md-12 form-group"><br>
                    <center><button type="submit" class="btn btn-sm btn-info" name="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modificar</b></button>
                     <a href="{{ url('/regProveedores') }}"  class="btn btn-sm btn-danger" ><i class="fa fa-reply " aria-hidden="true" title="Regresar"></i> <b>Regresar</b></a></center>  
                  </div>
              </div>
        </div>
    </div>
</div>
</form>
</body>
</html>

    <script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/validateGeneral.js') }}"></script>
    <script src="{{ asset('js/inputDinamicoanexosT.js') }}"></script>

