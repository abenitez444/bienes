@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12">
       <!--<img src="{{URL::asset('/img/cintillo.jpg')}}" id="banner" >-->
          </div>
    <div class="row separarInput">
        <div class="col-md-8 col-md-offset-2"><br>
            <div class="panel panel">
                <div class="panel-heading"><center><h4><b><i class="fa fa-plus-circle fa-lg"></i> Agregar usuario</b></h4></center></div>
                 <div class="row">
		          <div class="col-md-12 desvanecer">
		            @if(session()->has('msj'))
		              <center><div  class="col-md-12  alert alert-warning" ><b>{{session('msj')}}</b></div></center>
		                @endif

		                @if(session()->has('errormsj'))
		              <center><div  class="col-md-12  alert alert-danger" >{{session('errormsj')}}</div></center>
		            @endif
		          </div>
		      	</div>
                <div class="panel-body">
                	<form action="{{url('/search-user')}}" method="POST" id="formSearchUser" name="formSearchUser">
                        {{ csrf_field() }}
						<div class="row">
							<div class="col-md-4 col-md-offset-4 form-group">
								<input type="text" id="cedula" name="cedula" class="form-control" placeholder="Ingrese cédula de identidad" autofocus>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<center><button type="submit" class="btn btn-success fa fa-search fa-md text-center"> <b>Buscar</b></button></center>
							</div>
						</div>
						<div class="row" style="padding-top:5px;">
							<div class="col-md-12  form-group">
								<center>
									
								</center>
							</div>
						</div>
					</form>

					<div class="container" id="sha">
					<div id="resultado" hidden>
					<div class="row text-center">
						 <div class="panel-heading form-group"><center><h4><b><i class="fa fa-user" aria-hidden="true"></i> Usuario para el inicio de sesión</b></h4></center></div>
						
					</div>
						<div class="row col-md-offset-1 form-group ">
							<br><div class="col-md-6 form-group ">
								<li>Nombres</li>
								<input id="nombres" type="text" disabled>
							</div>

							<div class="col-md-6 form-group ">
								<li>Apellidos</li>
								<input id="apellidos" type="text" disabled>
							</div>
						</div>

						<div class="row col-md-offset-1 form-group">
							<div class="col-md-6 form-group">
								<li>Cédula</li>
								<input id="ci" type="text" disabled>
							</div>
							
							<div class="col-md-6  form-group separar40">
								<li>Cargo</li>
								<input id="cargo" type="text" disabled>
							</div>
				
						<form action="{{url('/add-user')}}" id="formAddUser" name="formAddUser" method="POST">
							{{ csrf_field() }}
							<input type="hidden" id="uid" name="uid">
							
							<div class="row form-group">
								<div class="col-md-8 col-lg-offset-1 form-group">
									<li>Asignar permisología</li>
									<select required name="rol" id="rol" class="form-control">
										<option value="0" disabled selected>Seleccione... </option>
										@foreach($roles as $rol)
											<option value="{{$rol->id}}">{{$rol->opcion}}</option>
										@endforeach									
									</select>
								</div>
							</div>

							<div class="row form-group">
								<div class="col-md-11">
									<center><button type="submit" class="btn btn-success fa fa-share-square fa-md"> <b>Enviar</b></button></center>
								</div>
							</div>
						</form>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection