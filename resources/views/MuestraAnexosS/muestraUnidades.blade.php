
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ficha Sedes</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/appstilo.css') }}" rel="stylesheet">  
        <link href="{{ asset('css/general.css') }}" rel="stylesheet">  
        <link href="{{ asset('css/tablas.css') }}" rel="stylesheet">  
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('img/bandera.png') }}" rel="icon">
    </head>
<body>


<div class="container" id="sha">
    <div class="row">
        <div class="col-md-12">
              <div class="row">
                  <div class="panel">
                        <img src="{{URL::asset('/img/cintillo.jpg')}}" id="banner" alt="Cintillo Web">
                      <div id="panelTitu" class="panel-heading text-center"><h5 id="h5Titu"><b>REGISTRO DE DATOS DE LAS UNIDADES ADMINISTRATIVAS DEL ÓRGANO O ENTE</b> <i class="fa fa-file-text-o" aria-hidden="true"></i></h5></div>
                  </div>   
              </div>

              <div class="row">
                <div class="col-md-12 text-center">
                    <h4>Ficha de Registro <b id="colorInstruccion">N°# {{$seleccion->id}}</b></h4>
                </div>
              </div>
@include('EliminarAnexosS.anularUnidades')
        <hr>
              <div class="row separar40">
                  <div class="col-md-12">
                      
                     	<div class="col-md-4 form-group">
                     	    <label for="codUnidad">Código de la Unidad</label>
                      	    <br>{{$seleccion->codUnidad}}
                     	</div>

                      
                      <div class="col-md-4 form-group">
                          <label for="descUnidad">Descripción de la Unidad</label>
                              <br>{{$seleccion->descUnidad}}
                      </div>  
                    
                      <div class="col-md-4 form-group">
                          <label for="categoria">Código de la Categoría de la Unidad</label>
                              <br>{{$seleccion->categoria}}
                      </div> 
                      
                  </div>
              </div>
            
              <div class="row separar40">
                  <div class="col-md-12">
                      @if($seleccion->espeCatego == '1')
                      <div class="col-md-4 form-group">
                          <label for="espeCatego">Especifique la Denominación de la Categoría</label>
                              <br>noaplica
                      </div>
                      @else
                      <div class="col-md-4 form-group">
                          <label for="espeCatego">Descripción de la Sede</label>
                              <br>{{$seleccion->espeCatego}}
                      </div>  
                      @endif
                    

                      @if($seleccion->codAdscrita == '1')
                      <div class="col-md-4 form-group">
                          <label for="codAdscrita">Código de Unidad a la cual está Adscrita</label>
                              <br>0
                      </div>
                      @else
                      <div class="col-md-4 form-group">
                          <label for="codAdscrita">Código de Unidad a la cual está Adscrita</label>
                              <br>{{$seleccion->codAdscrita}}
                      </div>  
                      @endif
                  </div>
            </div>

            <div class="row text-center separar">
               	<div class="col-md-12 separar form-group">
                    <!--@if($_SESSION['rol'] == 1)
                     <a  class="btn btn-danger"  data-toggle="modal" data-target="#AnularS5" title="AnularS5"  ><i class="fa fa-trash-o" aria-hidden="true"> <b>Eliminar</b></i></a> 
                    @endif-->
                     <a href="{{url('regUnidades')}}"  class="btn btn-success"><i class="fa fa-reply " aria-hidden="true" title="Regresar"></i> <b>Regresar</b></a>
                        
                     <a href="{{url ('unidades/'.$seleccion->id) }}/edit" class="btn btn-info" title="Modificar"><i class="fa fa-pencil-square-o" aria-hidden="true"> <b>Modificar</b></i></a> 
               	</div>
            </div>
        </div>
    </div> 
</div>
</body>
</html>

  <script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
