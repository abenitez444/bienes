<!DOCTYPE html>
<html>
     <head>
	
      	<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
      	<link href="{{ asset('css/general.css') }}" rel="stylesheet" type="text/css">
      	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
      
     </head>

    <body>
	
        	<div class="container">
		        <center><div class="row " id="sha">
			        <div class="col-md-12 separar"><br>
			        	
			        		<h2 id="color403"><b>¡No tienes permisos para ingresar a ésta página!</b></h2><br>
					        <center><img id="imgErrors" src="{{ asset('img/BANER2.jpg') }}"><br><br>
					        <h2 id="color403"><b>Error 403</b></h2><br>
					        <h3 class="errorContac">¡Vaya! Algo salió mal.</h3><br>
					        <h3 class="errorContac">Si el problema persiste, por favor contactar la Dirección General de Tecnología de la Información.</h3></center><br>
					        <h5 class="viceTitulo"> ¬ Vicepresidencia de la República Bolivariana de Venezuela. ¬</h5><br>

					        <a href="{{ url('/home') }}" class="btn btn-warning"><i class="fa fa-reply " aria-hidden="true" title="Regresar"></i> <b>Regresar</b></a>
					    </center>
			        </div>
			    </div>
       		</div>
    </body>
</html>