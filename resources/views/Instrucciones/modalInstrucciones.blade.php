
      <div class="modal fade" id="instrucciones" name="instrucciones" role="dialog" aria-labelledby="ModalLabel" aria-hidden="false" >
          <div class="modal-dialog modal-lg" style="width:80%;">
              <div class="modal-content">
                    <div class="modal-header">
                        
                      <center><h4 class="modal-title"><b> Introducción para la usabilidad del sistema de bienes nacionales</b></h4></center>

                    </div>
                  <div class="modal-body">
                      <p class="form-group">En cada formulario del sistema se encontrará un botón como el siguiente: <img class="" src="{{ asset('img/instruccionesHome.png') }}" style="width:130px;">
                      en donde se desplegará el instructivo detallado correspondiente para el uso correcto del sistema.</p>
                      
                    <p>Estructura del menú</p>

                    <p>En el botón <img src="{{ asset('img/registrarHome.png') }}" style="width:80px; border-radius:5px;"> del menú se podrá realizar la carga de información especificamente en las siguientes pestañas </p>
                    
                    <div>
                      <p><li class="list-unstyled"><b>°</b> Datos Generales de Organos y Entes</li></p>
                      <p><img src="{{ asset('img/datosorganoHome.png') }}" style="width:600px;"></p>
                    </div>

                    <div>
                      <p><li class="list-unstyled"><b>°</b> Datos Generales de Organos y Entes</li></p>
                      <p><img src="{{ asset('img/datosorganoHome.png') }}" style="width:600px;"></p>
                    </div>
                       <center>
                       <button id="btnCancelar" name="btnCancelar" type="button" class="btn btn-danger" data-toggle="tooltip" title="Cancelar" data-dismiss="modal"><b>Cerrar</b></button>
                       </center>
                  </div> 
              </div>                          
          </div>
