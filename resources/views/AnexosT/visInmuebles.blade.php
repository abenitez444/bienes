@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div id="panelTitu" class="panel-heading text-center separar">
                <h5 id="h5Titu"><b> <i class="fa fa-file-o" aria-hidden="true"></i> INMUEBLES / DATOS DE LOS BIENES
                        INMUEBLES DEL ÓRGANO O ENTE</b></h5>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <h6> <i id="colorInstruccion" class="fa fa-info-circle" aria-hidden="true" title=""></i>
                    <b id="colorInstruccion"> INSTRUCCIONES: LOS CAMPOS CON &nbsp; (<b class="aterisInst">*</b>
                        &nbsp;&nbsp;&nbsp;&nbsp; ) SON OBLIGATORIOS. SEGÚN EL MANUAL DE ESPECIFICACIONES TÉCNICAS.</b></h6>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
                @if(session()->has('msj'))
                <center>
                    <div class="col-md-12  alert alert-success">{{session('msj')}}</div>
                </center>
                @endif

                @if(session()->has('errormsj'))
                <center>
                    <div class="col-md-12  alert alert-danger"><b class="fa fa-envelope"></b> {{session('errormsj')}}</div>
                </center>
                @endif
            </div>
        </div>

        <div class="row separar">
            <div class="col-md-12">
                <li style="border-style: ridge; background-color: white; width: 160px;" class="listas"><b id="espaciar2">Último
                        Registro</b>
                    <li style="border-style: ridge; background-color: white; width: 160px;" class="listas"><b id="espaciar4">
                            Código de Origen</b>

                        @if($lastCod)
                        <center id="color"> {{$lastCod->codBien}}</center>
                        @else()
                        <center id="color">I2012001</center>
                        @endif

                    </li>
                </li>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 li">
                <ul class="js-errors li separar"></ul>
            </div>
        </div>
        <hr>

        <form role="form" id="formValidaT12" name="formValidaT12" method="POST" action="{{url('datosinmuebles')}}">
            {{ csrf_field() }}

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b><b>Código del Origen del Bien:</b></li>
                        <input type="text" class="form-control" name="codBien" id="codBien" placeholder="Introduzca el código de origen del bien"
                            maxlength="12">
                    </div>

                    <div class="col-md-4 form-group">
                        <li for="validationCustom01"><b class="requiredV">*</b> Código Según el Catalogo:</li>
                        <select name="codCata" id="codCata" class="buscador form-control">

                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($catalogo as $cata)
                            <option value="{{$cata->id}}">{{$cata->codigo}} - {{$cata->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li for="codUnidad" class="bold block"><b class="requiredV">*</b>Dependencia Administrativa:</li>
                        <select name="codUnidad" id="codUnidad" class="buscador form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @forelse($unidad as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->codUnidad}}</option>
                            @empty
                            <option disabled>Por favor, agregue una unidad administrativa</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Código del Responsable Administrativo:</li>
                        <select name="codRespAdm" id="codRespAdm" class="form-control" disabled>
                            <option value="0">Seleccione</option>
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Bien Inmueble Corresponde a Alguna Sede del Ente:</li>
                        <select name="corresBien" id="corresBien" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($corresBien as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Código de Sede donde Corresponde el Bien:</li>
                        <select name="sedeOrgano" id="sedeOrgano" class="form-control buscador">
                            <option value="0" disabled selected>Seleccione</option>
                            @forelse($sede as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->codSede}}</option>
                            @empty
                            <option selected disabled>Por favor, agregue una sede</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Localización:</li>
                        <select name="localizacion" id="localizacion" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($localizacion as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b> Código del País donde se Ubica la Sede:</li>
                        <select name="codPais" id="codPais" class="buscador form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($selectPais as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->pais}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Especifique el Otro País:</li>
                        <input type="text" id="espeOtroPais" name="espeOtroPais" class="form-control" placeholder="Especifique el Otro País"
                            maxlength="100" disabled>
                    </div>
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b> Código de la Parroquia donde se Ubica:</li>
                        <select name="codParroquia" id="codParroquia" class="buscador form-control" disabled>
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($selectParroquia as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->parroquia}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b> Código de la Ciudad donde se Ubica:</li>
                        <select name="codCiudad" id="codCiudad" class="buscador form-control" disabled>
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($selectCiudad as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->ciudad}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Especifique la Otra Ciudad:</li>
                        <input type="text" id="espeOtroCiudad" name="espeOtroCiudad" class="form-control" placeholder="Especifique la otra ciudad"
                            maxlength="100" disabled>
                    </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Urbanización:</li>
                        <input type="text" id="urbanizacion" name="urbanizacion" class="form-control" placeholder="Especifique la urbanización"
                            maxlength="30">
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Calle / Avenida:</li>
                        <input type="text" id="calleAvenida" name="calleAvenida" class="form-control" placeholder="Especifique la calle o avenida"
                            maxlength="50">
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Casa / Edificio:</li>
                        <input type="text" id="casaEdificio" name="casaEdificio" class="form-control" placeholder="Especifique la casa o el edificio"
                            maxlength="30">
                    </div>
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Código Interno del Bien:</li>
                        <input type="text" id="codInterno" name="codInterno" class="form-control" placeholder="Introduzca el código interno del bien"
                            maxlength="20">
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Estatus del uso del Bien:</li>
                        <select name="estatuBien" id="estatuBien" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($estatusBien as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Especifique el Otro Uso:</li>
                        <input type="text" id="espOtroUso" name="espOtroUso" class="form-control" placeholder="Especifique otro uso del bien"
                            maxlength="100" disabled>
                    </div>
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Valor de Adquisición del Bien:</li>
                        <input type="text" id="valorAdq" name="valorAdq" class="form-control money" placeholder="Introduzca el valor del bien"
                            maxlength="26">
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b><b>Moneda:</b></li>
                        <select name="moneda" id="moneda" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($moneda as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Especifique la Otra Moneda:</li>
                        <input type="text" id="espeMoneda" name="espeMoneda" class="form-control" placeholder="Especifique la otra moneda"
                            maxlength="30" disabled>
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-5 form-group">
                        <li>Fecha de Adquisición del Bien:</li>
                        <div class="input-group">
                            <span class="input-group-addon"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="¡Si se desconoce, deje el campo en blanco!"></i></span>
                            <input type="text" id="feAdqBien" name="feAdqBien" class="form-control calendario fechaplaceholder"
                                placeholder="¡Si se desconoce, deje el campo en blanco!" aria-describedby="inputGroupprimary3Status">
                        </div>
                    </div>

                    <div class="col-md-5 form-group">
                        <li>Fecha de Ingreso del Bien:</li>
                        <div class="input-group">
                            <span class="input-group-addon"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="¡Si se desconoce, deje el campo en blanco!"></i></span>
                            <input type="text" id="feIngBien" name="feIngBien" class="form-control calendario fechaplaceholder"
                                placeholder="¡Si se desconoce, deje el campo en blanco!" aria-describedby="inputGroupprimary3Status">
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-5 form-group">
                        <li><b class="requiredV">*</b><b>Estado del Bien:</b></li>
                        <select name="edoBien" id="edoBien" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($estadoBien as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-5 form-group">
                        <li>Especifique el Otro Estado del Bien:</li>
                        <input type="text" id="espOtroEdo" name="espOtroEdo" class="form-control" placeholder="Especifique el otro estado del bien"
                            maxlength="30" disabled>
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-10 form-estilo">
                        <li>Descripción del Estado del Bien:</li>
                        <textarea name="descEdoBien" id="descEdoBien" class="form-control" maxlength="255" rows="4"></textarea>
                        <div id="negro" for="contador">Caracteres: <div class="rojo" id="conbienes">0/255</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-5 form-group">
                        <li><b class="requiredV">*</b>Uso del Bien Inmueble:</li>
                        <select name="usoBienInmu" id="usoBienInmu" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($usoInmueble as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-5 form-group">
                        <li>Otro Uso:</li>
                        <input type="text" id="otroUsoInmu" name="otroUsoInmu" class="form-control" placeholder="Especifique el otro estado del bien"
                            maxlength="100" disabled>
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-5 form-estilo">
                        <li>Oficina de Registro/Notaria:</li>
                        <textarea name="ofiRegistro" id="ofiRegistro" class="form-control" maxlength="255" rows="4"></textarea>
                        <div id="negro" for="contador">Caracteres: <div class="rojo" id="contaReg">0/255</div>
                        </div>
                    </div>

                    <div class="col-md-5 form-estilo">
                        <li>Referencia del Registro:</li>
                        <textarea name="refRegistro" id="refRegistro" class="form-control" maxlength="255" rows="4"></textarea>
                        <div id="negro" for="contador">Caracteres: <div class="rojo" id="contaRef">0/255</div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Tomo:</li>
                        <input type="text" id="tomo" name="tomo" class="form-control" placeholder="Introzduca el tomo"
                            maxlength="4" onkeypress="return soloNum(event)">
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Folio:</li>
                        <input type="text" id="folio" name="folio" class="form-control" placeholder="Introzduca el folio"
                            maxlength="5" onkeypress="return soloNum(event)">
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Protocolo:</li>
                        <input type="text" id="protocolo" name="protocolo" class="form-control" placeholder="Introzduca el protocolo"
                            maxlength="20">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Número de Registro:</li>
                        <input type="text" id="numRegistro" name="numRegistro" class="form-control" placeholder="Introzduca el número de registro"
                            maxlength="20" onkeypress="return soloNum(event)">
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Fecha de Registro:</li>
                        <div class="input-group">
                            <span class="input-group-addon"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="¡Si se desconoce, deje el campo en blanco!"></i></span>
                            <input type="text" id="feRegistro" name="feRegistro" class="form-control calendario fechaplaceholder"
                                placeholder="¡Si se desconoce, deje el campo en blanco!" aria-describedby="inputGroupprimary3Status">
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Propietario Anterior:</li>
                        <input type="text" id="propieAnt" name="propieAnt" class="form-control" placeholder="Introzduca el propietario anterior"
                            maxlength="200">
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-10 form-estilo">
                        <li>Dependencias que lo Integran:</li>
                        <textarea name="depenIntegra" id="depenIntegra" class="form-control" maxlength="255" rows="4"></textarea>
                        <div id="negro" for="contador">Caracteres: <div class="rojo" id="contaDepen">0/255</div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Área de Construcción:</li>
                        <input type="text" id="areaConstru" name="areaConstru" class="form-control money" placeholder="Indique el área de construcción"
                            maxlength="22">
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Unidad de Medida del Área Construcción:</li>
                        <select name="unidadConstru" id="unidadConstru" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($unidadConstru as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Especifique la Otra Unidad de Medida (Construcción):</li>
                        <input type="text" id="espeOtraUnidad" name="espeOtraUnidad" class="form-control" placeholder="Especifique la otra unidad del área de construcción"
                            maxlength="100">
                    </div>
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <li>Área del Terreno:</li>
                        <input type="text" id="areaTerreno" name="areaTerreno" class="form-control money" placeholder="Indique el área de terreno"
                            maxlength="22">
                    </div>

                    <div class="col-md-4 form-group">
                        <li><b class="requiredV">*</b>Unidad de Medida del Área del Terreno:</li>
                        <select name="unidadTerreno" id="unidadTerreno" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($unidadConstru as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <li>Especifique la Otra Unidad de Medida (Terreno):</li>
                        <input type="text" id="espeOtraTerre" name="espeOtraTerre" class="form-control" placeholder="Especifique la otra unidad del área del terreno"
                            maxlength="100">
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-10 form-estilo">
                        <li>Otras Especificaciones:</li>
                        <textarea name="otrasEspecifi" id="otrasEspecifi" class="form-control" maxlength="255" rows="4"></textarea>
                        <div id="negro" for="contador">Caracteres: <div class="rojo" id="contaEspe">0/255</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row separar col-md-offset-1">
                <div class="col-md-12">
                    <div class="col-md-5 form-group">
                        <li><b class="requiredV">*</b>Se Encuentra el Bien Asegurado:</li>
                        <select name="seguroBien" id="seguroBien" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($seguroBien as $traeDir)
                            <option value="{{$traeDir->id}}">{{$traeDir->opcion}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-5 separar form-group">
                        <li>Código del Registro de Seguro:</li>
                        <input type="text" id="codRegSeguro" name="codRegSeguro" class="form-control" placeholder="Introduzca el código del registro del seguro"
                            maxlength="10">
                    </div>
                </div>
            </div>

            <input type="hidden" id="parroIn" value="{{url('parroIn')}}">
            <input type="hidden" id="ciudadIn" value="{{url('ciudadIn')}}">
            <input type="hidden" id="paisIn" value="{{url('paisIn')}}">

            <div class="row">
                <div class="col-md-12 form-group"><br>
                    <center>
                        <button type="submit" value="Submit" class="btn btn-md btn-success" name="#"><i class="fa fa-check-square-o"
                                aria-hidden="true"></i><b> Enviar</b></button>

                        <a href="home" class="btn btn-md btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                            <b>Salir</b></a>
                    </center>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection