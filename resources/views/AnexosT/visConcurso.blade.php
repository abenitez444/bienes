@extends('layouts.app')

@section('content')

<div class="row ">
    <div class="col-md-12">
        <div class="row form-group">
            <div id="panelTitu" class="panel-heading text-center">
                <h5 id="h5Titu"><b> <i class="fa fa-file-o" aria-hidden="true"></i> CONCURSO / DATOS DE LOS ORÍGENES
                        (FORMAS DE ADQUISICIÓN) DE LOS BIENES MUEBLES E INMUEBLES DEL ÓRGANO O ENTE .</b></h5>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h6><b>A) APLICABLE SOLO PARA LAS FORMAS DE ADQUISICIÓN DE COMPRA POR CONCURSO ABIERTO O CONCURSO
                        CERRADO.</b></h6>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <h6> <i id="colorInstruccion" class="fa fa-info-circle" aria-hidden="true" title=""></i>
                    <b id="colorInstruccion"> INSTRUCCIONES: LOS CAMPOS CON &nbsp; (<b class="aterisInst">*</b>
                        &nbsp;&nbsp;&nbsp;&nbsp; ) SON OBLIGATORIOS. SEGÚN EL MANUAL DE ESPECIFICACIONES TÉCNICAS.</b></h6>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 desvanecer">
                @if(session()->has('msj'))
                <center>
                    <div class="col-md-12  alert alert-success">{{session('msj')}}</div>
                </center>
                @endif

                @if(session()->has('errormsj'))
                <center>
                    <div class="col-md-12  alert alert-danger">{{session('errormsj')}}</div>
                </center>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <li style="border-style: ridge; background-color: white; width: 160px;" class="listas"><b id="espaciar2">Último
                        Registro</b>
                <li style="border-style: ridge; background-color: white; width: 160px;" class="listas"><b id="espaciar4">
                        Código de Origen</b>

                    @if($lastCod)
                        @if($lastCod->codOrigen == 'A-1')
                          <center><b id="color"> A-1 </b></center>
                        @else
                          <center><b id="color"> {{$lastCod->codOrigen}}</b></center>
                        @endif
                    @else
                      <center><b id="color">A2012001</b></center>
                    @endif
                    
                </li>
                </li>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12 li moverIzq">
                <ul class="js-errors li"></ul>
            </div>
        </div>

        <form role="form" id="formValidaT2" name="formValidaT2" method="POST" action="{{url('concurso')}}">
            {{ csrf_field() }}

            <!--ARRAY SELECT PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => mig_selectT2 Y T2-->
            <!--ARRAY SELECT BELOW TO CONTROLADORT2, RELATED TABLE IN BD => mig_selectT2 AND T2-->

            <div class="row">
                <div class="col-md-12">
                            
                    @foreach($codigoOrigen as $posicion => $valor)

                    <div class="col-md-4  form-group separar40">

                        <label for="{{$codigoOrigen[$posicion][0]}}">{{$codigoOrigen[$posicion][1]}}</label>

                        <input type="text" class="form-control" name="{{$codigoOrigen[$posicion][0]}}" id="{{$codigoOrigen[$posicion][0]}}"
                            placeholder="{{$codigoOrigen[$posicion][2]}}" maxlength="{{$codigoOrigen[$posicion][3]}}">

                    </div>

                    <!--ARRAY DE FECHA datet2 PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE datet2 BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @endforeach

                    @foreach($codigoAdq as $posicion => $valor)
                    <div class="col-md-4 {{$codigoAdq[$posicion][2]}} form-group separar40">
                        <label for="{{$codigoAdq[$posicion][0]}}"><b class="requiredV">*</b> {{$codigoAdq[$posicion][1]}}</label>
                        <select name="{{$codigoAdq[$posicion][0]}}" id="{{$codigoAdq[$posicion][0]}}" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($infoSelect as $traeSelect)
                            <option value="{{$traeSelect->id}}">{{$traeSelect->opcion}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endforeach

                    <!--ARRAY DE INPUT-TEXT PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF INPUT-TEXT BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @foreach($nombreConcu as $posicion => $valor)

                    <div class="col-md-4 form-group separar40">

                        <label for="{{$nombreConcu[$posicion][0]}}">{{$nombreConcu[$posicion][1]}}</label>

                        <input type="text" class="form-control" name="{{$nombreConcu[$posicion][0]}}" id="{{$nombreConcu[$posicion][0]}}"
                            placeholder="{{$nombreConcu[$posicion][2]}}" maxlength="{{$nombreConcu[$posicion][3]}}">

                    </div>

                    <!--ARRAY DE FECHA datet2 PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE datet2 BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @endforeach
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    @foreach($numeroConcu as $posicion => $valor)

                    <div class="col-md-4 form-group separar40">

                        <label for="{{$numeroConcu[$posicion][0]}}">{{$numeroConcu[$posicion][1]}}</label>

                        <input type="text" class="form-control" name="{{$numeroConcu[$posicion][0]}}" id="{{$numeroConcu[$posicion][0]}}"
                            placeholder="{{$numeroConcu[$posicion][2]}}" maxlength="{{$numeroConcu[$posicion][3]}}">

                    </div>

                    <!--ARRAY DE FECHA datet2 PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE datet2 BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @endforeach

                    @foreach($fechaConcu as $posicion => $valor)

                    <div class="col-md-4 form-group">
                        <label for="{{$fechaConcu[$posicion][0]}}">{{$fechaConcu[$posicion][1]}}</label>
                        <div class="{{$fechaConcu[$posicion][3]}}">

                            <span class="{{$fechaConcu[$posicion][4]}}"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="{{$fechaConcu[$posicion][2]}}"></i></span>

                            <input type="text" class="form-control fechaplaceholder calendario" onkeypress="return disable(event)"
                                name="{{$fechaConcu[$posicion][0]}}" id="{{$fechaConcu[$posicion][0]}}" placeholder="{{$fechaConcu[$posicion][2]}}"
                                aria-describedby="{{$fechaConcu[$posicion][5]}}">

                        </div>
                    </div>

                    @endforeach

                    @foreach($codProveedor as $posicion => $valor)
                    <div class="col-md-4 {{$codProveedor[$posicion][2]}} form-group separar40">
                        <label for="{{$codProveedor[$posicion][0]}}"><b class="requiredV">*</b> {{$codProveedor[$posicion][1]}}</label>
                        <select name="{{$codProveedor[$posicion][0]}}" id="{{$codProveedor[$posicion][0]}}" class="form-control">
                            <option value="0" disabled selected>Seleccione</option>
                            @foreach($infoProvee as $traeSelect)
                            <option value="{{$traeSelect->id}}">{{$traeSelect->codProvee}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endforeach
                </div>
            </div>

            <!--ARRAY DE FECHA fechaContra PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
            <!--ARRAY OF DATE fechaContra BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->
            <div class="row separar">
                <div class="col-md-12">
                    @foreach($numContra as $posicion => $valor)

                    <div class="col-md-4 form-group separar40">

                        <label for="{{$numContra[$posicion][0]}}">{{$numContra[$posicion][1]}}</label>

                        <input type="text" class="form-control" name="{{$numContra[$posicion][0]}}" id="{{$numContra[$posicion][0]}}"
                            placeholder="{{$numContra[$posicion][2]}}" maxlength="{{$numContra[$posicion][3]}}">

                    </div>

                    <!--ARRAY DE FECHA datet2 PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE datet2 BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @endforeach

                    @foreach($fechaContra as $posicion => $valor)

                    <div class="col-md-4 form-group">
                        <label for="{{$fechaContra[$posicion][0]}}">{{$fechaContra[$posicion][1]}}</label>
                        <div class="{{$fechaContra[$posicion][3]}}">

                            <span class="{{$fechaContra[$posicion][4]}}"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="{{$fechaContra[$posicion][2]}}"></i></span>

                            <input type="text" class="form-control fechaplaceholder calendario" onkeypress="return disable(event)"
                                name="{{$fechaContra[$posicion][0]}}" id="{{$fechaContra[$posicion][0]}}" placeholder="{{$fechaContra[$posicion][2]}}"
                                aria-describedby="{{$fechaContra[$posicion][5]}}">

                        </div>
                    </div>

                    @endforeach

                    <!--ARRAY DE FECHA fechaContra PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE fechaContra BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @foreach($numEntrega as $posicion => $valor)

                    <div class="col-md-4 form-group separar40">

                        <label for="{{$numEntrega[$posicion][0]}}">{{$numEntrega[$posicion][1]}}</label>

                        <input type="text" class="form-control" name="{{$numEntrega[$posicion][0]}}" id="{{$numEntrega[$posicion][0]}}"
                            placeholder="{{$numEntrega[$posicion][2]}}" maxlength="{{$numEntrega[$posicion][3]}}">

                    </div>

                    <!--ARRAY DE FECHA datet2 PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE datet2 BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @endforeach
                </div>
            </div>

            <div class="row separar">
                <div class="col-md-12">

                    @foreach($fechaEntrega as $posicion => $valor)

                    <div class="col-md-4 form-group">
                        <label for="{{$fechaEntrega[$posicion][0]}}">{{$fechaEntrega[$posicion][1]}}</label>
                        <div class="{{$fechaEntrega[$posicion][3]}}">

                            <span class="{{$fechaEntrega[$posicion][4]}}"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="{{$fechaEntrega[$posicion][2]}}"></i></span>

                            <input type="text" class="form-control fechaplaceholder calendario" onkeypress="return disable(event)"
                                name="{{$fechaEntrega[$posicion][0]}}" id="{{$fechaEntrega[$posicion][0]}}" placeholder="{{$fechaEntrega[$posicion][2]}}"
                                aria-describedby="{{$fechaEntrega[$posicion][5]}}">

                        </div>
                    </div>

                    @endforeach

                    @foreach($numeroFactura as $posicion => $valor)

                    <div class="col-md-4 form-group separar40">

                        <label for="{{$numeroFactura[$posicion][0]}}">{{$numeroFactura[$posicion][1]}}</label>

                        <input type="text" class="form-control" name="{{$numeroFactura[$posicion][0]}}" id="{{$numeroFactura[$posicion][0]}}"
                            placeholder="{{$numeroFactura[$posicion][2]}}" maxlength="{{$numeroFactura[$posicion][3]}}">

                    </div>

                    <!--ARRAY DE FECHA datet2 PERTENECIENTE AL CONTROLADORT2, TABLA RELACIONADA EN LA BD => T2-->
                    <!--ARRAY OF DATE datet2 BELONGING TO CONTROLADORT2, TABLE RELATED IN THE BD => T2-->

                    @endforeach

                    @foreach($fechaFactura as $posicion => $valor)

                    <div class="col-md-4 form-group">
                        <label for="{{$fechaFactura[$posicion][0]}}">{{$fechaFactura[$posicion][1]}}</label>
                        <div class="{{$fechaFactura[$posicion][3]}}">

                            <span class="{{$fechaFactura[$posicion][4]}}"><i style="color:#8E2121;" class="fa fa-info-circle"
                                    aria-hidden="true" title="{{$fechaFactura[$posicion][2]}}"></i></span>

                            <input type="text" class="form-control fechaplaceholder calendario" onkeypress="return disable(event)"
                                name="{{$fechaFactura[$posicion][0]}}" id="{{$fechaFactura[$posicion][0]}}" placeholder="{{$fechaFactura[$posicion][2]}}"
                                aria-describedby="{{$fechaFactura[$posicion][5]}}">

                        </div>
                    </div>

                    @endforeach

                </div>
            </div>
            <!-- FINAL DE LOS 3 FOREACH $arrayt2 $datet2 $date3t2 DEL ROW GENERAL -->

            <div class="row separar">
                <div class="col-md-12 form-group"><br>
                    <center>
                        <button type="submit" class="btn btn-md btn-success" name="#"><i class="fa fa-check-square-o"
                                aria-hidden="true"></i><b> Enviar</b></button>

                        <a href="home" class="btn btn-md btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i><b>
                                Salir</b></a>
                    </center>
                </div>
            </div>
        </form>
        <!-- FIN CONTAINER -->
    </div>
</div>
@endsection