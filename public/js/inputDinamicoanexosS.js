//ANEXO S y T ---> visS4 visInmuebles

    $("#tipoSede").change(function(){
       var id = $("#tipoSede").val();
       if(id != 8){
          $("#espeSede").attr("disabled", true).append($('#espeSede').val('noaplica'));
       }else{
          $("#espeSede").attr("disabled", false).append($('#espeSede').val(''));
    
       }
    })


    $("#codPaiS4").change(function(){
      var id = $("#codPaiS4").val();
      if(id != 239){
         $("#espeOtroPais").attr("disabled", true).append($('#espeOtroPais').val('noaplica'));

         $("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));

      }else{
         $("#espeOtroPais").attr("disabled", false).append($('#espeOtroPais').val(''));

      }
    })

    $("#codCiudadS4").change(function(){
      var id = $("#codCiudadS4").val();
      if(id != 331){

        $("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));
      }else{
        $("#espeOtroCiudad").attr("disabled", false).append($('#espeOtroCiudad').val(''));
    
       }
    })

    /*===========================================*/
  /*======Scrip para la vista Modificar sedes======*/

       $("#codPais2").change(function(){
      var id = $("#codPais2").val();
      if(id != 239){
         $("#espeOtroPais2").attr("disabled", true).append($('#espeOtroPais2').val('noaplica'));

         //$("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));

      }else{
         $("#espeOtroPais2").attr("disabled", false).append($('#espeOtroPais2').val(''));
      }
    })

    $("#codCiudad2").change(function(){
      var id = $("#codCiudad2").val();
      if(id != 331){
         $("#espeOtroCiudad2").attr("disabled", true).append($('#espeOtroCiudad2').val('noaplica'));

      }else{
         $("#espeOtroCiudad2").attr("disabled", false).append($('#espeOtroCiudad2').val(''));
      }
    })

    /*==============Input dinamico DATOS DE LAS UNIDADES AnexoS S============*/

    $("#categoria").change(function(){
      var id = $("#categoria").val();
      if(id != 20){
         $("#espeCatego").attr("disabled", true).append($('#espeCatego').val('noaplica'));

      }else{
         $("#espeCatego").attr("disabled", false).append($('#espeCatego').val(''));
      }
    })

//SELECT LOCALIZACIÓN Y SELECT si se selecciona nacional te trae venezuela si se selecciona internacional te trae otro país CODIGO DE PARROQUIA PARA CUANDO SELECCIONE INTERNACIONAL SE COLOQUE 99 EN CASO CONTRARIO TRAIGA LAS OPCIONES DEL OPTION(SELECT)*/  
<!---->
     $("#localizacionS4").on("change", function(){
      var id = $("#localizacionS4").val();
        var parroS4 = $('#parroS4').val();
          var ciudadS4 = $('#ciudadS4').val();
            var paisesS4 = $('#paisesS4').val();

        if(id == 1){

          $.get(parroS4+'/'+id+"",function(response,parroquia){
             $("#codParroquiaS4").empty();
                $("#codParroquiaS4").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<1093; i++){
                  $("#codParroquiaS4").append("<option value='"+response[i].id+ "'> "+response[i].parroquia+"</option>");
                }

            })

          $.get(ciudadS4+'/'+id+"",function(response,ciudad){
             $("#codCiudadS4").empty();
                $("#codCiudadS4").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<331; i++){
                  $("#codCiudadS4").append("<option value='"+response[i].id+ "'> "+response[i].ciudad+"</option>");
                }

            })
 
              $("#codPaiS4").empty().append("<option value='230'>Venezuela </option>");
              $("#espeOtroPais").attr("disabled", true).append($('#espeOtroPais').val('noaplica'));
              $("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));
              $("#codParroquiaS4").attr("disabled", false);
              $("#codCiudadS4").attr("disabled", false);

                    
          }else{
       

             $.get(paisesS4+'/'+id+"",function(response,pais){
              $("#codPaiS4").empty();
                $("#codPaiS4").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<response.length; i++){
                  $("#codPaiS4").append("<option value='"+response[i].id+ "'> "+response[i].pais+"</option>");
                 }

             })
            
              $("#codPaiS4").empty().append("<option value='2'>Otro País </option>");
              $("#codParroquiaS4").attr("disabled", false);
              $("#codParroquiaS4").empty().append("<option value='1094'>99 </option>");
              $("#codCiudadS4").attr("disabled", false);
              $("#codCiudadS4").empty().append("<option value='332'>99 </option>");
              $("#espeOtroPais").attr("disabled", false).append($('#espeOtroPais').val(''));
              $("#espeOtroCiudad").attr("disabled", false).append($('#espeOtroCiudad').val(''));

            }

      })

//Modificar visSedes = S4
$("#codPais2").change(function(){
      var id = $("#codPais2").val();
      if(id != 239){
         $("#espeOtroPais").attr("disabled", true).append($('#espeOtroPais').val('noaplica'));

      }else{
        $("#espeOtroPais").attr("disabled", false).append($('#espeOtroPais').val(''));
      }
    })

    $("#codCiudad2").change(function(){
      var id = $("#codCiudad2").val();
      if(id != 331){
        $("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));
      }else{
        $("#espeOtroCiudad").attr("disabled", false).append($('#espeOtroCiudad').val(''));
    
       }
    })

//MODIFICAR visSedes = s4
$("#localizacion2").change(function(event){

      var id = $("#localizacion2").val();
        var s4Parro2 = $('#s4Parro2').val();
          var s4Ciudad2 = $('#s4Ciudad2').val();
            var s4Pais2 = $('#s4Pais2').val();
        if(id == 1){
        
          $.get(s4Parro2+'/'+id+"",function(response,parroquia){
             $("#codParroquia2").empty();
                $("#codParroquia2").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<1093; i++){
                  $("#codParroquia2").append("<option value='"+response[i].id+ "'> "+response[i].parroquia+"</option>");
        }

            })

          
          $.get(s4Ciudad2+'/'+id+"",function(response,ciudad){
             $("#codCiudad2").empty();
                $("#codCiudad2").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<331; i++){
                  $("#codCiudad2").append("<option value='"+response[i].id+ "'> "+response[i].ciudad+"</option>");
                }

            })
       
              $("#codPais2").empty().append("<option value='230'>Venezuela </option>");
              $("#espeOtroPais2").attr("disabled", true).append($('#espeOtroPais2').val('noaplica'));
              $("#espeOtroCiudad2").attr("disabled", true).append($('#espeOtroCiudad2').val('noaplica'));
              $("#codParroquia2").attr("disabled", false);
              $("#codCiudad2").attr("disabled", false);
            
       
          }else{
            
             $.get(s4Pais2+'/'+id+"",function(response,pais){
              $("#codPais2").empty();
                $("#codPais2").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<response.length; i++){
                  $("#codPais2").append("<option value='"+response[i].id+ "'> "+response[i].pais+"</option>");
                }

            })
            
              $("#codPais2").empty().append("<option value='2'>Otro País </option>");
              $("#espeOtroPais2").attr("disabled", false).append($('#espeOtroPais2').val(''));
              $("#espeOtroCiudad2").attr("disabled", false).append($('#espeOtroCiudad2').val(''));
              $("#codParroquia2").attr("disabled", false);
              $("#codParroquia2").empty().append("<option value='1093'>99 </option>");
              $("#codCiudad2").attr("disabled", false);
              $("#codCiudad2").empty().append("<option value='332'>99 </option>");
              
             
            }

      })

      




 





 

