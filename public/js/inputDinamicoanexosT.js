
    // PROVEEDORES = Anexo T1
    $('#tipProvee').change(function(){
        var id = $('#tipProvee').val();
        if(id != 2){
           $("#otraDesc").attr("disabled", true).append($('#otraDesc').val('xxx'));

        }else{
            $("#otraDesc").attr("disabled", false).append($('#otraDesc').val(''));
        }
    })

     //SEGUROS = ANEXOS T3
     $("#tipoCobe").change(function(){
       var id = $("#tipoCobe").val();
       if(id != 3){
          $("#espeCobe").attr("disabled", true).append($('#espeCobe').val('noaplica'));
       }else{
          $("#espeCobe").attr("disabled", false).append($('#espeCobe').val(''));
    
        }
    })

     $("#compAse").change(function(){
       var id = $("#compAse").val();
       if(id != 54){
          $("#otraCom").attr("disabled", true).append($('#otraCom').val('noaplica'));
       }else{
          $("#otraCom").attr("disabled", false).append($('#otraCom').val(''));
    
        }
    })

    //ANEXOS BIENES , TRANSPORTE , SEMOVIENTES , INMUEBLES
    // moneda y espeMoneda js presente en las siguientes visBienes = Anexo T8 
    // visTransporte = Anexo T9 visSemovientes = Anexo T10 , visInmuebles = Anexo T12
    $("#moneda").change(function(){
      var id = $("#moneda").val();
        if(id != 4){
          $("#espeMoneda").attr("disabled", true).append($('#espeMoneda').val('noaplica'));
       }else{
          $("#espeMoneda").attr("disabled", false).append($('#espeMoneda').val(''));
    
        }
    })

    //ANEXOS BIENES , TRANSPORTE , SEMOVIENTES , INMUEBLES
    //estatuBien y espOtroUso |edoBien y espOtroEdo js presente en las siguientes visBienes = Anexo T8 , visTransporte = Anexo T9 , 
    //visSemovientes = Anexo T10 , visInmuebles = Anexos T12
    $("#estatuBien").change(function(){
        var id = $("#estatuBien").val();
        if(id != 11){
          $("#espOtroUso").attr("disabled", true).append($('#espOtroUso').val('noaplica'));
        }else{
          $("#espOtroUso").attr("disabled", false).append($('#espOtroUso').val(''));
    
      }
    })

    //ANEXOS BIENES , TRANSPORTE , SEMOVIENTES , INMUEBLES
    //edoBien y espOtroEdo | js presente en las siguientes visBienes = Anexo T8 , visTransporte = Anexo T9 , 
    //visSemovientes = Anexo T10 , visInmuebles = Anexos T12
    $("#edoBien").change(function(){
        var id = $("#edoBien").val();
        if(id != 7){
      $("#espOtroEdo").attr("disabled", true).append($('#espOtroEdo').val('noaplica'));
        }else{
      $("#espOtroEdo").attr("disabled", false).append($('#espOtroEdo').val(''));
    
        }
    })

  //ANEXOS BIENES , TRANSPORTE , SEMOVIENTES 
  //codColorBien y espeColor | js presente en las siguientes visBienes = Anexo T8 , visTransporte = Anexo T9 , 
  //visSemovientes = Anexo T10 
 
    $("#codColorBien").change(function(){
      var id = $("#codColorBien").val();
        if(id != 37){
      $("#espeColor").attr("disabled", true).append($('#espeColor').val('noaplica'));
        }else{
      $("#espeColor").attr("disabled", false).append($('#espeColor').val(''));
    
        }
    })


//ANEXO TRANSPORTE

  
    $("#tieneSistema").change(function(){
      var id = $("#tieneSistema").val();
        if(id != 1){
      $("#espeSistema").attr("disabled", true).append($('#espeSistema').val('noaplica'));
       }else{
      $("#espeSistema").attr("disabled", false).append($('#espeSistema').val(''));
    
        }
    })

    $("#claseBien").change(function(){
      var id = $("#claseBien").val();
        if(id != 4){
      $("#espeClase").attr("disabled", true).append($('#espeClase').val('noaplica'));
       }else{
      $("#espeClase").attr("disabled", false).append($('#espeClase').val(''));
    
        }
    })

    $("#seguroBien").change(function(){
      var id = $("#seguroBien").val();
        if(id != 1){
      $("#codRegSeguro").attr("disabled", true).append($('#codRegSeguro').val('99'));
       }else{
      $("#codRegSeguro").attr("disabled", false).append($('#codRegSeguro').val(''));
    
        }
    })


//ANEXO SEMOVIENTES


    $("#tipoAnimal").change(function(){
       var id = $("#tipoAnimal").val();
       if(id != 8){
          $("#espeOtroTipo").attr("disabled", true).append($('#espeOtroTipo').val('noaplica'));
       }else{
          $("#espeOtroTipo").attr("disabled", false).append($('#espeOtroTipo').val(''));
    
        }
    })


      $("#proposito").change(function(){
        var id = $("#proposito").val();
        if(id != 7){
          $("#espeOtroPro").attr("disabled", true).append($('#espeOtroPro').val('noaplica'));
        }else{
          $("#espeOtroPro").attr("disabled", false).append($('#espeOtroPro').val(''));
    
       }
    })


//ANEXO INMUEBLES


      $("#usoBienInmu").change(function(){
        var id = $("#usoBienInmu").val();
        if(id != 7){
          $("#otroUsoInmu").attr("disabled", true).append($('#otroUsoInmu').val('noaplica'));
        }else{
          $("#otroUsoInmu").attr("disabled", false).append($('#otroUsoInmu').val(''));
    
        }
    })


      $("#unidadConstru").change(function(){
        var id = $("#unidadConstru").val();
        if(id != 18){
          $("#espeOtraUnidad").attr("disabled", true).append($('#espeOtraUnidad').val('noaplica'));
        }else{
          $("#espeOtraUnidad").attr("disabled", false).append($('#espeOtraUnidad').val(''));
    
        }
    })


      $("#unidadTerreno").change(function(){
      var id = $("#unidadTerreno").val();
        if(id != 18){
      $("#espeOtraTerre").attr("disabled", true).append($('#espeOtraTerre').val('noaplica'));
        }else{
      $("#espeOtraTerre").attr("disabled", false).append($('#espeOtraTerre').val(''));
    
        }
    })

      /*=============Modificar Otro uso o localidad en la vista inmuebles=============*/
          /*=============================///=================================*/

    $("#codPais").change(function(){
      var id = $("#codPais").val();
      if(id != 239){
         $("#espeOtroPais").attr("disabled", true).append($('#espeOtroPais').val('noaplica'));

         //$("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));

      }else{
         $("#espeOtroPais").attr("disabled", false).append($('#espeOtroPais').val(''));
      }
    })

    $("#codCiudad").change(function(){
      var id = $("#codCiudad").val();
      if(id != 331){
         $("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));

         //$("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));

      }else{
         $("#espeOtroCiudad").attr("disabled", false).append($('#espeOtroCiudad').val(''));
      }
    })

    /*=============Modificar Otro uso o localidad en la vista inmuebles=============*/
          /*=============================///=================================*/

    $("#codPais3").change(function(){
      var id = $("#codPais3").val();
      if(id != 239){
         $("#espeOtroPais3").attr("disabled", true).append($('#espeOtroPais3').val('noaplica'));

         //$("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));

      }else{
         $("#espeOtroPais3").attr("disabled", false).append($('#espeOtroPais3').val(''));
      }
    })

    $("#codCiudad3").change(function(){
      var id = $("#codCiudad3").val();
      if(id != 331){
         $("#espeOtroCiudad3").attr("disabled", true).append($('#espeOtroCiudad3').val('noaplica'));

      }else{
         $("#espeOtroCiudad3").attr("disabled", false).append($('#espeOtroCiudad3').val(''));
      }
    })
    
    /*=============================///=================================*/
    /*=============================///=================================*/

      //REGISTRAR INPUT DE VISTA INMUEBLES

      $("#localizacion").change(function(event){

      var id = $("#localizacion").val();
        var parroIn = $('#parroIn').val();
          var ciudadIn = $('#ciudadIn').val();
            var paisIn = $('#paisIn').val();
        if(id == 1){
          
          $.get(parroIn+'/'+id+"",function(response,parroquia){
             $("#codParroquia").empty();
                $("#codParroquia").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<1093; i++){
                  $("#codParroquia").append("<option value='"+response[i].id+ "'> "+response[i].parroquia+"</option>");
                }

            })

          
          $.get(ciudadIn+'/'+id+"",function(response,ciudad){
             $("#codCiudad").empty();
                $("#codCiudad").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<331; i++){
                  $("#codCiudad").append("<option value='"+response[i].id+ "'> "+response[i].ciudad+"</option>");
                }

            })

              $("#codPais").empty().append("<option value='230'>Venezuela </option>");
              $("#espeOtroPais").attr("disabled", true).append($('#espeOtroPais').val('noaplica'));
              $("#espeOtroCiudad").attr("disabled", true).append($('#espeOtroCiudad').val('noaplica'));
              $("#codParroquia").attr("disabled", false);
              $("#codCiudad").attr("disabled", false);
            
              //$("#espeOtroPais").attr("disabled", true).prop($('#espeOtroPais').val('noaplica'));
             
       
          }else{

             $.get(paisIn+'/'+id+"",function(response,pais){
              $("#codPais").empty();
                $("#codPais").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<response.length; i++){
                  $("#codPais").append("<option value='"+response[i].id+ "'> "+response[i].pais+"</option>");
                }

            })
            
              /*$("#codPais").empty().append("<option value='2'>Otro País </option>");*/

              $("#codParroquia").attr("disabled", false);
              $("#codParroquia").empty().append("<option value='1094'>99 </option>");
              $("#codCiudad").attr("disabled", false);
              $("#codCiudad").empty().append("<option value='332'>99 </option>");
              $("#espeOtroPais").attr("disabled", false).append($('#espeOtroPais').val(''));
              $("#espeOtroCiudad").attr("disabled", false).append($('#espeOtroCiudad').val(''));
              
             
            }

      })
  

//MODIFICAR INPUT DE INMUEBLES
    $("#localizacion3").change(function(event){

      var id = $("#localizacion3").val();
        var parroModi = $('#parroModi').val();
          var ciudadModi = $('#ciudadModi').val();
            var paisModi = $('#paisModi').val();
        if(id == 1){
        
          $.get(parroModi+'/'+event.target.value+"",function(response,parroquia){
             $("#codParroquia3").empty();
                  $("#codParroquia3").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<1093; i++){
                  $("#codParroquia3").append("<option value='"+response[i].id+ "'> "+response[i].parroquia+"</option>");
        }

            })

          
          $.get(ciudadModi+'/'+id+"",function(response,ciudad){
             $("#codCiudad3").empty();
                  $("#codCiudad3").append("<option value='0' disabled selected>Seleccione... </option>");
                    for(i=0; i<331; i++){
                  $("#codCiudad3").append("<option value='"+response[i].id+ "'> "+response[i].ciudad+"</option>");
        }

            })

              $("#codPais3").empty().append("<option value='230'>Venezuela </option>");
              $("#espeOtroCiudad3").attr("disabled", true).append($('#espeOtroCiudad3').val('noaplica'));
              $("#espeOtroPais3").attr("disabled", true).append($('#espeOtroPais3').val('noaplica'));
              $("#codParroquia3").attr("disabled", false);
              $("#codCiudad3").attr("disabled", false);
            
      
          }else{

            
             $.get(paisModi+'/'+id+"",function(response,pais){
              $("#codPais3").empty();
                $("#codPais3").append("<option value='0' disabled selected>Seleccione... </option>");
                 for(i=0; i<response.length; i++){
                  $("#codPais3").append("<option value='"+response[i].id+ "'> "+response[i].pais+"</option>");
        }

            })
            
              $("#codPais").empty().append("<option value='239'>Otro País </option>");
             
             
              $("#codParroquia3").empty().append("<option value='1094'>99 </option>");
              $("#codCiudad3").attr("disabled", false);
              $("#codCiudad3").empty().append("<option value='332'>99 </option>");
              $("#espeOtroCiudad3").attr("disabled", false).append($('#espeOtroCiudad3').val(''));
              $("#espeOtroPais3").attr("disabled", false).append($('#espeOtroPais3').val(''));
              
             
            }

      })
  

