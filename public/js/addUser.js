  $('#formSearchUser').submit(function(e){
      e.preventDefault()
      $.ajax({
        url: $(this).attr("action"),
        method: $(this).attr("method"),
        data: $('#formSearchUser').serialize(),
        success: function(response) {
          $('#nombres').val(response[0].givenname[0]);
          $('#apellidos').val(response[0].sn[0]);
          $('#cargo').val(response[0].title[0]);
          $('#ci').val(response[0].employeenumber[0]);
          $('#uid').val(response[0].uid[0]);
          $('#resultado').removeAttr('hidden');
          
        },error: function(error) {
          
          Swal.fire({
            type: 'warning',
            title: error.responseJSON.message,
          });

           $('#resultado').attr('hidden',true);
           $('#cedula').val('');

        }
      })
      
    })

    $('#formAddUser').submit(async function(e){
      e.preventDefault()
      const { value: password } = await Swal.fire({
        title: 'Ingrese su contraseña',
        input: 'password',
        inputPlaceholder: 'Contraseña',
        inputAttributes: {
          maxlength: 30,
          autocapitalize: 'off',
          autocorrect: 'off'
        }
      })

      if (password) {
        console.log(password);

        validatePass(password);

      }else{
        Swal.fire({
            type: 'error',
            title: '¡Debe ingresar una contraseña!',
          })
        
      }

    });

