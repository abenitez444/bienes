$(document).ready(function() {
//TABLA PROVEEDORES

    $("#formValidaT1").validate({
        rules: {
           
           codProvee: { required:true},
           descProvee: { required:true},
           tipProvee: { required:true},
           grupo: { required:true},
           rifProvee: { required:true},
            
        },

        messages: {
           
            codProvee : "* ¡ Introduzca el código del proveedor!",
            descProvee : "* ¡ Introduzca la descripción del proveedor!",
            tipProvee : "* ¡ Seleccione el tipo de proveedor !",
            grupo : "* ¡ Seleccione el grupo de contribuyente !",
            rifProvee : "*  &nbsp;¡ Introduzca el número de rif !",

        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });

  // INPUT DISABLED EN TABLA DE PROVEEDORES AL SELECCIONAR LA SEGUNDA OPCION SE DESABILITA EL INPUT OTRA DESCRIPCIÓN
 


  //TABLA SEGUROS
    $("#formValidaT3").validate({
        rules: {
           codRegT3: { required:true},
           compAse: { required:true},
           tipPoli: { required:true},
           moneda: { required:true},
           poseRes: { required:true},
           tipoCobe: { required:true},
           
        },
        messages: {
            codRegT3 : "* ¡Introduzca el código de registro!",
            compAse : "* ¡Seleccione la compañia aseguradora!",
            tipPoli : "* ¡Seleccione el tipo de poliza!",
            moneda: "* ¡Seleccione la moneda!",
            poseRes : "* ¡Seleccione si posee responsabilidad civil!",
            tipoCobe : "* ¡Seleccione el tipo de cobertura!",
            
        

        },

        highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
                
    });
 
      //TABLA RESPONSABLE DE LOS BIENES
        $("#formValidaT4").validate({
        rules: {
           codResp: { required:true},
           tipoResp: { required:true},
           correRes:{required:true, email:true},
           cedula: { required:true},
           nomRes: { required:true},
           apeRes: { required:true},
           telfRes: { required:true},
           cargoRes: { required:true},
           codUnidad: { required:true},
            
        },
        messages: {
            codResp : "* Campo obligatorio, ¡ Introduzca el código del responsable!",
            tipoResp : "* Campo obligatorio, ¡ Seleccione el tipo de responsable!",
            correRes : {required: '<b class="jqueryMensaje">* Campo obligatorio, ¡ Ingrese el correo electrónico !</b>', email: '<b class="jqueryMensaje"> ¡ Inválido (Ej: ejemplo@gmail.com) !</b>'},
            cedula : "* Campo obligatorio, ¡ Introduzca la cédula de identidad!",
            nomRes: "* Campo obligatorio,¡ Introduzca nombre del responsable!",
            apeRes : "* Campo obligatorio, ¡ Introduzca el apellido del responsable!",
            telfRes : "* Campo obligatorio, ¡ Introduzca el teléfono del responsable!",
            cargoRes : "* Campo obligatorio, ¡ Introduzca el cargo del responsable",
            codUnidad : "* Campo obligatorio, ¡ Seleccione la dependencia administrativa del responsable!"
        

        },

        highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
                
            });
   
  //$("#formularioColor").hide();
    $("#formValidaAdd5").validate({
        rules: {
           codMarca: { required:true},
           denComar: { required:true},
           nomFabri: { required:true},
         
        },
        messages: {
           codMarca : "* Campo obligatorio, ¡ Introduzca el código de la marca!",
           denComar : "* Campo obligatorio, ¡ Introduzca la denominación de la marca!",
           nomFabri : "* Campo obligatorio, ¡ Introduzca el nombre del fabricante!"

        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });

     $("#formValidaT6").validate({
        rules: {
           codModel: { required:true},
           denModFab: { required:true},
           codMarca: { required:true},
           codCata: { required:true},
         
        },
        messages: {
           codModel : "* Campo obligatorio, ¡ Introduzca el código del modelo!",
           denModFab : "* Campo obligatorio, ¡ Introduzca la denominación del modelo!",
           codMarca : "* Campo obligatorio, ¡ Seleccione el código de la marca!",
           codCata : "* Campo obligatorio, ¡ Seleccione el código del bien mueble !"
         
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });


    $("#formValidaT7").validate({
        rules: {
           codigo: { required:true},
           denComponente: { required:true},
           codBienMueble: { required:true},
           
         
        },
        messages: {
           codigo : "* Campo obligatorio, ¡ Introduzca el código asignado por el ente !",
           denComponente : "* Campo obligatorio, ¡ Introduzca la denominación del componente !",
           codBienMueble : "* Campo obligatorio, ¡ Indique el código del bien mueble !"
           
         
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });

    // INPUT DISABLED EN TABLA DE BIENES AL SELECCIONAR LA SEGUNDA OPCION SE DESABILITA EL INPUT OTRA DESCRIPCIÓN
    //ANEXO T8 = BIENES

    $("#formValidaT8").validate({
        rules: {
           codOt2_1: { required:true},
           codCata: { required:true},
           codUnidad: { required:true},
           sedeOrgano: { required:true},
           codRespAdm: { required:true},
           codResBien: { required:true},
           estatuBien: { required:true},
           moneda: { required:true},
           edoBien: { required:true},
           codMarca: { required:true},
           codModel: { required:true},
           codColorBien: { required:true},
           unidadMedi: { required:true},
           poseeCompo: { required:true},
           seguroBien: { required:true},
           
        },
        
        messages: {
           codOt2_1 : "* ¡ Introduzca el código del origen  !",
           codCata : "* ¡ Seleccione el código del catalogo (Anexo A) !",
           codUnidad : "* ¡ Seleccione la dependencia administrativa !",
           sedeOrgano : "* ¡ Seleccione el código del organo o ente donde se encuentra el bien !",
           codRespAdm : "* ¡ Seleccione el código del responsable administrativo !",
           codResBien : "* ¡ Seleccione el código del responsable del uso directo del bien !",
           estatuBien : "* ¡ Seleccione el estatus del uso del bien !",
           moneda : "* ¡ Seleccione la moneda !",
           edoBien : "* ¡ Seleccione el estado del bien !",
           codMarca : "* ¡ Seleccione el código de la marca del bien !",
           codModel : "* ¡ Seleccione el código del modelo del bien !",
           codColorBien : "* ¡ Seleccione el color del bien !",
           unidadMedi : "* ¡ Seleccione la unidad de medida de la garantía !",
           poseeCompo : "* ¡ Seleccione si el bien posee componentes !",
           seguroBien : "* ¡ Seleccione si el bien se encuentra asegurado !",
           
         
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });


$("#formValidaT9").validate({
        rules: {
           codBien: { required:true},
           codCata: { required:true},
           depAdmRes: { required:true},
           estatuBien: { required:true},
           sedeOrgano: { required:true},
           codRespAdm: { required:true},
           codResBien: { required:true},
           moneda: { required:true},
           edoBien: { required:true},
           claseBien: { required:true},
           codMarca: { required:true},
           codModel: { required:true},
           codColorBien: { required:true},
           unidadMedi: { required:true},
           tieneSistema: { required:true},
           poseeCompo: { required:true},
           seguroBien: { required:true},
           
        },
        
        messages: {
           codBien : "* ¡ Introduzca código del origen !",
           codCata : "* ¡ Seleccione el código del catalogo al cual esta adscrito!",
           depAdmRes : "* ¡ Seleccione la dependencia administrativa !",
           estatuBien : "* ¡ Seleccione el estatus del uso del bien !",
           sedeOrgano : "* ¡ Seleccione el código del organo o ente donde se encuentra el bien !",
           codRespAdm : "* ¡ Seleccione el código según la dependencia la cual ésta adscrita !",
           codResBien : "* ¡ Seleccione el código del responsable del uso directo del bien !",
           moneda : "* ¡ Seleccione la moneda !",
           edoBien : "* ¡ Seleccione el estado del bien !",
           claseBien : "* ¡ Seleccione la clase del bien !",
           codMarca : "* ¡ Seleccione el código de la marca del bien !",
           codModel : "* ¡ Seleccione el código del modelo del bien !",
           codColorBien : "* ¡ Seleccione el color del bien !",
           unidadMedi : "* ¡ Seleccione la unidad de medida de la garantía !",
           tieneSistema : "* ¡ Seleccione si posee, sistema de rastreo y localización en uso !",
           poseeCompo : "* ¡ Seleccione si el bien posee componentes !",
           seguroBien : "* ¡ Seleccione si el bien se encuentra asegurado !"
           
         
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });

$("#formValidaT10").validate({
        rules: {
           codBien: { required:true},
           codCata: { required:true},
           codUnidad: { required:true},
           codRespAdm: { required:true},
           codResBien: { required:true},
           sedeOrgano: { required:true},
           estatuBien: { required:true},
           moneda: { required:true},
           edoBien: { required:true},
           genero: { required:true},
           tipoAnimal: { required:true},
           proposito: { required:true},
           codColorBien: { required:true},
           unidadPeso: { required:true},
           seguroBien: { required:true},
           
        },
        
        messages: {
           codBien : "* Campo obligatorio, ¡ Introduzca el código del origen del bien !",
           codCata : "* Seleccione el código del catalogo al cual esta adscrito!",
           codUnidad : "* Campo obligatorio, ¡ Seleccione la dependencia administrativa !",
           codRespAdm : "* Seleccione el código según la dependencia la cual ésta adscrita !",
           codResBien : "* Seleccione el código del responsable del uso directo del bien !",
           sedeOrgano : "* Seleccione el código del organo o ente donde se encuentra el bien !",
           estatuBien : "* Campo obligatorio, ¡ Seleccione el estatus del uso del bien !",
           moneda : "* Campo obligatorio, ¡ Seleccione la moneda !",
           edoBien : "* Campo obligatorio, ¡ Seleccione el estado del bien !",
           genero : "* Campo obligatorio, ¡ Seleccione el genero !",
           tipoAnimal : "* Campo obligatorio, ¡ Seleccione el tipo de animal !",
           proposito : "* Campo obligatorio, ¡ Seleccione el propósito !",
           codColorBien : "* Campo obligatorio, ¡ Seleccione el color del bien !",
           unidadPeso : "* Campo obligatorio, ¡ Seleccione la unidad de medida del peso !",
           seguroBien : "* Campo obligatorio, ¡ Seleccione si el bien se encuentra asegurado !"
           
         
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });

$("#formValidaT11").validate({
        rules: {
           codBien: { required:true},
           codMarca: { required:true},
           codModel: { required:true},
        },
        
        messages: {
           codBien : "* Campo obligatorio, ¡ Por favor, introduzca el código del origen del bien !",
           codMarca : "* Campo obligatorio, ¡ Por favor, seleccione el código de la marca !",
           codModel : "* Campo obligatorio, ¡ Por favor, seleccione el código del modelo !",
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });

//ANEXO INMUEBLES

$("#formValidaT12").validate({
        rules: {
           codBien: { required:true},
           codUnidad: { required:true},
           codCata: { required:true},
           codRespAdm: { required:true},
           corresBien: { required:true},
           sedeOrgano: { required:true},
           localizacion: { required:true},
           codPais: { required:true},
           codParroquia: { required:true},
           codCiudad: { required:true},
           estatuBien: { required:true},
           moneda: { required:true},
           edoBien: { required:true},
           usoBienInmu: { required:true},
           unidadConstru: { required:true},
           unidadTerreno: { required:true},
           seguroBien: { required:true},
          
        },
        
        messages: {
           codBien : "* Campo obligatorio, ¡ Introduzca el código del origen del bien !",
           codUnidad : "* Campo obligatorio, ¡ Seleccione la dependencia administrativa !",
           codCata : "* Seleccione el código del catalogo al cual esta adscrito!",
           codRespAdm : "*  Seleccione el código según la dependencia la cual ésta adscrita !",
           corresBien : "* Campo obligatorio, ¡ Seleccione si el inmueble corresponde a alguna sede del ente !",
           localizacion : "* Campo obligatorio, ¡ Seleccione la localización !",
           sedeOrgano : "*  Seleccione el código del ente donde corresponde el bien !",
           codPais : "* Campo obligatorio, ¡ Seleccione el país !",
           codParroquia : "* Campo obligatorio, ¡ Seleccione la parroquia !",
           codCiudad : "* Campo obligatorio, ¡ Seleccione la ciudad !",
           estatuBien : "* Campo obligatorio, ¡ Seleccione el estatus del bien !",
           moneda : "* Campo obligatorio, ¡ Seleccione la moneda !",
           edoBien : "* Campo obligatorio, ¡ Seleccione el estado del bien !",
           usoBienInmu : "* Campo obligatorio, ¡ Seleccione el uso del bien inmueble !",
           unidadConstru : "* Campo obligatorio, ¡ Seleccione la unidad de medida del área de construcción !",
           unidadTerreno : "* Campo obligatorio, ¡ Seleccione la unidad de medida del área del terreno !",
           seguroBien : "* Campo obligatorio, ¡ Seleccione si se encuentra asegurado el bien !",
         
        },
        
      highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li", 
   
    });

$("#formValidaS1").validate({
        rules: {
           
           codSigecof: { required:true},
           rifProvee: { required:true},
           telfEnte: { required:true},
           direcWeb: { required:true},
           correEnte: { required:true, email:true},
            
        },

        messages: {
           
            codSigecof : "* ¡ Introduzca el código SIGECOF !",
            rifProvee : "* ¡ Número de rif !",
            telfEnte : "* ¡ Introduzca el teléfono !",
            direcWeb : "* ¡ Indique la dirección web !",
            correEnte : {required: '<b class="jqueryMensaje">* ¡ Ingrese el correo electrónico !</b>', email: '<b class="jqueryMensaje"> ¡ Inválido (Ej: ejemplo@gmail.com) !</b>'},

        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });

$("#formValidaS2").validate({
        rules: {
           
           cedula: { required:true},
           nombre: { required:true},
           apellido: { required:true},
           telefono: { required:true},
           cargo: { required:true},
           correo: { required:true, email:true},
            
        },

        messages: {
           
            cedula : "* ¡ Introduzca la cédula de identidad !",
            nombre : "* ¡ Introduzca el nombre del responsable !",
            apellido : "* ¡ Introduzca el apellido del responsable !",
            telefono : "* ¡ Introduzca el teléfono del responsable !",
            cargo : "* ¡ Introduzca el cargo del responsable !",
            correo : {required: '<b class="jqueryMensaje">* ¡ Ingrese el correo electrónico !</b>', email: '<b class="jqueryMensaje"> ¡ Inválido (Ej: ejemplo@gmail.com) !</b>'},

        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });

$("#formValidaS3").validate({
        rules: {
           
           cedula: { required:true},
           nombre: { required:true},
           apellido: { required:true},
           telefono: { required:true},
           cargo: { required:true},
           correo: { required:true, email:true},
            
        },

        messages: {
           
            cedula : "* ¡ Introduzca la cédula de identidad !",
            nombre : "* ¡ Introduzca el nombre del responsable !",
            apellido : "* ¡ Introduzca el apellido del responsable !",
            telefono : "* ¡ Introduzca el teléfono del responsable !",
            cargo : "* ¡ Introduzca el cargo del responsable !",
            correo : {required: '<b class="jqueryMensaje">* ¡ Ingrese el correo electrónico !</b>', email: '<b class="jqueryMensaje"> ¡ Inválido (Ej: ejemplo@gmail.com) !</b>'},


        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });


$("#formValidaS4").validate({
        rules: {
           
           tipoSede: { required:true},
           descSede: { required:true},
           localizacion: { required:true},
           codPais: { required:true},
           codParroquia: { required:true},
           codCiudad: { required:true},
           
            
        },

        messages: {
           
            tipoSede : "* ¡ Seleccione el tipo de sede !",
            descSede : "* ¡ Describa la sede !",
            localizacion : "* ¡ Seleccione la localización !",
            codPais : "* ¡ Seleccione el código del país !",
            codParroquia : "* ¡ Seleccione el código de la parroquia !",
            codCiudad : "* ¡ Seleccione el código de la ciudad !",

        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });

$("#formValidaS5").validate({
        rules: {
           
           codUnidad: { required:true},
           descUnidad: { required:true},
           categoria: { required:true},
         
          
            
        },

        messages: {
           
            codUnidad : "* ¡ Introduzca el código de la unidad administrativa !",
            descUnidad : "* ¡ Describa la denominación de la unidad !",
            categoria : "* ¡ Seleccione la categoría de la unidad !",
           
            

        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });



$("#formValidaS6").validate({
        rules: {
           
           codSede: { required:true},
           codUnidad: { required:true},
           
         
          
            
        },

        messages: {
           
            codSede : "* ¡ Seleccione el código de la sede !",
            codUnidad : "* ¡ Seleccione el código de la unidad administrativa !",
            
           
            

        },

         highlight: function(element, errorClass, validClass) {
                  $(element).addClass(errorClass).removeClass(validClass);
          // Keeps the default behaviour, adding error class to element but seems to break the default radio group behaviour but the below fixes that
                  $(element).closest('ul').addClass(errorClass);
          // add error class to ul element for checkbox groups and radio inputs
              },
              unhighlight: function(element, errorClass, validClass) {
                  $(element).removeClass(errorClass).addClass(validClass);
          // keeps the default behaviour removing error class from elements
                  $(element).closest('ul').removeClass(errorClass);
          // remove error class from ul element for checkbox groups and radio inputs
              },
        // FIX END
 
        errorLabelContainer: ".js-errors",
        errorElement: "li",
                
    });

}); 




