//MARCA Y MODELO EN RELACIÓN

   $("#codMarca").change(function(event){

	 $.get("dropdown/"+event.target.value+"",function(response,codMarca){
  //recorrer respuentas e incluir en el select las areas
   if(response.length > 0){

   $('#codModel').removeAttr('disabled');
   $("#codModel").empty();
   $("#codModel").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
     	$("#codModel").append("<option value='"+response[i].id+ "'> "+response[i].codModel+"</option>");
        }
    
    }else {

      $("#codModel").empty().append("<option>Por favor, agregue un código de módelo del bien. </option>");
     
      }
	 });
 });

//MODIFICAR BIENES

   $("#codMarca2").change(function(event){
    var rut = $('#rut').val();
    $.get(rut+'/'+event.target.value+"",function(response,codMarca){
  //recorrer respuentas e incluir en el select las areas
   $('#codModel2').removeAttr('disabled');
   $("#codModel2").empty();
   $("#codModel2").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
        $("#codModel2").append("<option value='"+response[i].id+ "'> "+response[i].codModel+"</option>");

      }
     });
  });

//MODIFICAR DATOS BIENES

   $("#codMarca3").change(function(event){
      var rut = $('#rut').val();
    $.get(rut+'/'+event.target.value+"",function(response,codMarca){
  //recorrer respuentas e incluir en el select las areas
   $('#codModel3').removeAttr('disabled');
  
   $("#codModel3").empty();
   $("#codModel3").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
        $("#codModel3").append("<option value='"+response[i].id+ "'> "+response[i].codModel+"</option>");

      }
     });
  });

//RESPONSABLE Y DEPENDENCIA EN RELACIÓN BIENES

$("#codUnidad").change(function(event){
 
   $.get("downsede/"+event.target.value+"",function(response,codUnidad){
  //recorrer respuentas e incluir en el select las areas
   if (response.length > 0){

   $('#codRespAdm').removeAttr('disabled');
   $("#codRespAdm").empty();
   $("#codRespAdm").append("<option value='0' disabled selected>Seleccione... </option>");
   $('#codResBien').removeAttr('disabled');
   $("#codResBien").empty();
   $("#codResBien").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
      if (response[i].codUnidad==event.target.value && response[i].tipoResp == 1 ) {
        $("#codRespAdm").append("<option value='"+response[i].id+ "'> "+response[i].codResp+" - "+response[i].nomRes+"</option>");
      }
      if (response[i].codUnidad==event.target.value && response[i].tipoResp == 2 ) {
        $("#codResBien").append("<option value='"+response[i].id+ "'> "+response[i].codResp+" - "+response[i].nomRes+"</option>");
      }
      }

      }else {

        $("#codRespAdm").empty().append("<option selected>Por favor, agregue un código de responsable administrativo </option>");
        $("#codResBien").empty().append("<option selected>Por favor, agregue un código de responsable de uso directo </option>");
        $("#codRespAdm").attr('disabled', 'true');
        $("#codResBien").attr('disabled', 'true');
     
      }
   });
 });

//MODIFICAR RESPONSABLE Y DEPENDENCIA EN RELACION BIENES

   $("#codUnidadM").change(function(event){
    $.get("../../downsede/"+event.target.value+"",function(response,codUnidad){
   //recorrer respuentas e incluir en el select las areas
   if (response.length > 0){

   $('#codRespAdm').removeAttr('disabled');
   $("#codRespAdm").empty();
   $("#codRespAdm").append("<option value='0' disabled selected>Seleccione... </option>");
   $('#codResBien').removeAttr('disabled');
   $("#codResBien").empty();
   $("#codResBien").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){

      if (response[i].codUnidad==event.target.value && response[i].tipoResp == 1 ) {
        $("#codRespAdm").append("<option value='"+response[i].id+ "'> "+response[i].codResp+" - "+response[i].nomRes+"</option>");
        }
      if (response[i].codUnidad==event.target.value && response[i].tipoResp == 2 ) {
        $("#codResBien").append("<option value='"+response[i].id+ "'> "+response[i].codResp+" - "+response[i].nomRes+"</option>");
        }
      }

      }else {

        $("#codRespAdm").empty().append("<option selected>Por favor, agregue un código de responsable administrativo </option>");
        $("#codResBien").empty().append("<option selected>Por favor, agregue un código de responsable de uso directo </option>");
        $("#codRespAdm").attr('disabled', 'true');
        $("#codResBien").attr('disabled', 'true');
      }
   });
 });

//MODIFICAR RESPONSABLE Y DEPENDENCIA EN RELACIÓN TRANSPORTE

   $("#codUnidad2").change(function(event){
    $.get("../../downtran/"+event.target.value+"",function(response,codUnidad){
  //recorrer respuentas e incluir en el select las areas
   $('#codRespAdm2').removeAttr('disabled');
   $("#codRespAdm2").empty();
   $("#codRespAdm2").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
      $("#codRespAdm2").append("<option value='"+response[i].id+ "'> "+response[i].codResp+"</option>");

      }
   });
 });


//MODIFICAR RESPONSABLE Y DEPENDENCIA EN RELACIÓN SEMOVIENTES

   $("#codUnidad3").change(function(event){
   $.get("../../downsemo/"+event.target.value+"",function(response,codUnidad){
  //recorrer respuentas e incluir en el select las areas
   $('#codRespAdm3').removeAttr('disabled');
  
   $("#codRespAdm3").empty();
   $("#codRespAdm3").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
      $("#codRespAdm3").append("<option value='"+response[i].id+ "'> "+response[i].codResp+"</option>");

      }
   });
 });

//MODIFICAR RESPONSABLE Y DEPENDENCIA EN RELACIÓN INMUEBLE

   $("#codUnidad4").change(function(event){
   $.get("../../downinmu/"+event.target.value+"",function(response,codUnidad){
  //recorrer respuentas e incluir en el select las areas
   $('#codRespAdm4').removeAttr('disabled');
   $("#codRespAdm4").empty();
   $("#codRespAdm4").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
      $("#codRespAdm4").append("<option value='"+response[i].id+ "'> "+response[i].codResp+"</option>");

      }
   });
 });


  //===========================================//
/*=======REGISTRAR DATOS DE LA SEDE ANEXO S6====*/

//REGISTRAR DATOS DE LA SEDE

/*$("#codSede").change(function(event){

  $.get("dropdownS6/"+event.target.value+"",function(response,codSede){
  //recorrer respuentas e incluir en el select las areas
  $('#codUnidad').removeAttr('disabled');
  
   $("#codUnidad").empty();
    $("#codUnidad").append("<option value='0' disabled selected>Seleccione... </option>");
     for(i=0; i<response.length; i++){
      $("#codUnidad").append("<option value='"+response[i].id+ "'> "+response[i].codUnidad+"</option>");

      }
   });
 });*/

