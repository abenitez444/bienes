<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('direcciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('conversion');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('users');
        Schema::dropIfExists('acciones');
        Schema::dropIfExists('sel_proveedores');
        Schema::dropIfExists('sel_concurso');
        Schema::dropIfExists('sel_directa');
        Schema::dropIfExists('sel_confiscacion');
        Schema::dropIfExists('sel_dacion');
        Schema::dropIfExists('sel_donacion');
        Schema::dropIfExists('sel_expropiacion');
        Schema::dropIfExists('sel_permuta');
        Schema::dropIfExists('sel_transferencia');
        Schema::dropIfExists('sel_adjudicacion');
        Schema::dropIfExists('sel_seguros');
        Schema::dropIfExists('sel_seguros1');
        Schema::dropIfExists('sel_seguros2');
        Schema::dropIfExists('sel_seguros3');
        Schema::dropIfExists('sel_seguros4');
        Schema::dropIfExists('sel_responsables');
        Schema::dropIfExists('sel_estatusbien');
        Schema::dropIfExists('sel_condicionbien');
        Schema::dropIfExists('sel_garantiabien');
        Schema::dropIfExists('sel_colorbien');
        Schema::dropIfExists('sel_clasebien');
        Schema::dropIfExists('sel_tipoanimal');
        Schema::dropIfExists('sel_proposito');
        Schema::dropIfExists('sel_medidapeso');
        Schema::dropIfExists('sel_genero');
        Schema::dropIfExists('sel_paises');
        Schema::dropIfExists('sel_parroquias');
        Schema::dropIfExists('sel_ciudad');
        Schema::dropIfExists('sel_usos');
        Schema::dropIfExists('sel_sedes');
        Schema::dropIfExists('sel_piso');
        Schema::dropIfExists('proveedores');
        Schema::dropIfExists('confiscacion');
        Schema::dropIfExists('concurso');
        Schema::dropIfExists('directa');
        Schema::dropIfExists('dacionPago');
        Schema::dropIfExists('donacion');
        Schema::dropIfExists('expropiacion');
        Schema::dropIfExists('permuta');
        Schema::dropIfExists('transferencia');
        Schema::dropIfExists('adjudicacion');
        Schema::dropIfExists('seguros');
        Schema::dropIfExists('responsables');
        Schema::dropIfExists('marcas');
        Schema::dropIfExists('modelos');
        Schema::dropIfExists('componentes');
        Schema::dropIfExists('bienes');
        Schema::dropIfExists('transporte');
        Schema::dropIfExists('semovientes');
        Schema::dropIfExists('datosbienes');
        Schema::dropIfExists('datosinmuebles');
        Schema::dropIfExists('basicos');
        Schema::dropIfExists('maxima');
        Schema::dropIfExists('patrimonial');
        Schema::dropIfExists('sedes');
        Schema::dropIfExists('unidades');
        Schema::dropIfExists('ubicacion');
        Schema::dropIfExists('bitacora');
    }
}
