<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
}); 
Route::get('/login', function () {
    return view('auth.login');
});


Route::post('/sesion', 'controladorLogin@login');
Route::get('/prueba', 'controladorLogin@prueba');
Route::get('/logout', 'controladorLogin@logout');
/*==============================================*/
/*==============================================*/

#SEGURIDAD DEL MIDDLEWARE PARA LA URL ARCHIVOS QUE LO DOMINAN: /En: MIDDLEWARE SECURITY FOR THE URL DOMAIN FILE: 
#middlewareSesion.php/kernel.php/controladorLogin /En: middlewareSesion.php / kernel.php / controllerLogin

/*=================================================*/
             #RUTAS PARA LOS USUARIOS LOGUEADOS
/*=================================================*/

if(!isset($_SESSION)) 
{ 
    session_start(); 
} 

Route::group(['middleware' => 'sesion'], function () {
	Route::get('/home', 'homeController@index');
});

Route::group(['middleware' => 'analista'], function () {
	Route::get('/proveedores', 'controladorProveedores@index');
	Route::resource('proveedores', 'controladorProveedores');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-provee', 'controladorProveedores@exportProvee');

	# ANEXO T2 = concurso
	Route::get('/concurso', 'controladorConcurso@index');
	Route::resource('concurso', 'controladorConcurso');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-concu', 'controladorConcurso@exportConcurso');

	# ANEXO T2-1 = directa
	Route::get('/directa', 'controladorDirecta@index');
	Route::resource('directa', 'controladorDirecta');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-directa', 'controladorDirecta@exportDirecta');

	# ANEXO T2-2 = confiscacion
	Route::get('/confiscacion', 'controladorConfiscacion@index');
	Route::resource('confiscacion', 'controladorConfiscacion');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-confiscacion', 'controladorConfiscacion@exportConfis');

	# ANEXO T2-3 = dacion
	Route::get('/dacion', 'controladorDacion@index');
	Route::resource('dacion', 'controladorDacion');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-dacion', 'controladorDacion@exportDacion');

	# ANEXO T2-4 = donacion
	Route::get('/donacion', 'controladorDonacion@index');
	Route::resource('donacion', 'controladorDonacion');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-donacion', 'controladorDonacion@exportDonacion');

	# ANEXO T2-5 = expropiacion
	Route::get('/expropiacion', 'controladorExpropiacion@index');
	Route::resource('expropiacion', 'controladorExpropiacion');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-expro', 'controladorExpropiacion@exportExpro');

	# ANEXO T2-6 = permuta
	Route::get('/permuta', 'controladorPermuta@index');
	Route::resource('permuta', 'controladorPermuta');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-permuta', 'controladorPermuta@exportPermuta');

	# ANEXO T2-7 = transferencia
	Route::get('/transferencia', 'controladorTransferencia@index');
	Route::resource('transferencia', 'controladorTransferencia');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-transfe', 'controladorTransferencia@exportTransfe');

	# ANEXO T2-8 = adjudicacion
	Route::get('/adjudicacion', 'controladorAdjudicacion@index');
	Route::resource('adjudicacion', 'controladorAdjudicacion');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-adjud', 'controladorAdjudicacion@exportAdjud');

	# ANEXO T3 = seguros
	Route::get('/seguros', 'controladorSeguros@index');
	Route::resource('seguros', 'controladorSeguros');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-seguro', 'controladorSeguros@exportSeguro');

	# ANEXO T4 = responsables
	Route::get('/responsables', 'controladorResponsables@index');
	Route::resource('responsables', 'controladorResponsables');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-respon', 'controladorResponsables@exportRespon');


	# ANEXO T5 = Marcas
	Route::resource('marcas', 'controladorAddMarca');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-marcas', 'controladorAddMarca@exportMarcas');

	# ANEXO T6 = Modelos
	Route::get('/modelos', 'controladorAddModelos@index');
	Route::resource('modelos', 'controladorAddModelos');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-modelos', 'controladorAddModelos@exportModelos');

	# ANEXO T7 = Componentes
	Route::get('/componentes', 'controladorComponentes@index');
	Route::resource('componentes', 'controladorComponentes');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-compo', 'controladorComponentes@exportCompo');

	# ANEXO T8 = Bienes
	Route::get('/bienes', 'controladorBienes@index');
	Route::resource('bienes', 'controladorBienes');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-bienes', 'controladorBienes@exportBienes');

	# ANEXO T9 = Equipo de Transporte
	Route::get('/transporte', 'controladorEqtransporte@index');
	Route::resource('transporte', 'controladorEqtransporte');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-trans', 'controladorEqtransporte@exportTranspo');

	# ANEXO T10 = Semovientes
	Route::get('/semovientes', 'controladorSemovientes@index');
	Route::resource('semovientes', 'controladorSemovientes');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-semo', 'controladorSemovientes@exportSemo');

	# ANEXO T11 = Datos de los Bienes
	Route::get('/datosbienes', 'controladorDatosbienes@index');
	Route::resource('datosbienes', 'controladorDatosbienes');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-muebles', 'controladorDatosbienes@exportMuebles');

	# ANEXO T12 = Datos de los Inmuebles
	Route::get('/datosinmuebles', 'controladorInmuebles@index');
	Route::resource('datosinmuebles', 'controladorInmuebles');
			/*=========Imprimir Reporte==========*/
	Route::get('/export-inmuebles', 'controladorInmuebles@exportInmuebles');

	# SELECT DINAMICOS
			/*===Registrar Marcas y Modelos Anexos T===*/
	Route::get('dropdown/{id}','controladorBienes@getBienes');

	/*============Modificar Marcas y Modelos Anexos T==============*/
	Route::get('dropdown2/{id}','controladorBienes@getBienesmodif');

			/*===Registrar dependencia y responsable Anexos  T -Bienes===*/
	Route::get('downsede/{id}','controladorBienes@depenResponsable');

	/*============Registrar dependencia y responsable Anexos T -Transporte==============*/
	Route::get('downtran/{id}','controladorEqtransporte@depenResponsable');

	/*============Registrar dependencia y responsable Anexos T -Semovientes==============*/
	Route::get('downsemo/{id}','controladorSemovientes@depenResponsemo');

	/*============Registrar dependencia y responsable Anexos T -Inmuebles==============*/
	Route::get('downinmu/{id}','controladorInmuebles@depenResponsableIn');

	/*=================================================*/
				  # ANEXOS S
				  # ANEXO S1 = Datos Básicos
	/*=================================================*/

	Route::get('/basicos', 'controladorS1@index');
	Route::resource('basicos', 'controladorS1');
	/*=========Imprimir Reporte==========*/
	Route::get('/export-basicos', 'controladorS1@exporBasicos');

	# ANEXO S2 = Datos Máxima Autoridad
	Route::get('/maxima', 'controladorS2@index');
	Route::resource('maxima', 'controladorS2');
	/*=========Imprimir Reporte==========*/
	Route::get('/export-maxima', 'controladorS2@exportMaxima');

	# ANEXO S3 = Datos del Responsable Patrimonial
	Route::get('/patrimonial', 'controladorS3@index');
	Route::resource('patrimonial', 'controladorS3');
	/*=========Imprimir Reporte==========*/
	Route::get('/export-patrimonial', 'controladorS3@exportPatrimonial');

	# ANEXO S4 = Datos de la sede
	Route::get('/sedes', 'controladorS4@index');
	Route::resource('sedes', 'controladorS4');
	/*=========Imprimir Reporte==========*/
	Route::get('/export-sedes', 'controladorS4@exportSedes');

	# ANEXO S5 = Datos de las Unidades Administrativas
	Route::get('/unidades', 'controladorS5@index');
	Route::resource('unidades', 'controladorS5');
	/*=========Imprimir Reporte==========*/
	Route::get('/export-unidades', 'controladorS5@exportUnidades');

	# ANEXO S6 = Datos de las Unidades Administrativas
	Route::get('/ubicacion', 'controladorS6@index');
	Route::resource('ubicacion', 'controladorS6');
	/*=========Imprimir Reporte==========*/
	Route::get('/export-ubicacion', 'controladorS6@exportUbicacion');


	/*------------------------------  // -------------------------*/

	# SELECT LOCALIZACIÓN Y SELECT CODIGO DE PARROQUIA PARA CUANDO SELECCIONE INTERNACIONAL SE COLOQUE 99 EN CASO CONTRARIO TRAIGA LAS OPCIONES DEL OPTION(SELECT)

	/*------------- Vista Inmuebles pertenece al controlador inmuebles-------------*/

	/*== Ruta Paises ==*/
	Route::get('paisIn/{id}', 'controladorInmuebles@paisInmu');
	/*== Ruta Parroquias ==*/
	Route::get('parroIn/{id}', 'controladorInmuebles@parroInmu');
	/*== Ruta Ciudad ==*/
	Route::get('ciudadIn/{id}', 'controladorInmuebles@ciudadInmu');

	/* Modificar Inmuebles pertenece al controlador inmuebles*/
	/*== Ruta Paises ==*/
	Route::get('paisModi/{id}', 'controladorInmuebles@modifiPais');
	/*== Ruta Parroquias ==*/
	Route::get('parroModi/{id}', 'controladorInmuebles@modifiParro');
	/*== Ruta Ciudad ==*/
	Route::get('ciudadModi/{id}', 'controladorInmuebles@modifiCiudad');

	/*------------------------------  // -------------------------*/

	/*------------- Vista Datos de las Sedes pertenece al controladorS4 -------------*/

	/*== Ruta Paises S4 ==*/
	Route::get('paisesS4/{id}', 'controladorS4@paises');
	/*== Ruta Parroquias S4 ==*/
	Route::get('parroS4/{id}', 'controladorS4@parroquias');
	/*== Ruta Ciudad S4 ==*/
	Route::get('ciudadS4/{id}', 'controladorS4@ciudades');

	/*------------- Modificar Datos de la sede al controladorS4 -------------*/

	/*== Ruta Paises ==*/
	Route::get('s4Pais2/{id}', 'controladorS4@modiPaisesS4');
	/*== Ruta Parroquias ==*/
	Route::get('s4Parro2/{id}', 'controladorS4@modiParroS4');
	/*== Ruta Ciudad ==*/
	Route::get('s4Ciudad2/{id}', 'controladorS4@modiCiudadS4');

	/*------------------------------  // -------------------------*/

	# VISTAS DE MUESTRA REGISTROS DATATABLE registroT./En: VIEWS OF SAMPLE REGISTERS DATATABLE REGISTRO
	Route::get('/regProveedores', 'con_proveedoresVer@index');
	Route::get('/regConcurso', 'con_concursoVer@index');
	Route::get('/regDirecta', 'con_directaVer@index');
	Route::get('/regConfiscacion', 'con_confiscacionVer@index');
	Route::get('/regDacion', 'con_dacionVer@index');
	Route::get('/regDonacion', 'con_donacionVer@index');
	Route::get('/regExpropiacion', 'con_expropiacionVer@index');
	Route::get('/regPermuta', 'con_permutaVer@index');
	Route::get('/regTransferencia', 'con_transferenciaVer@index');
	Route::get('/regAdjudicacion', 'con_adjudicacionVer@index');
	Route::get('/regSeguros', 'con_segurosVer@index');
	Route::get('/regResponsables', 'con_responsablesVer@index');

	# MUESTRA REGISTRO DATATABLE DE LAS NUEVAS MARCAS AGREGADAS AL SELECT
	Route::get('/histoMarcas', 'con_histoMarcas@index');
	Route::get('/histoModelos', 'con_histoModelos@index');
	Route::get('/regComponentes', 'con_componentesVer@index');
	Route::get('/regBienes', 'con_bienesVer@index');
	Route::get('/regTransporte', 'con_EqtransporteVer@index');
	Route::get('/regSemovientes', 'con_semovientesVer@index');
	Route::get('/regDatosbienes', 'con_datosbienesVer@index');
	Route::get('/regInmuebles', 'con_inmueblesVer@index');

	# MUESTRA REGISTRO DE LOS ANEXOS S
	Route::get('/regBasicos', 'con_s1Ver@index');
	Route::get('/regMaxima', 'con_s2Ver@index');
	Route::get('/regPatrimonial', 'con_s3Ver@index');
	Route::get('/regSedes', 'con_s4Ver@index');
	Route::get('/regUnidades', 'con_s5Ver@index');
	Route::get('/regUbicacion', 'con_s6Ver@index');

	# AÑADIR 
	Route::get('/histoPaises', 'con_histoPaises@index');
	Route::get('/histoParroquia', 'con_histoParro@index');
	Route::get('/histoCiudad', 'con_histoCiudad@index');

	/*------------------------------  // -------------------------*/

	# FUNCION DE REVISADO FUNCIÓN selectId => con_VerT./En: REVISED FUNCTION FUNCTION selectId => VT controller

	Route::get('seleccionProveedores/{id}', 'con_proveedoresVer@selectId');
	Route::get('seleccionConcurso/{id}', 'con_concursoVer@selectId');
	Route::get('seleccionDirecta/{id}', 'con_directaVer@idDirecta');
	Route::get('seleccionConfiscacion/{id}', 'con_confiscacionVer@selectId');
	Route::get('seleccionDacion/{id}', 'con_dacionVer@selectId');
	Route::get('seleccionDonacion/{id}', 'con_donacionVer@selectId');
	Route::get('seleccionExpropiacion/{id}', 'con_expropiacionVer@selectId');
	Route::get('seleccionPermuta/{id}', 'con_permutaVer@selectId');
	Route::get('seleccionTransferencia/{id}', 'con_transferenciaVer@selectId');
	Route::get('seleccionAdjudicacion/{id}', 'con_adjudicacionVer@selectId');
	Route::get('seleccionSeguros/{id}', 'con_segurosVer@selectId');
	Route::get('seleccionResponsables/{id}', 'con_responsablesVer@selectId');
	Route::get('seleccionMarcas/{id}', 'con_histoMarcas@selectId');
	Route::get('seleccionModelos/{id}', 'con_histoModelos@selectId');
	Route::get('seleccionComponentes/{id}', 'con_componentesVer@selectId');
	Route::get('seleccionBienes/{id}', 'con_bienesVer@idBienes');
	Route::get('seleccionEqtransporte/{id}', 'con_EqtransporteVer@selectId');
	Route::get('seleccionSemovientes/{id}', 'con_semovientesVer@selectId');
	Route::get('seleccionDatosbien/{id}', 'con_datosbienesVer@selectId');
	Route::get('seleccionInmuebles/{id}', 'con_inmueblesVer@selectId');

	# FUNCIÓN DE REVISADO DE ANEXOS S selectId con_sN°Ver 
	Route::get('seleccionBasicos/{id}', 'con_s1Ver@selectId');
	Route::get('seleccionMaxima/{id}', 'con_s2Ver@selectId');
	Route::get('seleccionPatrimonial/{id}', 'con_s3Ver@selectId');
	Route::get('seleccionSedes/{id}', 'con_s4Ver@selectId');
	Route::get('seleccionUnidades/{id}', 'con_s5Ver@selectId');
	Route::get('seleccionUbicacion/{id}', 'con_s6Ver@selectId');

	# AÑADIR PAISES
	Route::get('seleccionPaises/{id}', 'con_histoPaises@selectId');
	Route::get('seleccionParroquia/{id}', 'con_histoParro@selectId');
	Route::get('seleccionCiudad/{id}', 'con_histoCiudad@selectId');

	/*------------------------------  // -------------------------*/ 

	});

/*=================================================*/
             #BITACORA DE ADMINISTRADOR
/*=================================================*/




Route::group(['middleware' => 'admin'], function () {
 # ELIMINAR REGISTRO DE TABLAS DE ANEXOS T =>con_Ver...
/*============AGREGAR USUARIO===============*/
Route::get('/addUser', 'controladorUser@index');
Route::post('/search-user', 'controladorUser@searchUser');
Route::post('/add-user', 'controladorUser@addUser');
Route::post('/validatePass', 'controladorUser@validatePass');

Route::get('/bitacora', 'controladorBitacora@enviarVariable');
Route::get('/anularProvee/{id}', 'con_proveedoresVer@anularProvee');
Route::get('/anularConcur/{id}', 'con_concursoVer@anularConcur');
Route::get('/anularDirec/{id}', 'con_directaVer@anularDirec');
Route::get('/anularConfis/{id}', 'con_confiscacionVer@anularConfis');
Route::get('/anularDacion/{id}', 'con_dacionVer@anularDacion');
Route::get('/anularDonac/{id}', 'con_donacionVer@anularDonac');
Route::get('/anularExpro/{id}', 'con_expropiacionVer@anularExpro');
Route::get('/anularPermu/{id}', 'con_permutaVer@anularPermu');
Route::get('/anularTrans/{id}', 'con_transferenciaVer@anularTrans');
Route::get('/anularSegu/{id}', 'con_segurosVer@anularSegu');
Route::get('/anularRespon/{id}', 'con_responsablesVer@anularRespon');
Route::get('/anularAdjud/{id}', 'con_adjudicacionVer@anularAdjud');
Route::get('/anularMarca/{id}', 'con_histoMarcas@anularMarca');
Route::get('/anularModelo/{id}', 'con_histoModelos@anularModelo');
Route::get('/anularComponentes/{id}', 'con_componentesVer@anularComponentes');
Route::get('/anularBienes/{id}', 'con_bienesVer@anularBienes');
Route::get('/anularEqtransporte/{id}', 'con_EqtransporteVer@anularTransporte');
Route::get('/anularSemo/{id}', 'con_semovientesVer@anularSemo');
Route::get('/anularDatos/{id}', 'con_datosbienesVer@anularDatos');
Route::get('/anularInmu/{id}', 'con_inmueblesVer@anularInmuebles');

/*------------------------------  // -------------------------*/

# ELIMINAR REGISTRO DE TABLAS DE ANEXOS S =>con_Ver...
Route::get('/anularBasicos/{id}', 'con_s1Ver@anularBasicos');
Route::get('/anularMaxima/{id}', 'con_s2Ver@anularMaxima');
Route::get('/anularPatrimonial/{id}', 'con_s3Ver@anularPatrimonial');
Route::get('/anularSedes/{id}', 'con_s4Ver@anularSedes');
Route::get('/anularUnidades/{id}', 'con_s5Ver@anularUnidades');
Route::get('/anularUbicacion/{id}', 'con_s6Ver@anularUbicacion');

# ELIMINAR PAISES
Route::get('/anularPaises/{id}', 'con_histoPaises@anularPaises');
Route::get('/anularParroquia/{id}', 'con_histoParro@anularParroquia');
Route::get('/anularCiudad/{id}', 'con_histoCiudad@anularCiudad');

});



	Route::get('/home', function () {
    return view('/home');
});


