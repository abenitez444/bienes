<?php

namespace App\Http\Middleware;

use Closure;

class middlewareAnalista
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($_SESSION['rol'])) {
            return redirect('/');
        }
         
         if($_SESSION['rol'] == '2' || $_SESSION['rol'] == '1')
        {
     
            return $next($request); 
        
        }

        else
        {
            return redirect('/');
        }
    }
}
