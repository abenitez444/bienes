<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloSemovientes;
use App\modeloBitacora;


class con_semovientesVer extends Controller
{
    public function index(){
        
    $verT10= modeloSemovientes::all();

    return view('RegistrosT.regSemovientes', compact('verT10'));

    }

    public function selectId($id){

        $seleccion = modeloSemovientes::find($id);

        $fechaAdqui = $seleccion->feAdqBien;
        $fechaAdqui = date("d/m/Y", strtotime($fechaAdqui));

        $fechaIng = $seleccion->feIngBien;
        $fechaIng = date("d/m/Y", strtotime($fechaIng));

        $fechaNa = $seleccion->feNacimiento;
        $fechaNa = date("d/m/Y", strtotime($fechaNa));

       return view('MuestraAnexosT.muestraSemovientes',compact('seleccion','fechaAdqui','fechaIng','fechaNa'));
    }

     /*FUNCIÓN anularT10 es para simular la eliminacion del registro en el datatable CUANDO ESTA EN 0 SE MUESTRA Y CUANDO CAMBIA A 1 EL REGISTRO NO SE MUESTRA*/

    public function anularSemo($id)
    {
    
    $seleccion= modeloSemovientes::find($id);
        
    if($seleccion->delete()){
       	$bit = new modeloBitacora();
        $bit->user = $_SESSION['id'];
        $bit->accion  = 3;
        $bit->referencia = 'Semovientes';
        $bit->save();
          
         return redirect('regSemovientes')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
         return redirect()->with('errormsj', 'Los datos no se guardaron');
       }
    }
}
