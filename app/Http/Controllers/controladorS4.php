<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS4;
use App\sel_proveedores;
use App\sel_sedes;
use App\sel_paises;
use App\sel_parroquias;
use App\sel_ciudad;
use App\modeloBitacora;		


class controladorS4 extends Controller
{
    public function paises(Request $request)
    {
         if($request->ajax()){
            $paisS4 = modeloS4::paisesS4();
          return response()->json($paisS4);
      }
    }

    public function parroquias(Request $request)
    {
         if($request->ajax()){
            $parroS4 = modeloS4::parroquiaS4();
          return response()->json($parroS4);
      }
    }

    public function ciudades(Request $request)
    {
         if($request->ajax()){
            $ciudadS4 = modeloS4::ciudadesS4();
          return response()->json($ciudadS4);
      }
    }

    public function modiPaisesS4(Request $request)
    {
         if($request->ajax()){
            $s4Pais= modeloS4::modiPaisesS4();
          return response()->json($s4Pais);
      }
    }


    public function modiParroS4(Request $request)
    {
         if($request->ajax()){
            $s4Parro= modeloS4::modiParroquiasS4();
          return response()->json($s4Parro);
      }
    }

    public function modiCiudadS4(Request $request)
    {
         if($request->ajax()){
            $s4Ciudad= modeloS4::modiCiudadesS4();
          return response()->json($s4Ciudad);
      }
    }

    public function index()
    {
    		$lastCod = modeloS4::select('codSede')->get()->last();
        $lugarSedes = sel_sedes::all();
    		$selectLoca = sel_proveedores::all();
        $selectPais = sel_paises::all();
        $selectParroquia = sel_parroquias::all();
        $selectCiudad = sel_ciudad::all();
    	

      	$codSede = array(
      		array("codSede","Código de la Sede:","Introduzca el código de la sede","10"),
      		);

      	$selectSede = array(
          array("tipoSede","Tipo de Sede:","2"),
          );

      	$espeSede = array(
      		array("espeSede","Especifique Tipo de Sede o Lugar:","Especifique el tipo de sede","100"),
      		);

      	$descSede = array(
      		array("descSede","Descripción de la Sede:","Describa la sede","255"),
      		);

        $localizacion = array(
            array("localizacion","localizacionS4","Localización:","2"),
            );

        $codPais = array(
            array("codPais","codPaiS4","Código del País donde se Ubica la Sede:","2"),
            );

    	  $espePais = array(
    		array("espeOtroPais","Especifique Otro País:","Especifique otro país","100"),
    		);

        $codParroquia = array(
            array("codParroquia","codParroquiaS4","Código de Parroquia donde se Ubica el Ente:","2"),
            );

        $codCiudad = array(
            array("codCiudad","codCiudadS4","Código de la Ciudad donde se Ubica el Ente:","2"),
            );


        $espeCiudad = array(
            array("espeOtroCiudad","Especifique Nombre de la Otra Ciudad:","Especifique el nombre de la otra ciudad","100"),
            );

        $urbanizacion = array(
            array("urbanizacion","Urbanización:","Introduzca la urbanización de la sede","30"),
            );

        $calleAvenida = array(
            array("calleAvenida","Calle / Avenida:","Introduzca la calle o avenida de la sede","50"),
            );

        $casaEdificio = array(
            array("casaEdificio","Casa / Edificio:","Introduzca la casa o avenida de la sede","30"),
            );

        $piso = array(
            array("piso","Piso:","Introduzca el piso donde se localiza la sede","20"),
            );


    	return view('AnexosS.visSedes', compact('lastCod','lugarSedes','selectLoca','selectPais','selectParroquia','selectCiudad','codSede','selectSede','espeSede','descSede','localizacion','codPais','espePais','codParroquia','codCiudad','espeCiudad','urbanizacion','calleAvenida','casaEdificio','piso'));
    }

    public function store(Request $request)
    {
     $duplicado = modeloS4::where('codSede', $request->codSede)->get();


     if($duplicado == '[]'){

        $form_s4 = new modeloS4();
        $form_s4->tipoSede = $request->tipoSede;
        $form_s4->localizacion = $request->localizacion;
        $form_s4->codPais = $request->codPais;

        if($form_s4->codSede = $request->codSede == '')
        {
            $form_s4->codSede = '0';
        }else{
            $form_s4->codSede = $request->codSede;
        }

        $form_s4->codParroquia = $request->codParroquia;
        
        $form_s4->codCiudad = $request->codCiudad;
       

        if($form_s4->espeSede = $request->espeSede == '')
        {
            $form_s4->espeSede = '1';
        }else{
            $form_s4->espeSede = $request->espeSede;
        }

        if($form_s4->descSede = $request->descSede == '')
        {
            $form_s4->descSede = '1';
        }else{
            $form_s4->descSede = $request->descSede;
        }

        if($form_s4->espeOtroPais = $request->espeOtroPais == '')
        {
            $form_s4->espeOtroPais = '1';
        }else{
            $form_s4->espeOtroPais = $request->espeOtroPais;
        }

        if($form_s4->espeOtroCiudad = $request->espeOtroCiudad == '')
        {
            $form_s4->espeOtroCiudad = '1';
        }else{
            $form_s4->espeOtroCiudad = $request->espeOtroCiudad;
        }

        if($form_s4->urbanizacion = $request->urbanizacion == '')
        {
            $form_s4->urbanizacion = '1';
        }else{
            $form_s4->urbanizacion = $request->urbanizacion;
        }

        if($form_s4->calleAvenida = $request->calleAvenida == '')
        {
            $form_s4->calleAvenida = '1';
        }else{
            $form_s4->calleAvenida = $request->calleAvenida;
        }

        if($form_s4->casaEdificio = $request->casaEdificio == '')
        {
            $form_s4->casaEdificio = '1';
        }else{
            $form_s4->casaEdificio = $request->casaEdificio;
        }

        if($form_s4->piso = $request->piso == '')
        {
            $form_s4->piso = '1';
        }else{
            $form_s4->piso = $request->piso;
        }

        if($form_s4->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos de las Sedes y Similares del Ente';
          $bit->save();
         }

        return back()->with('msj', 'Datos Registrados Exitosamente');
         }else{
      return back()->with('errormsj', 'El código de la sede "'.$request->codSede.'" ya existe, por favor intente otro.');
    }
}
     public function edit($id)
    {   

        $form_s4 = modeloS4::find($id);
        $lastCod = modeloS4::select('codSede')->get()->last();
        $lugarSedes = sel_sedes::all();
        $selectLoca = sel_proveedores::all();
        $selectPais = sel_paises::all();
        $selectParroquia = sel_parroquias::all();
        $selectCiudades = sel_ciudad::all();
        
        return view('layouts.ModificarAnexosS.modificarSedes', compact('form_s4','lastCod','lugarSedes','selectLoca','selectPais','selectParroquia','selectCiudades'));
    }

    public function update(Request $request, $id)
    {
        $form_s4 = modeloS4::find($id);
        $form_s4->tipoSede = $request->tipoSede;
        $form_s4->localizacion = $request->localizacion;
        $form_s4->codPais = $request->codPais;
    

        if($form_s4->codSede = $request->codSede == '')
        {
            $form_s4->codSede = '0';
        }else{
            $form_s4->codSede = $request->codSede;
        }

        if($form_s4->codParroquia = $request->codParroquia == '1093')
        {
            $form_s4->codParroquia = '1093';
        }else{
            $form_s4->codParroquia = $request->codParroquia;
        }
      
        
        $form_s4->codCiudad = $request->codCiudad;
       

        if($form_s4->espeSede = $request->espeSede == '')
        {
            $form_s4->espeSede = '1';
        }else{
            $form_s4->espeSede = $request->espeSede;
        }

        if($form_s4->descSede = $request->descSede == '')
        {
            $form_s4->descSede = '1';
        }else{
            $form_s4->descSede = $request->descSede;
        }

        if($form_s4->espeOtroPais = $request->espeOtroPais == '')
        {
            $form_s4->espeOtroPais = '1';
        }else{
            $form_s4->espeOtroPais = $request->espeOtroPais;
        }

        if($form_s4->espeOtroCiudad = $request->espeOtroCiudad == '')
        {
            $form_s4->espeOtroCiudad = '1';
        }else{
            $form_s4->espeOtroCiudad = $request->espeOtroCiudad;
        }

        if($form_s4->urbanizacion = $request->urbanizacion == '')
        {
            $form_s4->urbanizacion = '1';
        }else{
            $form_s4->urbanizacion = $request->urbanizacion;
        }

        if($form_s4->calleAvenida = $request->calleAvenida == '')
        {
            $form_s4->calleAvenida = '1';
        }else{
            $form_s4->calleAvenida = $request->calleAvenida;
        }

        if($form_s4->casaEdificio = $request->casaEdificio == '')
        {
            $form_s4->casaEdificio = '1';
        }else{
            $form_s4->casaEdificio = $request->casaEdificio;
        }

        if($form_s4->piso = $request->piso == '')
        {
            $form_s4->piso = '1';
        }else{
            $form_s4->piso = $request->piso;
        }


        if($form_s4->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Datos de las Sedes y Similares del Ente';
          $bit->save();

         

        return back()->with('msj', 'Datos modificados exitosamente');
        }else {
        return back()->with('errormsj', 'Los datos no se modificaron');
        }
    }

    public function exportSedes()
    {
         \Excel::create('sedes', function($excel) {
        
          $sedes = modeloS4::all();
          
          $excel->sheet('sedes', function($sheet) use($sedes) {
     
          /*========CABECERA DE LA FILA N° 1========*/


          $sheet->row(1, [
              'CÓDIGO DE LA SEDE', 'TIPO DE SEDE ', 'ESPECIFIQUE EL TIPO DE SEDE', 'DESCRIPCIÓN DE LA SEDE', 'LOCALIZACIÓN', 'CÓDIGO DEL PAÍS', 'ESPECIFIQUE OTRO PAÍS', 'CÓDIGO DE LA PARROQUIA', 'CÓDIGO DE LA CIUDAD', 'ESPECIFIQUE EL NOMBRE DE LA OTRA CIUDAD', 'URBANIZACIÓN', 'CALLE / AVENIDA', 'CASA / EDIFICIO', 'PISO'

          ]);

          $sheet->setWidth([
                        'A'     =>  60,
                        'B'     =>  60,
                        'C'     =>  70,
                        'D'     =>  150,
                        'E'     =>  60,
                        'F'     =>  60,
                        'G'     =>  70,
                        'H'     =>  60,
                        'I'     =>  60,
                        'J'     =>  70,
                        'K'     =>  100,
                        'L'     =>  100,
                        'M'     =>  100,
                        'N'     =>  50,
                      
                    ]);

          $sheet->setHeight(1, 35);
                    $sheet->cell('A1:N4000',function($cell) 
                    {
                        $cell->setAlignment('center');    
                                           
                    });

          /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
        foreach($sedes  as $index => $sedes) 
        {
                

                /*===ESPECIFIQUE EL TIPO DE SEDE===*/
                if($sedes->espeSede == '1')
                {
                  $espeSede = 'noaplica';

                }else{
                  
                 $espeSede=$sedes->espeSede;
                }

                /*===DESCRIPCIÓN DE LA SEDE===*/
                if($sedes->descSede == '1')
                {
                  $descSede = 'xxx';

                }else{
                  
                 $descSede=$sedes->descSede;
                }

                /*===ESPECIFIQUE NOMBRE DE OTRO PAÍS===*/
                if($sedes->espeOtroPais == '1')
                {
                  $espeOtroPais = 'noaplica';

                }else{
                  
                  $espeOtroPais=$sedes->espeOtroPais;
                }

                /*===ESPECIFIQUE NOMBRE DE LA OTRA CIUDAD===*/
                if($sedes->espeOtroCiudad == '1')
                {
                  $espeOtroCiudad = 'xxx';

                }else{
                  
                  $espeOtroCiudad=$sedes->espeOtroCiudad;
                }

                 /*===URBANIZACIÓN===*/
                if($sedes->urbanizacion == '1')
                {
                  $urbanizacion = 'xxx';

                }else{
                  
                  $urbanizacion=$sedes->urbanizacion;
                }

                 /*===CALLE / AVENIDA===*/
                if($sedes->calleAvenida == '1')
                {
                  $calleAvenida = 'xxx';

                }else{
                  
                  $calleAvenida=$sedes->calleAvenida;
                }

                 /*===CALLE / AVENIDA===*/
                if($sedes->calleAvenida == '1')
                {
                  $calleAvenida = 'xxx';

                }else{
                  
                  $calleAvenida=$sedes->calleAvenida;
                }

                /*===CALLE / EDIFICIO===*/
                if($sedes->casaEdificio == '1')
                {
                  $casaEdificio = 'xxx';

                }else{
                  
                  $casaEdificio=$sedes->casaEdificio;
                }

                /*===CALLE / EDIFICIO===*/
                if($sedes->piso == '1')
                {
                  $piso = 'xxx';

                }else{
                  
                  $piso=$sedes->piso;
                }
              

            $sheet->row($index+2, 
               [
                  $sedes->codSede, 
                  $sedes->tipoSede, 
                  $espeSede, 
                  $descSede, 
                  $sedes->selectLocalizacion->opcion,
                  $sedes->codPais,
                  $espeOtroPais,
                  $sedes->selectParroquia->codParroquia,
                  $sedes->selectCiudades->codCiudad,
                  $espeOtroCiudad,
                  $urbanizacion,
                  $calleAvenida,
                  $casaEdificio,
                  $piso,

               ]); 
        }

      });
   
      })->export('xlsx');
    }
  
}
