<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloPermuta;
use App\modeloBitacora;

class con_permutaVer extends Controller
{
     public function index(){
        
        $verT26= modeloPermuta::all();

        return view('RegistrosT.regPermuta', compact('verT26'));

    }

    public function selectId($id){

        $seleccion = modeloPermuta::find($id);

        $fechaLic = $seleccion->feLic;
        $fechaLic = date("d/m/Y", strtotime($fechaLic));

        $fechaCon = $seleccion->feCon;
        $fechaCon = date("d/m/Y", strtotime($fechaCon));

        $fechaReg = $seleccion->feCon;
        $fechaReg = date("d/m/Y", strtotime($fechaReg));

       return view('MuestraAnexosT.muestraPermuta',compact('seleccion','fechaLic','fechaCon','fechaReg'));
    }


    public function anularPermu($id)
    {
        $seleccion= modeloPermuta::find($id);

       if($seleccion->delete()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 3;
          $bit->referencia = 'Permuta';
          $bit->save();

         return redirect('regPermuta')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
         return redirect()->with('errormsj', 'Los datos no se guardaron');
       }
    }
}
