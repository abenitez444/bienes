<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloTransferencia;
use App\sel_transferencia;
use App\modeloBitacora;


class controladorTransferencia extends Controller
{
    public function index()
    {
        $infoSelect = sel_transferencia::all();

        $lastCod = modeloTransferencia::select('codOt2_7')->get()->last();

        $arrayT27 = array(
            array("codOt2_7","Código de Origen:","Introduzca número consecutivo. Ej: H-2; H-3","12","col-md-pull-4",""),
            #array("codAdq","CÓDIGO DE LA FORMA DE ADQUISICIÓN","Introduzca el N° el código de origen","12","col-md-pull-4"),
            array("nomQtra","Nombre de Quien Transfiere:","Introduzca nombre del quien transfiere","120","col-md-push-0",""),
            array("nomBen","Nombre del Beneficiario:","Introduzca nombre del beneficiario (quien recibe)","120","",""),
            array("numAuto","Número de la Autorización:","Introduzca el número de la autorización","30","",""),
            array("nomRegn","Nombre del Registro o Notaría:","Introduzca nombre del registro o notaría","100","",""),
            array("tomo","Tomo:","Introduzca el tomo del registro","20","",""),
            array("folio","Folio:","Introduzca el N° de folio","6","","return soloNum(event)"),
            );

        $selectT27 = array(
        array("codAdq","Código de Adquisición:","col-md-push-4"),
        );

        $dateT26 = array(
            array("feAuto","Fecha de Autorización:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
            );

        $dateT27 = array(
            array("feReg","Fecha de Registro:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
            );

       

        return view('AnexosT.visTransferencia', compact('infoSelect','arrayT27','selectT27','dateT26','dateT27','lastCod'));
    }

   
    public function store(Request $request)
    {

        $form_t27= new modeloTransferencia();
        $form_t27->codAdq = $request->codAdq;

        if($form_t27->codOt2_7 =$request->codOt2_7 == ''){
          $form_t27->codOt2_7 = 'H-1';
          }else{
          $form_t27->codOt2_7 = $request->codOt2_7;  
        }

        if($form_t27->nomQtra =$request->nomQtra == ''){
          $form_t27->nomQtra = '1';
          }else{
          $form_t27->nomQtra = $request->nomQtra;  
        }

        if($form_t27->nomBen = $request->nomBen == ''){
          $form_t27->nomBen = '1';
          }else{
          $form_t27->nomBen = $request->nomBen;  
        }

        if($form_t27->numAuto = $request->numAuto == ''){
          $form_t27->numAuto = '0';
          }else{
          $form_t27->numAuto = $request->numAuto;  
        }

        if($form_t27->nomRegn = $request->nomRegn == ''){
          $form_t27->nomRegn = '1';
          }else{
          $form_t27->nomRegn = $request->nomRegn;  
        }

        if($form_t27->nomRegn = $request->nomRegn == ''){
          $form_t27->nomRegn = '1';
          }else{
          $form_t27->nomRegn = $request->nomRegn;  
        }

        if($form_t27->tomo = $request->tomo == ''){
          $form_t27->tomo = '1';
          }else{
          $form_t27->tomo = $request->tomo;  
        }

        if($form_t27->folio = $request->folio == ''){
          $form_t27->folio = '0';
          }else{
          $form_t27->folio = $request->folio;  
        }

        if($form_t27->feAuto = $request->feAuto == ''){
          $form_t27->feAuto = '11111111';
          }else{
          $form_t27->feAuto = $request->feAuto;  
        }


        if($form_t27->feReg = $request->feReg == ''){
          $form_t27->feReg = '11111111';
          }else{
          $form_t27->feReg = $request->feReg;  
        }

        if($form_t27->save()){

	        $bit = new modeloBitacora();
	        $bit->user = $_SESSION['id'];
	        $bit->accion  = 1;
	        $bit->referencia = 'Transferencia';
	        $bit->save();

        }
          return back()->with('msj', 'Datos Registrados Exitosamente');
          
    }
    

    public function edit($id)
    {
        $form_t27 = modeloTransferencia::find($id);
        $infoSelect = sel_transferencia::all();

        return view('layouts.ModificarAnexosT.modificarTransferencia', compact('form_t27','infoSelect'));
    }

   
    public function update(Request $request, $id)
    {
        $form_t27= modeloTransferencia::find($id);
        $form_t27->codOt2_7 = $request->codOt2_7;
        $form_t27->codAdq = $request->codAdq;

        if($form_t27->nomQtra =$request->nomQtra == ''){
          $form_t27->nomQtra = '1';
          }else{
          $form_t27->nomQtra = $request->nomQtra;  
        }

        if($form_t27->nomBen = $request->nomBen == ''){
          $form_t27->nomBen = '1';
          }else{
          $form_t27->nomBen = $request->nomBen;  
        }

        if($form_t27->numAuto = $request->numAuto == ''){
          $form_t27->numAuto = '0';
          }else{
          $form_t27->numAuto = $request->numAuto;  
        }

        if($form_t27->nomRegn = $request->nomRegn == ''){
          $form_t27->nomRegn = '1';
          }else{
          $form_t27->nomRegn = $request->nomRegn;  
        }

        if($form_t27->nomRegn = $request->nomRegn == ''){
          $form_t27->nomRegn = '1';
          }else{
          $form_t27->nomRegn = $request->nomRegn;  
        }

        if($form_t27->tomo = $request->tomo == ''){
          $form_t27->tomo = '1';
          }else{
          $form_t27->tomo = $request->tomo;  
        }

        if($form_t27->folio = $request->folio == ''){
          $form_t27->folio = '0';
          }else{
          $form_t27->folio = $request->folio;  
        }

        if($form_t27->feAuto = $request->feAuto == ''){
          $form_t27->feAuto = '11111111';
          }else{
          $form_t27->feAuto = $request->feAuto;  
        }


        if($form_t27->feReg = $request->feReg == ''){
          $form_t27->feReg = '11111111';
          }else{
          $form_t27->feReg = $request->feReg;  
        }

        if($form_t27->save()){

        	$bit = new modeloBitacora();
         	$bit->user = $_SESSION['id'];
            $bit->accion  = 2;
            $bit->referencia = 'Transferencia';
            $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }

    public function exportTransfe()
    {
       \Excel::create('transferencia', function($excel) {
    
      $transferencia = modelotransferencia::all();
      $relacion = sel_transferencia::all();
      
      
      $excel->sheet('transferencia', function($sheet) use($transferencia) {
 
      /*========CABECERA DE LA FILA N° 1========*/

     
      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN','CÓDIGO DE LA FORMA DE ADQUISICIÓN ','NOMBRE DE QUIEN TRANSFIERE', 'NOMBRE DEL BENEFICIARIO ','NÚMERO DE AUTORIZACIÓN','NOMBRE DEL REGISTRO O NOTARÍA','TOMO','FOLIO','FECHA DE AUTORIZACIÓN','FECHA DE REGISTRO'


      ]);

      $sheet->setWidth([
                    'A'     =>  30,
                    'B'     =>  35,
                    'C'     =>  35,
                    'D'     =>  35,
                    'E'     =>  35,
                    'F'     =>  35,
                    'G'     =>  20,
                    'H'     =>  20,
                    'I'     =>  30,
                    'J'     =>  30,
                    
                ]);

                $sheet->setHeight(1, 35);
                $sheet->cell('A1:J6000',function($cell) 
                {
                    $cell->setAlignment('center');                
                    
                });
                
      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
     foreach($transferencia  as $index => $transferencia) {
            
           /*========CÓDIGO DE ADQUISICIÓN=======*/
          
            if($transferencia->codAdq == '1')
            {
              $codAdq = '5';

            }

            /*====NOMBRE DE QUIEN TRANSFIERE====*/
            
            if($transferencia->nomQtra == '1')
            {
              $nomQtra = 'xxx';

            }else{

              $nomQtra=$transferencia->nomQtra;
            }

            /*====NOMBRE DEL BENEFICIARIO====*/
            
            if($transferencia->nomBen == '1')
            {
              $nomBen = 'xxx';

            }else{

              $nomBen=$transferencia->nomBen;
            }


            /*====NUMERO DE AUTORIZACIÓN====*/
            
            if($transferencia->numAuto == '0')
            {
              $numAuto = 'xxx';

            }else{

              $numAuto=$transferencia->numAuto;
            }

          /*====NOMBRE DEL REGISTRO O NOTARÍA====*/

            if($transferencia->nomRegn == '1')
            {
              $nomRegn = 'xxx';

            }else{

              $nomRegn=$transferencia->nomRegn;
            }

            /*====TOMO====*/

            if($transferencia->tomo == '1')
            {
              $tomo = 'xxx';

            }else{

              $tomo=$transferencia->tomo;
            }

            /*====FOLIO====*/

            if($transferencia->folio == '0')
            {
              $folio = '99';

            }else{

              $folio=$transferencia->folio;
            }

            /*========FECHA DE AUTORIZACIÓN=======*/

            if($transferencia->feReg == '1111-11-11')
            {
              $feReg = '11111111';

            }else{

              $feAuto = $transferencia->feAuto;
              $feAuto = date("dmY", strtotime($feAuto));
            }

                        
            /*========FECHA DE REGISTRO=======*/

            if($transferencia->feReg == '1111-11-11')
            {
              $feReg = '11111111';

            }else{

              $feReg = $transferencia->feReg;
              $feReg = date("dmY", strtotime($feReg));
            }

          $sheet->row($index+2, [
              $transferencia->codOt2_7, 
              $codAdq, 
              $nomQtra, 
              $nomBen, 
              $numAuto, 
              $nomRegn, 
              $tomo, 
              $folio, 
              $feAuto,
              $feReg, 

          ]);

        }


      });
   
      })->export('xlsx');
   }
   


}
