<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS5;
use App\modeloBienes;
use App\modeloBitacora;


class con_s5Ver extends Controller
{
     public function index(){
        
        $verS5= modeloS5::all();

        return view('RegistrosS.regUnidades', compact('verS5'));

    }

   	 public function selectId($id)
   	 {
   	 	
   	 	$seleccion = modeloS5::find($id);

   		return view('MuestraAnexosS.muestraUnidades',compact('seleccion'));
   	 }


    public function anularUnidades($id)
    {

      $datos = modeloBienes::all()->where('codUnidad', $id)->count();
        if(empty($datos)){

      $unidades= modeloS5::find($id);
        
       if($unidades->delete()){
        $bit = new modeloBitacora();
        $bit->user = $_SESSION['id'];
        $bit->accion  = 3;
        $bit->referencia = 'Datos de las Sedes y Similares del Ente';
        $bit->save();

                return redirect('regUnidades')->with('msj', 'Registro Eliminado Exitosamente');
           } else {
                return redirect()->with('errormsj', 'Los datos no se guardaron');
           }
        }
        else{
            return redirect('regUnidades')->with('errormsj', 'No puede eliminar ésta unidad administrativa, ya se encuentra registrada en otro formulario.');
        }
    }

}