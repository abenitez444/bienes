<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloInmuebles;
use App\sel_catalogo;
use App\modeloS4;
use App\modeloS5;
use App\modeloResponsables;
use App\sel_seguros3;
use App\sel_proveedores;
use App\sel_paises;
use App\sel_parroquias;
use App\sel_ciudad;
use App\sel_estatusbien;
use App\sel_condicionbien;
use App\sel_seguros2;
use App\sel_medidapeso;
use App\sel_usos;
use App\modeloBitacora;

class controladorInmuebles extends Controller
{   

    public function paisInmu(Request $request)
    {
         if($request->ajax()){
            $paisIn = modeloInmuebles::paisInmueble();
          return response()->json($paisIn);
      }
    }

    public function parroInmu(Request $request)
    {
         if($request->ajax()){
            $parroIn = modeloInmuebles::parroInmueble();
          return response()->json($parroIn);
      }
    }

    public function ciudadInmu(Request $request)
    {
         if($request->ajax()){
            $ciudadIn = modeloInmuebles::ciudadInmueble();
          return response()->json($ciudadIn);
      }
    }
    
    /*============Modificar==============*/
    public function modifiPais(Request $request)
    {
         if($request->ajax()){
            $s4Pais3 = modeloInmuebles::modiPais();
          return response()->json($s4Pais3);
      }
    }

    public function modifiParro(Request $request)
    {
         if($request->ajax()){
            $s4Parro3 = modeloInmuebles::modiParroquia();
          return response()->json($s4Parro3);
      }
    }

    

    public function modifiCiudad(Request $request)
    {
         if($request->ajax()){
            $s4Ciudad3 = modeloInmuebles::modiCiudad();
          return response()->json($s4Ciudad3);
      }
    }

    public function depenResponsableIn(Request $request, $id)
    {
    
      if($request->ajax()){
         $dependencia = modeloInmuebles::selectResponinmu($id);
          return response()->json($dependencia);
      }
    }


    public function index()
    {	
    	$lastCod = modeloInmuebles::select('codBien')->get()->last();
      $catalogo = sel_catalogo::all();
      $unidad = modeloS5::all();
    	$sede = modeloS4::all();
      $responsable = modeloResponsables::select('codResp')->get();
    	$corresBien = sel_seguros3::all();
    	$localizacion = sel_proveedores::all();
    	$selectPais = sel_paises::all();
    	$selectParroquia = sel_parroquias::all();
    	$selectCiudad = sel_ciudad::all();
      $estatusBien = sel_estatusbien::all();
    	$estadoBien = sel_condicionbien::all();
    	$moneda = sel_seguros2::all();
    	$usoInmueble = sel_usos::all();
    	$unidadConstru = sel_medidapeso::all();
    	$unidadTerreno = sel_medidapeso::all();
    	$seguroBien = sel_seguros3::all();


    	return view('AnexosT.visInmuebles', compact('lastCod','catalogo','unidad','sede','responsable','corresBien','localizacion','selectPais','selectParroquia','selectCiudad','estatusBien','estadoBien','moneda','usoInmueble','unidadConstru','unidadTerreno','seguroBien'));
    }

    public function store(Request $request)
    {
        $form_t12 = new modeloInmuebles();
 
        $form_t12->codUnidad = $request->codUnidad;
        $form_t12->codCata = $request->codCata; 
        $form_t12->sedeOrgano = $request->sedeOrgano; 
        $form_t12->corresBien = $request->corresBien;
        $form_t12->localizacion = $request->localizacion;
        $form_t12->codPais = $request->codPais;
        $form_t12->estatuBien = $request->estatuBien;
        $form_t12->moneda = $request->moneda;
        $form_t12->edoBien = $request->edoBien;
        $form_t12->usoBienInmu = $request->usoBienInmu;
        $form_t12->unidadConstru = $request->unidadConstru;
        $form_t12->unidadTerreno = $request->unidadTerreno;
        $form_t12->seguroBien = $request->seguroBien;

        if($form_t12->codBien = $request->codBien == '')
        {
          $form_t12->codBien = '1';
        }else{
          $form_t12->codBien = $request->codBien;  
        }

        if($form_t12->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Debe agregar el código del responsable administrativo, para guardar el registro.');
        }else{
          $form_t12->codRespAdm = $request->codRespAdm;
        }

        if($form_t12->espeOtroPais = $request->espeOtroPais == '')
        {
          $form_t12->espeOtroPais = '1';
        }else{
          $form_t12->espeOtroPais = $request->espeOtroPais;  
        }

        if($form_t12->codParroquia = $request->codParroquia == '99')
        {
          $form_t12->codParroquia = '1';
        }else{
          $form_t12->codParroquia = $request->codParroquia;  
        }

        if($form_t12->codCiudad = $request->codCiudad == '99')
        {
          $form_t12->codCiudad = '1';
        }else{
          $form_t12->codCiudad = $request->codCiudad;  
        }

        if($form_t12->espeOtroCiudad = $request->espeOtroCiudad == '')
        {
          $form_t12->espeOtroCiudad = '1';
        }else{
          $form_t12->espeOtroCiudad = $request->espeOtroCiudad;  
        }

        if($form_t12->urbanizacion = $request->urbanizacion == '')
        {
          $form_t12->urbanizacion = '1';
        }else{
          $form_t12->urbanizacion = $request->urbanizacion;  
        }

        if($form_t12->calleAvenida = $request->calleAvenida == '')
        {
          $form_t12->calleAvenida = '1';
        }else{
          $form_t12->calleAvenida = $request->calleAvenida;  
        }

        if($form_t12->casaEdificio = $request->casaEdificio == '')
        {
          $form_t12->casaEdificio = '1';
        }else{
          $form_t12->casaEdificio = $request->casaEdificio;  
        }

        if($form_t12->casaEdificio = $request->casaEdificio == '')
        {
          $form_t12->casaEdificio = '1';
        }else{
          $form_t12->casaEdificio = $request->casaEdificio;  
        }

        if($form_t12->codInterno = $request->codInterno == '')
        {
          $form_t12->codInterno = '1';
        }else{
          $form_t12->codInterno = $request->codInterno;  
        }

        if($form_t12->espOtroUso = $request->espOtroUso == '')
        {
          $form_t12->espOtroUso = '1';
        }else{
          $form_t12->espOtroUso = $request->espOtroUso;  
        }

        if($form_t12->valorAdq = $request->valorAdq == '')
        {
          $form_t12->valorAdq = '0';
        }else{
          $form_t12->valorAdq = $request->valorAdq;  
        }

        if($form_t12->espeMoneda = $request->espeMoneda == '')
        {
          $form_t12->espeMoneda = '1';
        }else{
          $form_t12->espeMoneda = $request->espeMoneda;  
        }

        if($form_t12->feAdqBien = $request->feAdqBien == '')
         {
          $form_t12->feAdqBien = '11111111';

         }else{
          $form_t12->feAdqBien = $request->feAdqBien;
        }

        if($form_t12->feIngBien = $request->feIngBien == '')
         {
          $form_t12->feIngBien = '11111111';

         }else{
          $form_t12->feIngBien = $request->feIngBien;
        }

        if($form_t12->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t12->espOtroEdo = '1';

         }else{
          $form_t12->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t12->descEdoBien = $request->descEdoBien == '')
         {
          $form_t12->descEdoBien = '1';

         }else{
          $form_t12->descEdoBien = $request->descEdoBien;
        }

        if($form_t12->otroUsoInmu = $request->otroUsoInmu == '')
         {
          $form_t12->otroUsoInmu = '1';

         }else{
          $form_t12->otroUsoInmu = $request->otroUsoInmu;
        }

        if($form_t12->ofiRegistro = $request->ofiRegistro == '')
         {
          $form_t12->ofiRegistro = '1';

         }else{
          $form_t12->ofiRegistro = $request->ofiRegistro;
        }

        if($form_t12->refRegistro = $request->refRegistro == '')
         {
          $form_t12->refRegistro = '1';

         }else{
          $form_t12->refRegistro = $request->refRegistro;
        }
        
         if($form_t12->tomo = $request->tomo == '')
         {
          $form_t12->tomo = '0';

         }else{
          $form_t12->tomo = $request->tomo;
        }

         if($form_t12->folio = $request->folio == '')
         {
          $form_t12->folio = '0';

         }else{
          $form_t12->folio = $request->folio;
        }

        if($form_t12->protocolo = $request->protocolo == '')
         {
          $form_t12->protocolo = '1';

         }else{
          $form_t12->protocolo = $request->protocolo;
        }

         if($form_t12->numRegistro = $request->numRegistro == '')
         {
          $form_t12->numRegistro = '0';

         }else{
          $form_t12->numRegistro = $request->numRegistro;
        }

        if($form_t12->feRegistro = $request->feRegistro == '')
         {
          $form_t12->feRegistro = '11111111';

         }else{
          $form_t12->feRegistro = $request->feRegistro;
        }

        if($form_t12->propieAnt = $request->propieAnt == '')
         {
          $form_t12->propieAnt = '1';

         }else{
          $form_t12->propieAnt = $request->propieAnt;
        }

        if($form_t12->depenIntegra = $request->depenIntegra == '')
         {
          $form_t12->depenIntegra = '1';

         }else{
          $form_t12->depenIntegra = $request->depenIntegra;
        }

        if($form_t12->areaConstru = $request->areaConstru == '')
         {
          $form_t12->areaConstru = '0';

         }else{
          $form_t12->areaConstru = $request->areaConstru;
        }

        if($form_t12->espeOtraUnidad = $request->espeOtraUnidad == '')
         {
          $form_t12->espeOtraUnidad = '1';

         }else{
          $form_t12->espeOtraUnidad = $request->espeOtraUnidad;
        }

        if($form_t12->areaTerreno = $request->areaTerreno == '')
         {
          $form_t12->areaTerreno = '0';

         }else{
          $form_t12->areaTerreno = $request->areaTerreno;
        }

        if($form_t12->espeOtraTerre = $request->espeOtraTerre == '')
         {
          $form_t12->espeOtraTerre = '1';

         }else{
          $form_t12->espeOtraTerre = $request->espeOtraTerre;
        }
        
        if($form_t12->otrasEspecifi = $request->otrasEspecifi == '')
         {
          $form_t12->otrasEspecifi = '1';

         }else{
          $form_t12->otrasEspecifi = $request->otrasEspecifi;
        }

        if($form_t12->codRegSeguro = $request->codRegSeguro == '')
         {
          $form_t12->codRegSeguro = '1';

         }else{
          $form_t12->codRegSeguro = $request->codRegSeguro;
        }

         if($form_t12->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos de los Bienes Inmuebles del Ente';
          $bit->save();

        return back()->with('msj', 'Datos Registrados Exitosamente');
        }else {
        return back()->with('errormsj', 'Los datos no se guardaron');
        }
        

    }

    public function edit($id)
    {
        $form_t12 = modeloInmuebles::find($id);
        $lastCod = modeloInmuebles::select('codBien')->get()->last();
        $catalogo = sel_catalogo::all();
        $sede = modeloS4::all();
        $unidad = modeloS5::all();
        $responsable = modeloResponsables::select('codResp')->get();
        $corresBien = sel_seguros3::all();
        $localizacion = sel_proveedores::all();
        $selectPais = sel_paises::all();
        $selectParroquia = sel_parroquias::all();
        $selectCiudad = sel_ciudad::all();
        $estatusBien = sel_estatusbien::all();
        $estadoBien = sel_condicionbien::all();
        $moneda = sel_seguros2::all();
        $usoInmueble = sel_usos::all();
        $unidadConstru = sel_medidapeso::all();
        $unidadTerreno = sel_medidapeso::all();
        $seguroBien = sel_seguros3::all();

       return view('layouts.ModificarAnexosT.modificarInmuebles',compact('form_t12','catalogo','lastCod','unidad','responsable','corresBien','sede','localizacion','selectPais','selectParroquia','selectCiudad','estatusBien','estadoBien','moneda','usoInmueble','unidadConstru','unidadTerreno','seguroBien'));
    }


      public function update(Request $request, $id)
      {

        $form_t12 = modeloInmuebles::find($id);
        $form_t12->codBien = $request->codBien;
        $form_t12->codCata = $request->codCata;
        $form_t12->codUnidad = $request->codUnidad;
        $form_t12->sedeOrgano = $request->sedeOrgano;
        $form_t12->corresBien = $request->corresBien;
        $form_t12->localizacion = $request->localizacion;
        $form_t12->codPais = $request->codPais;
        $form_t12->estatuBien = $request->estatuBien;
        $form_t12->moneda = $request->moneda;
        $form_t12->edoBien = $request->edoBien;
        $form_t12->usoBienInmu = $request->usoBienInmu;
        $form_t12->unidadConstru = $request->unidadConstru;
        $form_t12->unidadTerreno = $request->unidadTerreno;
        $form_t12->seguroBien = $request->seguroBien;

        if($form_t12->codRespAdm = $request->codRespAdm == '')
        {
          return back()->with('errormsj', 'Debe seleccionar el código del responsable administrativo, para modificar el registro.');
        }else{
          $form_t12->codRespAdm = $request->codRespAdm;  
        }

        if($form_t12->espeOtroPais = $request->espeOtroPais == '')
        {
          $form_t12->espeOtroPais = '1';
        }else{
          $form_t12->espeOtroPais = $request->espeOtroPais;  
        }

        if($form_t12->codParroquia = $request->codParroquia == '1094')
        {
          $form_t12->codParroquia = '1094';
        }else{
          $form_t12->codParroquia = $request->codParroquia;  
        }

        if($form_t12->codCiudad = $request->codCiudad == '332')
        {
          $form_t12->codCiudad = '332';
        }else{
          $form_t12->codCiudad = $request->codCiudad;  
        }

        if($form_t12->espeOtroCiudad = $request->espeOtroCiudad == '')
        {
          $form_t12->espeOtroCiudad = '1';
        }else{
          $form_t12->espeOtroCiudad = $request->espeOtroCiudad;  
        }

        if($form_t12->urbanizacion = $request->urbanizacion == '')
        {
          $form_t12->urbanizacion = '1';
        }else{
          $form_t12->urbanizacion = $request->urbanizacion;  
        }

        if($form_t12->calleAvenida = $request->calleAvenida == '')
        {
          $form_t12->calleAvenida = '1';
        }else{
          $form_t12->calleAvenida = $request->calleAvenida;  
        }

        if($form_t12->casaEdificio = $request->casaEdificio == '')
        {
          $form_t12->casaEdificio = '1';
        }else{
          $form_t12->casaEdificio = $request->casaEdificio;  
        }

        if($form_t12->casaEdificio = $request->casaEdificio == '')
        {
          $form_t12->casaEdificio = '1';
        }else{
          $form_t12->casaEdificio = $request->casaEdificio;  
        }

        if($form_t12->codInterno = $request->codInterno == '')
        {
          $form_t12->codInterno = '1';
        }else{
          $form_t12->codInterno = $request->codInterno;  
        }

        if($form_t12->espOtroUso = $request->espOtroUso == '')
        {
          $form_t12->espOtroUso = '1';
        }else{
          $form_t12->espOtroUso = $request->espOtroUso;  
        }

        if($form_t12->valorAdq = $request->valorAdq == '')
        {
          $form_t12->valorAdq = '0';
        }else{
          $form_t12->valorAdq = $request->valorAdq;  
        }

        if($form_t12->espeMoneda = $request->espeMoneda == '')
        {
          $form_t12->espeMoneda = '1';
        }else{
          $form_t12->espeMoneda = $request->espeMoneda;  
        }

        if($form_t12->feAdqBien = $request->feAdqBien == '')
         {
          $form_t12->feAdqBien = '11111111';

         }else{
          $form_t12->feAdqBien = $request->feAdqBien;
        }

        if($form_t12->feIngBien = $request->feIngBien == '')
         {
          $form_t12->feIngBien = '11111111';

         }else{
          $form_t12->feIngBien = $request->feIngBien;
        }

        if($form_t12->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t12->espOtroEdo = '1';

         }else{
          $form_t12->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t12->descEdoBien = $request->descEdoBien == '')
         {
          $form_t12->descEdoBien = '1';

         }else{
          $form_t12->descEdoBien = $request->descEdoBien;
        }

        if($form_t12->otroUsoInmu = $request->otroUsoInmu == '')
         {
          $form_t12->otroUsoInmu = '1';

         }else{
          $form_t12->otroUsoInmu = $request->otroUsoInmu;
        }

        if($form_t12->ofiRegistro = $request->ofiRegistro == '')
         {
          $form_t12->ofiRegistro = '1';

         }else{
          $form_t12->ofiRegistro = $request->ofiRegistro;
        }

        if($form_t12->refRegistro = $request->refRegistro == '')
         {
          $form_t12->refRegistro = '1';

         }else{
          $form_t12->refRegistro = $request->refRegistro;
        }
        
         if($form_t12->tomo = $request->tomo == '')
         {
          $form_t12->tomo = '0';

         }else{
          $form_t12->tomo = $request->tomo;
        }

         if($form_t12->folio = $request->folio == '')
         {
          $form_t12->folio = '0';

         }else{
          $form_t12->folio = $request->folio;
        }

        if($form_t12->protocolo = $request->protocolo == '')
         {
          $form_t12->protocolo = '1';

         }else{
          $form_t12->protocolo = $request->protocolo;
        }

         if($form_t12->numRegistro = $request->numRegistro == '')
         {
          $form_t12->numRegistro = '0';

         }else{
          $form_t12->numRegistro = $request->numRegistro;
        }

        if($form_t12->feRegistro = $request->feRegistro == '')
         {
          $form_t12->feRegistro = '11111111';

         }else{
          $form_t12->feRegistro = $request->feRegistro;
        }

        if($form_t12->propieAnt = $request->propieAnt == '')
         {
          $form_t12->propieAnt = '1';

         }else{
          $form_t12->propieAnt = $request->propieAnt;
        }

        if($form_t12->depenIntegra = $request->depenIntegra == '')
         {
          $form_t12->depenIntegra = '1';

         }else{
          $form_t12->depenIntegra = $request->depenIntegra;
        }

        if($form_t12->areaConstru = $request->areaConstru == '')
         {
          $form_t12->areaConstru = '0';

         }else{
          $form_t12->areaConstru = $request->areaConstru;
        }

        if($form_t12->espeOtraUnidad = $request->espeOtraUnidad == '')
         {
          $form_t12->espeOtraUnidad = '1';

         }else{
          $form_t12->espeOtraUnidad = $request->espeOtraUnidad;
        }

        if($form_t12->areaTerreno = $request->areaTerreno == '')
         {
          $form_t12->areaTerreno = '0';

         }else{
          $form_t12->areaTerreno = $request->areaTerreno;
        }

        if($form_t12->espeOtraTerre = $request->espeOtraTerre == '')
         {
          $form_t12->espeOtraTerre = '1';

         }else{
          $form_t12->espeOtraTerre = $request->espeOtraTerre;
        }
        
        if($form_t12->otrasEspecifi = $request->otrasEspecifi == '')
         {
          $form_t12->otrasEspecifi = '1';

         }else{
          $form_t12->otrasEspecifi = $request->otrasEspecifi;
        }

        if($form_t12->codRegSeguro = $request->codRegSeguro == '')
         {
          $form_t12->codRegSeguro = '1';

         }else{
          $form_t12->codRegSeguro = $request->codRegSeguro;
        }

         if($form_t12->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos de los Bienes Inmuebles del Ente';
          $bit->save();

         

        return back()->with('msj', 'Datos Registrados Exitosamente');
        }else {
        return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }

    public function exportInmuebles()
    {

      \Excel::create('datosinmuebles', function($excel) {
    
          $datosinmuebles = modeloInmuebles::all();
         
      
      $excel->sheet('datosinmuebles', function($sheet) use($datosinmuebles) {
 
      /*========CABECERA DE LA FILA N° 1========*/

      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN DEL BIEN', 'CÓDIGO SEGÚN EL CATALOGO', 'DEPENDENCIA ADMINISTRATIVA', 'CÓDIGO DEL RESPONSABLE ADMINISTRATIVO','BIEN INMUEBLE QUE CORRESPONDE A ALGÚN ENTE', 'CÓDIGO DE SEDE DEL ENTE DONDE CORRESPONDE EL BIEN', 'LOCALIZACIÓN', 'CÓDIGO DEL PAÍS DONDE SE UBICA LA SEDE', 'ESPECIFIQUE EL OTRO PAÍS', 'CÓDIGO DE LA PARROQUIA DONDE SE UBICA EL INMUEBLE', 'CÓDIGO DE LA CIUDAD DONDE SE UBICA EL BIEN', 'ESPECIFIQUE OTRA CIUDAD', 'URBANIZACIÓN', 'CALLE / AVENIDA', 'CASA / EDIFICIO', 'CÓDIGO INTERNO DEL BIEN', 'ESTATUS DEL USO DEL BIEN', 'ESPECIFIQUE EL OTRO USO', 'VALOR DE ADQUISICIÓN', 'MONEDA', 'ESPECIFIQUE LA OTRA MONEDA', 'FECHA DE ADQUISICIÓN DEL BIEN', 'FECHA DE INGRESO DEL BIEN', 'ESTADO DEL BIEN', 'ESPECIFIQUE EL OTRO ESTADO DEL BIEN', 'DESCRIPCIÓN DEL ESTADO DEL BIEN', 'USO DEL BIEN INMUEBLE', 'OTRO USO', 'OFICINA DE REGISTRO / NOTARÍA', 'REFERENCIA DEL REGISTRO', 'TOMO', 'FOLIO', 'PROTOCOLO', 'NÚMERO DE REGISTRO', 'FECHA DE REGISTRO', 'PROPIETARIO ANTERIOR', 'DEPENDENCIAS QUE LO INTEGRAN', 'ÁREA DE CONSTRUCCIÓN', 'UNIDAD DE MEDIDA DEL ÁREA DE CONSTRUCCIÓN', 'ESPECIFIQUE LA OTRA UNIDAD DE MEDIDA (Construcción)', 'ÁREA DEL TERRENO', 'UNIDAD DE MEDIDA DEL ÁREA DEL TERRENO', 'ESPECIFIQUE LA OTRA UNIDAD DE MEDIDA (Terreno)', 'OTRAS ESPECIFICACIONES', 'SE ENCUENTRA EL BIEN ASEGURADO', 'CÓDIGO DEL REGISTRO SEGURO'

      ]);

      $sheet->setWidth([
                    'A'     =>  40, /*==Código del Origen del Bien==*/
                    'B'     =>  40, /*==Código Según el Catalogo==*/
                    'C'     =>  40, /*==Dependencia Administrativa==*/
                    'D'     =>  40, /*==Código del Responsable Administrativo==*/
                    'E'     =>  60, /*==Bien Inmueble Corresponde a Alguna sede del Ente==*/
                    'F'     =>  60, /*==Código de Sede del Ente donde Corresponde el Bien==*/
                    'G'     =>  60, /*==Localización==*/
                    'H'     =>  60, /*==Código del País donde se Ubica la Sede==*/
                    'I'     =>  60, /*==Especifique el Otro País==*/
                    'J'     =>  70, /*==Código de la Parroquia donde se Ubica el Inmueble==*/
                    'K'     =>  70, /*==Código de la Ciudad donde se Ubica el Bien==*/
                    'L'     =>  70, /*==Especifique la Otra Ciudad==*/
                    'M'     =>  60, /*==Urbanización==*/
                    'N'     =>  60, /*==Calle / Avenida==*/
                    'O'     =>  60, /*==Casa / Edificio==*/
                    'P'     =>  40, /*==Código Interno del Bien==*/
                    'Q'     =>  40, /*==Estatus del uso del Bien==*/
                    'R'     =>  40, /*==Especifique el Otro Uso==*/
                    'S'     =>  50, /*==Valor de Adquisición del Bien==*/
                    'T'     =>  50, /*==Moneda==*/
                    'U'     =>  50, /*==Especifique la Otra Moneda==*/
                    'V'     =>  50, /*==Fecha de Adquisición del Bien==*/
                    'W'     =>  50, /*==Fecha de Ingreso del Bien==*/
                    'X'     =>  50, /*==Estado del Bien==*/
                    'Y'     =>  70, /*==Especifique el Otro Estado del Bien==*/
                    'Z'     =>  70, /*==Descripción del Estado del Bien==*/
                    'AA'    =>  50, /*==Uso del Bien Inmueble==*/
                    'AB'    =>  40, /*==Otro Uso==*/
                    'AC'    =>  50, /*==Oficina de Registro/Notaria==*/
                    'AD'    =>  50, /*==Referencia del Registro==*/
                    'AE'    =>  50, /*==Tomo==*/
                    'AF'    =>  50, /*==Folio==*/
                    'AG'    =>  50, /*==Protocolo==*/
                    'AH'    =>  50, /*==Número de Registro==*/
                    'AI'    =>  50, /*==Fecha de Registro==*/
                    'AJ'    =>  50, /*==Propietario Anterior==*/
                    'AK'    =>  60, /*==Dependencias que lo Integran==*/
                    'AL'    =>  60, /*==Área de Construcción==*/
                    'AM'    =>  70, /*==Unidad de Medida del Área Construcción==*/
                    'AN'    =>  70, /*==Especifique la Otra Unidad de Medida ==*/
                    'AO'    =>  50, /*==Área Terreno ==*/
                    'AP'    =>  70, /*==Unidad de Medida del Área Terreno ==*/
                    'AQ'    =>  50, /*==Especifique la Otra Unidad==*/
                    'AR'    =>  50, /*==Otras Especificaciones==*/
                    'AS'    =>  50, /*==Se Encuentra el Bien Asegurado==*/
                    'AT'    =>  70, /*==Código del Registro de Seguro==*/
               
                
                                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:AT3000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($datosinmuebles  as $index => $datosinmuebles) {
           
            /*========Código del Responsable Administrativo=======*/

            if($datosinmuebles->codRespAdm == '1')
            {
              $codRespAdm = 'xxx';

            }else{

              $codRespAdm=$datosinmuebles->codRespAdm;
            }

            /*========Código de Sede donde Corresponde el Bien=======*/

            if($datosinmuebles->codSede == '1')
            {
              $codSede = 'xxx';

            }else{

              $codSede=$datosinmuebles->codSede;
            }

            /*====Código del Responsable del uso directo del Bien====*/
           
            if($datosinmuebles->codSede == '1')
            {
              $codSede = 'xxx';

            }else{

              $codSede=$datosinmuebles->codSede;
            }

            /*====Especifique el Otro País====*/
           
            if($datosinmuebles->espeOtroPais == '1')
            {
              $espeOtroPais = 'noaplica';

            }else{

              $espeOtroPais=$datosinmuebles->espeOtroPais;
            }

            /*====Especifique la Otra Ciudad====*/
           
            if($datosinmuebles->espeOtroCiudad == '1')
            {
              $espeOtroCiudad = 'noaplica';

            }else{

              $espeOtroCiudad=$datosinmuebles->espeOtroCiudad;
            }

            /*====Urbanización====*/
           
            if($datosinmuebles->urbanizacion == '1')
            {
              $urbanizacion = 'xxx';

            }else{

              $urbanizacion=$datosinmuebles->urbanizacion;
            }

            /*====Calle Avenida====*/
           
            if($datosinmuebles->calleAvenida == '1')
            {
              $calleAvenida = 'noaplica';

            }else{

              $calleAvenida=$datosinmuebles->calleAvenida;
            }

            /*====Casa / Edificio====*/
           
            if($datosinmuebles->casaEdificio == '1')
            {
              $casaEdificio = 'noaplica';

            }else{

              $casaEdificio=$datosinmuebles->casaEdificio;
            }

            /*====Código Interno del Bien====*/
           
            if($datosinmuebles->codInterno == '1')
            {
              $codInterno = 'xxx';

            }else{

              $codInterno=$datosinmuebles->codInterno;
            }

             /*====Especifique el Otro Uso====*/
           
            if($datosinmuebles->espOtroUso == '1')
            {
              $espOtroUso = 'xxx';

            }else{

              $espOtroUso=$datosinmuebles->espOtroUso;
            }

            /*====Valor de Adquisición del Bien====*/
           
            if($datosinmuebles->valorAdq == '0')
            {
              $valorAdq = '99.99';

            }else{

              $valorAdq=$datosinmuebles->valorAdq;
            }
      
            /*====Especifique la Otra Moneda====*/
           
            if($datosinmuebles->espeMoneda == '1')
            {
              $espeMoneda = 'noaplica';

            }else{

              $espeMoneda=$datosinmuebles->espeMoneda;
            }

             /*====Fecha de Adquisición del Bien===*/

            if($datosinmuebles->feAdqBien == '1111-11-11')
            {
              $feAdqBien = '11111111';

            }else{

               $feAdqBien = $datosinmuebles->feAdqBien;
               $feAdqBien = date("dmY", strtotime($feAdqBien));
            }

             /*====Fecha de Ingreso del Bien===*/

            if($datosinmuebles->feIngBien == '1111-11-11')
            {
              $feIngBien = '11111111';

            }else{

              $feIngBien = $datosinmuebles->feIngBien;
              $feIngBien = date("dmY", strtotime($feIngBien));
            }

             /*====Especifique Otro Estado===*/

            if($datosinmuebles->espOtroEdo == '1')
            {
              $espOtroEdo = 'noaplica';

            }else{

              $espOtroEdo=$datosinmuebles->espOtroEdo;
            }

            /*====Descripción del Edo del Bien===*/

            if($datosinmuebles->descEdoBien == '1')
            {
              $descEdoBien = 'xxx';

            }else{

              $descEdoBien=$datosinmuebles->descEdoBien;
            }


            /*====Otro Uso====*/
           
            if($datosinmuebles->otroUsoInmu == '1')
            {
              $otroUsoInmu = 'noaplica';

            }else{

              $otroUsoInmu=$datosinmuebles->otroUsoInmu;
            }

            /*====Oficina de Registro/Notaría====*/
           
            if($datosinmuebles->ofiRegistro == '1')
            {
              $ofiRegistro = 'xxx';

            }else{

              $ofiRegistro=$datosinmuebles->ofiRegistro;
            }

            /*====Referencia del Registro====*/
           
            if($datosinmuebles->refRegistro == '1')
            {
              $refRegistro = 'xxx';

            }else{

              $refRegistro=$datosinmuebles->refRegistro;
            }

            /*====Tomo====*/
           
            if($datosinmuebles->tomo == '0')
            {
              $tomo = '99';

            }else{

              $tomo=$datosinmuebles->tomo;
            }

            /*====Folio====*/
           
            if($datosinmuebles->folio == '0')
            {
              $folio = '99';

            }else{

              $folio=$datosinmuebles->folio;
            }

            /*====Protocolo====*/
           
            if($datosinmuebles->protocolo == '1')
            {
              $protocolo = 'xxx';

            }else{

              $protocolo=$datosinmuebles->protocolo;
            }

            /*====Número de Registro====*/
           
            if($datosinmuebles->numRegistro == '0')
            {
              $numRegistro = 'xxx';

            }else{

              $numRegistro=$datosinmuebles->numRegistro;
            }

            /*====Fecha de Registro===*/

            if($datosinmuebles->feRegistro == '1111-11-11')
            {
              $feRegistro = '11111111';

            }else{

             $feRegistro = $datosinmuebles->feRegistro;
             $feRegistro = date("dmY", strtotime($feRegistro));
            }

            /*====Propietario Anterior===*/

            if($datosinmuebles->propieAnt == '1')
            {
              $propieAnt = 'xxx';

            }else{

              $propieAnt=$datosinmuebles->propieAnt;
            }

            /*====Dependencias que lo Integran===*/

            if($datosinmuebles->depenIntegra == '1')
            {
              $depenIntegra = 'xxx';

            }else{

              $depenIntegra=$datosinmuebles->depenIntegra;
            }

            /*====Área de Construcción===*/

            if($datosinmuebles->areaConstru == '0')
            {
              $areaConstru = '99.99';

            }else{

              $areaConstru=$datosinmuebles->areaConstru;
            }

            /*====Especifique la Otra Unidad de Medida (Construcción)===*/

            if($datosinmuebles->espeOtraUnidad == '1')
            {
              $espeOtraUnidad = 'xxx';

            }else{

              $espeOtraUnidad=$datosinmuebles->espeOtraUnidad;
            }

            /*====Área del Terreno===*/

            if($datosinmuebles->areaTerreno == '0')
            {
              $areaTerreno = '99.99';

            }else{

              $areaTerreno=$datosinmuebles->areaTerreno;
            }

            /*====Especifique la Otra Unidad de Medida (Terreno)===*/

            if($datosinmuebles->espeOtraTerre == '1')
            {
              $espeOtraTerre = 'xxx';

            }else{

              $espeOtraTerre=$datosinmuebles->espeOtraTerre;
            }

            /*====Otras especificaciones===*/

            if($datosinmuebles->otrasEspecifi == '1')
            {
              $otrasEspecifi = 'xxx';

            }else{

              $otrasEspecifi=$datosinmuebles->otrasEspecifi;
            }

            /*===Código del Registro de Seguro===*/

            if($datosinmuebles->codRegSeguro == '1')
            {
              $codRegSeguro = 'xxx';

            }else{

              $codRegSeguro=$datosinmuebles->codRegSeguro;
            }

          $sheet->row($index+2, [
              $datosinmuebles->codBien, 
              $datosinmuebles->selectCatalogoinm->codigo, 
              $datosinmuebles->selectUnidadinmu->codUnidad,
              $codRespAdm, 
              $datosinmuebles->selectCorresinmu->opcion, 
              $codSede, 
              $datosinmuebles->selectLocalinmu->opcion, 
              $datosinmuebles->selectPaisesinmu->codPais, 
              $espeOtroPais, 
              $datosinmuebles->selectParroinmu->codParroquia, 
              $datosinmuebles->selectCiudadinmu->codCiudad, 
              $espeOtroCiudad,
              $urbanizacion,
              $calleAvenida,
              $casaEdificio,
              $codInterno,
              $datosinmuebles->selectEstatuinmu->opcion, 
              $espOtroUso,
              $valorAdq,
              $datosinmuebles->selectMonedainmu->opcion,
              $espeMoneda,
              $feAdqBien,
              $feIngBien,
              $datosinmuebles->selectCondicioninmu->opcion,
              $espOtroEdo,
              $descEdoBien,
              $datosinmuebles->selectUsoninmu->opcion,
              $otroUsoInmu,
              $ofiRegistro,
              $refRegistro,
              $tomo,
              $folio,
              $protocolo,
              $numRegistro,
              $feRegistro,
              $propieAnt,
              $depenIntegra,
              $areaConstru,
              $datosinmuebles->selectContrucinmu->opcion,
              $espeOtraUnidad,
              $areaTerreno,
              $datosinmuebles->selectTerrenoinmu->opcion,
              $espeOtraTerre,
              $otrasEspecifi,
              $datosinmuebles->selectSeguroinmu->opcion,
              $codRegSeguro,

          ]);

        }

      });
   
      })->export('xlsx');
    }
}
