<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloEqtransporte;
use App\sel_catalogo;
use App\modeloS4;
use App\modeloS5;
use App\modeloResponsables;
use App\sel_estatusbien;
use App\sel_seguros2;
use App\sel_condicionbien;
use App\modeloMarcas;
use App\modeloModelos;
use App\sel_colorbien;
use App\sel_garantiabien;
use App\sel_seguros3;
use App\sel_clasebien;
use App\modeloBitacora;
use Carbon\Carbon;

class controladorEqtransporte extends Controller
{
    public function index()
    {
    	$lastCod = modeloEqtransporte::select('codBien')->get()->last();
      $catalogo = sel_catalogo::all();
    	$unidad = modeloS5::all();
      $sede = modeloS4::all();
      $responsable = modeloResponsables::select('codResp')->get();
    	$estatusBien = sel_estatusbien::all();
    	$moneda = sel_seguros2::all();
    	$condicion = sel_condicionbien::all();
    	$marcas = modeloMarcas::all();
      $modelos = modeloModelos::all();
      $colorBien = sel_colorbien::all();
      $unidadGarantia = sel_garantiabien::all();
      $poseeComponente = sel_seguros3::all();
      $seguroBien = sel_seguros3::all();
      $tieneSistem = sel_seguros3::all();
      $claseBien = sel_clasebien::all();


        return view('AnexosT.visEqtransporte', compact('lastCod','catalogo','sede','responsable','unidad','estatusBien','moneda','condicion','marcas','modelos','colorBien','unidadGarantia','poseeComponente','seguroBien','tieneSistem','claseBien'));
    }
    
    public function depenResponsable(Request $request, $id)
    {
    
      if($request->ajax()){
         $dependencia = modeloEqtransporte::selectRespontran($id);
          return response()->json($dependencia);
      }
    }

    public function store(Request $request)
    {
         $form_t9=new modeloEqtransporte();
         $form_t9->codBien = $request->codBien;
         $form_t9->codCata = $request->codCata;
         $form_t9->codUnidad = $request->codUnidad;
         $form_t9->sedeOrgano = $request->sedeOrgano;
         $form_t9->estatuBien = $request->estatuBien;
         $form_t9->moneda = $request->moneda;
         $form_t9->edoBien = $request->edoBien;
         $form_t9->claseBien = $request->claseBien;
         $form_t9->codMarca = $request->codMarca;
         $form_t9->codModel = $request->codModel;
         $form_t9->codColorBien = $request->codColorBien;
         $form_t9->unidadMedi = $request->unidadMedi;
         $form_t9->tieneSistema = $request->tieneSistema;
         $form_t9->poseeCompo = $request->poseeCompo;
         $form_t9->espeSistema = $request->espeSistema;
         $form_t9->seguroBien = $request->seguroBien;

         if($form_t9->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Por favor agregue el código del responsable administrativo y del uso directo del bien, para modificar el registro.');
         }else{
            $form_t9->codRespAdm = $request->codRespAdm;
         }

         if($form_t9->codResBien = $request->codResBien == '')
         {
          return back()->with('errormsj', 'Por favor agregue el código del responsable administrativo y del uso directo del bien, para modificar el registro.');

         }else{
          $form_t9->codResBien = $request->codResBien;
         }

         if($form_t9->codRespAdm = $request->codRespAdm == '')
         {
          $form_t9->codRespAdm = '1';

         }else{
          $form_t9->codRespAdm = $request->codRespAdm;
         }

         if($form_t9->codResBien = $request->codResBien == '')
         {
          $form_t9->codResBien = '1';

         }else{
          $form_t9->codResBien = $request->codResBien;
         }

         if($form_t9->codInterno = $request->codInterno == '')
         {
          $form_t9->codInterno = '1';

         }else{
          $form_t9->codInterno = $request->codInterno;
         }

         if($form_t9->espOtroUso = $request->espOtroUso == '')
         {
          $form_t9->espOtroUso = '1';

         }else{
          $form_t9->espOtroUso = $request->espOtroUso;
         }

         if($form_t9->valorAdq = $request->valorAdq == '')
         {
          $form_t9->valorAdq = '0';

         }else{
          $form_t9->valorAdq = $request->valorAdq;
         }

         if($form_t9->espeMoneda = $request->espeMoneda == '')
         {
          $form_t9->espeMoneda = '1';

         }else{
          $form_t9->espeMoneda = $request->espeMoneda;
         }

         if($form_t9->anoFabriBien = $request->anoFabriBien == ''){
         
          $form_t9->anoFabriBien = '0';

         }else{
          $form_t9->anoFabriBien = $request->anoFabriBien;
         }

        if($form_t9->feAdqBien = $request->feAdqBien == ''){
          $form_t9->feAdqBien = '11111111';
          }else{
          $form_t9->feAdqBien = $request->feAdqBien;  
         }

        if($form_t9->feIngBien = $request->feIngBien == ''){
          $form_t9->feIngBien = '11111111';
          }else{

          $form_t9->feIngBien = $request->feIngBien;
         }

        if($form_t9->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t9->espOtroEdo = '1';

         }else{
          $form_t9->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t9->descEdoBien = $request->descEdoBien == '')
         {
          $form_t9->descEdoBien = '1';

         }else{
          $form_t9->descEdoBien = $request->descEdoBien;
        }

        if($form_t9->espeClase = $request->espeClase == '')
         {
          $form_t9->espeClase = '1';

         }else{
          $form_t9->espeClase = $request->espeClase;
        }

        if($form_t9->serialCarro = $request->serialCarro == '')
         {
          $form_t9->serialCarro = '1';

         }else{
          $form_t9->serialCarro = $request->serialCarro;
        }

        if($form_t9->serialMotor = $request->serialMotor == '')
         {
          $form_t9->serialMotor = '1';

         }else{
          $form_t9->serialMotor = $request->serialMotor;
        }

        if($form_t9->placaBien = $request->placaBien == '')
         {
          $form_t9->placaBien = '1';

         }else{
          $form_t9->placaBien = $request->placaBien;
        }

        if($form_t9->numTituPro = $request->numTituPro == '')
         {
          $form_t9->numTituPro = '1';

         }else{
          $form_t9->numTituPro = $request->numTituPro;
        }

        if($form_t9->espeColor = $request->espeColor == '')
         {
          $form_t9->espeColor = '1';

         }else{
          $form_t9->espeColor = $request->espeColor;
        }

        if($form_t9->espeColor = $request->espeColor == '')
         {
          $form_t9->espeColor = '1';

         }else{
          $form_t9->espeColor = $request->espeColor;
        }

        if($form_t9->otraEspeColor = $request->otraEspeColor == '')
         {
          $form_t9->otraEspeColor = '1';

         }else{
          $form_t9->otraEspeColor = $request->otraEspeColor;
        }

        if($form_t9->capacidadBien = $request->capacidadBien == '')
         {
          $form_t9->capacidadBien = '1';

         }else{
          $form_t9->capacidadBien = $request->capacidadBien;
        }

        if($form_t9->nomDadoBien = $request->nomDadoBien == '')
         {
          $form_t9->nomDadoBien = '1';

         }else{
          $form_t9->nomDadoBien = $request->nomDadoBien;
        }

        if($form_t9->usoBien = $request->usoBien == '')
         {
          $form_t9->usoBien = '1';

         }else{
          $form_t9->usoBien = $request->usoBien;
        }

        if($form_t9->espeTecBien = $request->espeTecBien == '')
         {
          $form_t9->espeTecBien = '1';

         }else{
          $form_t9->espeTecBien = $request->espeTecBien;
        }

        if($form_t9->otraEspeBien = $request->otraEspeBien == '')
         {
          $form_t9->otraEspeBien = '1';

         }else{
          $form_t9->otraEspeBien = $request->otraEspeBien;
        }

        if($form_t9->garantia = $request->garantia == '')
         {
          $form_t9->garantia = '1';

         }else{
          $form_t9->garantia = $request->garantia;
        }

        if($form_t9->feIniGarantia = $request->feIniGarantia == ''){
          $form_t9->feIniGarantia = '11111111';
          }else{
          $form_t9->feIniGarantia = $request->feIniGarantia;  
        }

        if($form_t9->feFinGarantia = $request->feFinGarantia == ''){
          $form_t9->feFinGarantia = '11111111';
          }else{
          $form_t9->feFinGarantia = $request->feFinGarantia;  
        }

        if($form_t9->codRegSeguro = $request->codRegSeguro == '')
         {
          $form_t9->codRegSeguro = '1';

         }else{
          $form_t9->codRegSeguro = $request->codRegSeguro;
        }

        if($form_t9->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Equipo de Transporte';
          $bit->save();

        }

        return back()->with('msj', 'Datos Registrados Exitosamente');


    }

    public function edit($id)
    {
        $form_t9 = modeloEqtransporte::find($id);
        $lastCod = modeloEqtransporte::select('codBien')->get()->last();
        $catalogo = sel_catalogo::all();
        $sede = modeloS4::all();
        $unidad = modeloS5::all();
        $responsable = modeloResponsables::select('codResp')->get();
        $estatusBien = sel_estatusbien::all();
        $moneda = sel_seguros2::all();
        $condicion = sel_condicionbien::all();
        $marcas = modeloMarcas::all();
        $modelos = modeloModelos::all();
        $colorBien = sel_colorbien::all();
        $unidadGarantia = sel_garantiabien::all();
        $poseeComponente = sel_seguros3::all();
        $seguroBien = sel_seguros3::all();
        $tieneSistema = sel_seguros3::all();
        $claseBien = sel_clasebien::all();


       return view('layouts.ModificarAnexosT.modificarEqtransporte',compact('form_t9','catalogo','sede','responsable','lastCod','unidad','estatusBien','estatusBien','moneda','condicion','marcas','modelos','colorBien','unidadGarantia','poseeComponente','seguroBien','tieneSistema','claseBien'));
    }

     public function update(Request $request, $id)
     {   
         $form_t9= modeloEqtransporte::find($id);
         $form_t9->codBien = $request->codBien;
         $form_t9->codCata = $request->codCata;
         $form_t9->codUnidad = $request->codUnidad;
         $form_t9->sedeOrgano = $request->sedeOrgano;
         $form_t9->estatuBien = $request->estatuBien;
         $form_t9->moneda = $request->moneda;
         $form_t9->edoBien = $request->edoBien;
         $form_t9->claseBien = $request->claseBien;
         $form_t9->codMarca = $request->codMarca;
         $form_t9->codModel = $request->codModel;
         $form_t9->codColorBien = $request->codColorBien;
         $form_t9->unidadMedi = $request->unidadMedi;
         $form_t9->tieneSistema = $request->tieneSistema;
         $form_t9->poseeCompo = $request->poseeCompo;
         $form_t9->seguroBien = $request->seguroBien;


         if($form_t9->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Debe agregar el código del responsable administrativo y del uso directo del bien, para modificar el registro.');
         }else{
            $form_t9->codRespAdm = $request->codRespAdm;
         }

         if($form_t9->codResBien = $request->codResBien == '')
         {
          return back()->with('errormsj', 'Debe seleccionar el código del responsable administrativo y del uso directo del bien, para modificar el registro.');

         }else{
          $form_t9->codResBien = $request->codResBien;
         }

         if($form_t9->codInterno = $request->codInterno == '')
         {
          $form_t9->codInterno = '1';

         }else{
          $form_t9->codInterno = $request->codInterno;
         }

         if($form_t9->espOtroUso = $request->espOtroUso == '')
         {
          $form_t9->espOtroUso = '1';

         }else{
          $form_t9->espOtroUso = $request->espOtroUso;
         }

         if($form_t9->valorAdq = $request->valorAdq == '')
         {
          $form_t9->valorAdq = '0';

         }else{
          $form_t9->valorAdq = $request->valorAdq;
         }

         if($form_t9->espeMoneda = $request->espeMoneda == '')
         {
          $form_t9->espeMoneda = '1';

         }else{
          $form_t9->espeMoneda = $request->espeMoneda;
         }

         if($form_t9->feAdqBien = $request->feAdqBien == ''){
          $form_t9->feAdqBien = '11111111';

          }else{
          
          $form_t9->feAdqBien = $request->feAdqBien;

         }

         if($form_t9->anoFabriBien = $request->anoFabriBien == ''){
         
          $form_t9->anoFabriBien = '0';

         }else{
          $form_t9->anoFabriBien = $request->anoFabriBien;
         }

        if($form_t9->feIngBien = $request->feIngBien == ''){
          $form_t9->feIngBien = '11111111';
          }else{
          $form_t9->feIngBien = $request->feIngBien;  
         }

        if($form_t9->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t9->espOtroEdo = '1';

         }else{
          $form_t9->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t9->descEdoBien = $request->descEdoBien == '')
         {
          $form_t9->descEdoBien = '1';

         }else{
          $form_t9->descEdoBien = $request->descEdoBien;
        }

        if($form_t9->espeClase = $request->espeClase == '')
         {
          $form_t9->espeClase = '1';

         }else{
          $form_t9->espeClase = $request->espeClase;
        }

        if($form_t9->serialCarro = $request->serialCarro == '')
         {
          $form_t9->serialCarro = '1';

         }else{
          $form_t9->serialCarro = $request->serialCarro;
        }

        if($form_t9->serialMotor = $request->serialMotor == '')
         {
          $form_t9->serialMotor = '1';

         }else{
          $form_t9->serialMotor = $request->serialMotor;
        }

        if($form_t9->serialMotor = $request->serialMotor == '')
         {
          $form_t9->serialMotor = '1';

         }else{
          $form_t9->serialMotor = $request->serialMotor;
        }

        if($form_t9->placaBien = $request->placaBien == '')
         {
          $form_t9->placaBien = '1';

         }else{
          $form_t9->placaBien = $request->placaBien;
        }

        if($form_t9->numTituPro = $request->numTituPro == '')
         {
          $form_t9->numTituPro = '1';

         }else{
          $form_t9->numTituPro = $request->numTituPro;
        }

        if($form_t9->espeColor = $request->espeColor == '')
         {
          $form_t9->espeColor = '1';

         }else{
          $form_t9->espeColor = $request->espeColor;
        }

        if($form_t9->espeColor = $request->espeColor == '')
         {
          $form_t9->espeColor = '1';

         }else{
          $form_t9->espeColor = $request->espeColor;
        }

        if($form_t9->otraEspeColor = $request->otraEspeColor == '')
         {
          $form_t9->otraEspeColor = '1';

         }else{
          $form_t9->otraEspeColor = $request->otraEspeColor;
        }

        if($form_t9->capacidadBien = $request->capacidadBien == '')
         {
          $form_t9->capacidadBien = '1';

         }else{
          $form_t9->capacidadBien = $request->capacidadBien;
        }

        if($form_t9->nomDadoBien = $request->nomDadoBien == '')
         {
          $form_t9->nomDadoBien = '1';

         }else{
          $form_t9->nomDadoBien = $request->nomDadoBien;
        }

        if($form_t9->usoBien = $request->usoBien == '')
         {
          $form_t9->usoBien = '1';

         }else{
          $form_t9->usoBien = $request->usoBien;
        }

        if($form_t9->espeTecBien = $request->espeTecBien == '')
         {
          $form_t9->espeTecBien = '1';

         }else{
          $form_t9->espeTecBien = $request->espeTecBien;
        }

        if($form_t9->otraEspeBien = $request->otraEspeBien == '')
         {
          $form_t9->otraEspeBien = '1';

         }else{
          $form_t9->otraEspeBien = $request->otraEspeBien;
        }

        if($form_t9->garantia = $request->garantia == '')
         {
          $form_t9->garantia = '1';

         }else{
          $form_t9->garantia = $request->garantia;
        }

        if($form_t9->feIniGarantia = $request->feIniGarantia == ''){
          $form_t9->feIniGarantia = '11111111';
          }else{
          $form_t9->feIniGarantia = $request->feIniGarantia;  
        }

        if($form_t9->feFinGarantia = $request->feFinGarantia == ''){
          $form_t9->feFinGarantia = '11111111';
          }else{
          $form_t9->feFinGarantia = $request->feFinGarantia;  
        }

        if($form_t9->espeSistema = $request->espeSistema == '')
         {
          $form_t9->espeSistema = '0';

         }else{
          $form_t9->espeSistema = $request->espeSistema;
        }

        if($form_t9->codRegSeguro = $request->codRegSeguro == '')
         {
          $form_t9->codRegSeguro = '1';

         }else{
          $form_t9->codRegSeguro = $request->codRegSeguro;
        }

        if($form_t9->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Equipo de Transporte';
          $bit->save();

            return back()->with('msj', 'Datos Registrados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
     }

     public function exportTranspo()
     {

      \Excel::create('transporte', function($excel) {
    
          $transporte = modeloEqtransporte::all();
          $unidad = modeloS5::all();
          $estatusBien = sel_estatusbien::all();
          $moneda = sel_seguros2::all();
          $condicion = sel_condicionbien::all();
          $marcas = modeloMarcas::all();
          $modelos = modeloModelos::all();
          $colorBien = sel_colorbien::all();
          $unidadGarantia = sel_garantiabien::all();
          $poseeComponente = sel_seguros3::all();
          $tieneSistem = sel_seguros3::all();
      
      $excel->sheet('transporte', function($sheet) use($transporte) {
 
      /*========CABECERA DE LA FILA N° 1========*/

      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN',
          'CÓDIGO SEGÚN EL CATALOGO',
          'DEPENDENCIA ADMINISTRATIVA',
          'SEDE DEL ÓRGANO O ENTE DONDE SE ENCUENTRA EL BIEN',
          'CÓDIGO DEL RESPONSABLE ADMINISTRATIVO',
          'CÓDIGO DEL RESPONSABLE DEL USO DIRECTO DEL BIEN',
          'CÓDIGO INTERNO DEL BIEN', 
          'ESTATUS DEL USO DEL BIEN',
          'ESPECIFIQUE EL OTRO USO',
          'VALOR DE ADQUISICIÓN DEL BIEN',
          'MONEDA', 
          'ESPECIFIQUE LA OTRA MONEDA',
          'FECHA DE ADQUISICIÓN DEL BIEN',
          'FECHA DE INGRESO DEL BIEN', 
          'ESTADO DEL BIEN', 
          'ESPECIFIQUE EL OTRO ESTADO DEL BIEN', 
          'DESCRIPCIÓN DEL ESTADO DEL BIEN', 
          'CLASE DEL BIEN', 
          'ESPECIFIQUE LA OTRA CLASE', 
          'CÓDIGO DE LA MARCA DEL BIEN', 
          'CÓDIGO DEL MODELO DEL BIEN', 
          'AÑO DE FABRICACIÓN DEL BIEN', 
          'SERIAL DE CARROCERÍA DEL BIEN', 
          'SERIAL DEL MOTOR DEL BIEN', 
          'PLACAS / SIGLAS DEL BIEN', 
          'NÚMERO DEL TÍTULO DE PROPIEDAD', 
          'CÓDIGO DEL COLOR DEL BIEN', 
          'ESPECIFICACIÓN DE OTRO COLOR', 
          'OTRAS ESPECIFICACIONES DEL COLOR', 
          'CAPACIDAD DEL BIEN', 
          'NOMBRE DADO A EL BIEN', 
          'USO DEL BIEN', 
          'ESPECIFICACIONES TÉCNICAS DEL BIEN', 
          'OTRAS ESPECIFICACIONES DE DESCRIPCIÓN DEL BIEN', 
          'GARANTÍA', 
          'UNIDAD DE MEDIDA DE LA GARANTÍA', 
          'FECHA DE INICIO DE LA GARANTÍA', 
          'FECHA FINAL DE LA GARANTÍA',
          'TIENE SISTEMA DE RASTREO Y LOCALIZACIÓN INSTALADO',
          'ESPECIFICACIONES DE SISTEMA DE RASTREO INSTALADO', 
          'POSEE COMPONENTES', 
          'SE ENCUENTRA ASEGURADO EL BIEN', 
          'CÓDIGO DEL REGISTRO DE SEGURO',

      ]);

      $sheet->setWidth([
                    'A'     =>  40, /*==Código del Origen==*/
                    'B'     =>  40, /*==Código según el catalogo==*/
                    'C'     =>  40, /*==Dependencia Administrativa==*/
                    'D'     =>  70, /*==Sede del Órgano o Ente Donde se Encuentra el Bien==*/
                    'E'     =>  60, /*==Código del Responsable Administrativo==*/
                    'F'     =>  70, /*==Código del Responsable del uso Directo del Bien==*/
                    'G'     =>  40, /*==Código Interno del Bien==*/
                    'H'     =>  40, /*==Estatus del Uso del Bien==*/
                    'I'     =>  40, /*==Especifique el Otro Uso==*/
                    'J'     =>  40, /*==Valor de Adquisición del Bien==*/
                    'K'     =>  40, /*==Moneda==*/
                    'L'     =>  40, /*==Especifique la Otra Moneda==*/
                    'M'     =>  40, /*==Fecha de Adquisición del Bien==*/
                    'N'     =>  40, /*==Fecha de Ingreso del Bien==*/
                    'O'     =>  40, /*==Estado del Bien==*/
                    'P'     =>  70, /*==Especifique el Otro Estado del Bien==*/
                    'Q'     =>  70, /*==Descripción del Estado del Bien==*/
                    'R'     =>  40, /*==Clase del Bien==*/
                    'S'     =>  60, /*==Especifique la Otra Clase==*/
                    'T'     =>  60, /*==Código de la Marca del Bien==*/
                    'U'     =>  60, /*==Código Modelo del Bien==*/
                    'V'     =>  60, /*==Año de Fabricación del Bien==*/
                    'W'     =>  60, /*==Serial de Carrocería del Bien==*/
                    'X'     =>  40, /*==Serial del Motor del Bien==*/
                    'Y'     =>  40, /*==Placas / Siglas del Bien==*/
                    'Z'     =>  60, /*==Número del Título de Propiedad==*/
                    'AA'    =>  40, /*==Código del Color del Bien==*/
                    'AB'    =>  60, /*==Especificación de Otro Color==*/
                    'AC'    =>  60, /*==Otras Especificación de Color==*/
                    'AD'    =>  40, /*==Capacidad del Bien==*/
                    'AE'    =>  40, /*==Nombre Dado al Bien==*/
                    'AF'    =>  40, /*==Uso del Bien==*/
                    'AG'    =>  70, /*==Especificaciones Técnicas del Bien==*/
                    'AH'    =>  70, /*==Otras Especificaciones de Descripción del Bien==*/
                    'AI'    =>  40, /*==Garantía==*/
                    'AJ'    =>  70, /*==Unidad de Medida de la Garantía==*/
                    'AK'    =>  50, /*==Fecha Inicio de la Garantía==*/
                    'AL'    =>  50, /*==Fecha Fin de la Garantía==*/
                    'AM'    =>  70, /*==Tiene Sistema de Rastreo y Localización Instalado==*/
                    'AN'    =>  70, /*==Especificaciones Sistema de Rastreo Instalado==*/
                    'AO'    =>  40, /*==Posee Componentes==*/
                    'AP'    =>  70, /*==Se Encuentra Asegurado el Bien==*/
                    'AQ'    =>  70, /*==Código del Registro de Seguro==*/

                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:AQ3000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($transporte  as $index => $transporte) {
            
        
            /*========Sede del Órgano o Ente Donde se Encuentra el Bien=======*/

            if($transporte->sedeOrgano == '1')
            {
              $sedeOrgano = 'xxx';

            }else{

              $sedeOrgano=$transporte->sedeOrgano;
            }

            /*========Código del Responsable Administrativo=======*/

            if($transporte->codRespAdm == '1')
            {
              $codRespAdm = 'xxx';

            }else{

              $codRespAdm=$transporte->codRespAdm;
            }

            /*====Código del Responsable del uso directo del Bien====*/
           
            if($transporte->codResBien == '1')
            {
              $codResBien = 'xxx';

            }else{

              $codResBien=$transporte->codResBien;
            }

            /*====Código del Responsable del uso directo del Bien====*/
           
            if($transporte->codInterno == '1')
            {
              $codInterno = 'xxx';

            }else{

              $codInterno=$transporte->codInterno;
            }

            /*====Especifique el otro uso====*/
           
            if($transporte->espOtroUso == '1')
            {
              $espOtroUso = 'noaplica';

            }else{

              $espOtroUso=$transporte->espOtroUso;
            }

            /*====Valor de Adquisición====*/
           
            if($transporte->valorAdq == '0')
            {
              $valorAdq = '99.99';

            }else{

              $valorAdq=$transporte->valorAdq;
            }

            /*====Especifique la Otra Moneda====*/
           
            if($transporte->espeMoneda == '1')
            {
              $espeMoneda = 'noaplica';

            }else{

              $espeMoneda=$transporte->espeMoneda;
            }

          
            /*====Serial de Carrocería del Bien====*/
           
            if($transporte->serialCarro == '1')
            {
              $serialCarro = 'xxx';

            }else{

              $serialCarro=$transporte->serialCarro;
            }

            /*===Fecha de Adquisición del Bien===*/

            if($transporte->feAdqBien == '1111-11-11')
            {
              $feAdqBien = '11111111';

            }else{

              $feAdqBien = $transporte->feAdqBien;
              $feAdqBien = date("dmY", strtotime($feAdqBien));
            }

            /*===Fecha de Ingreso del Bien===*/

            if($transporte->feIngBien == '1111-11-11')
            {
              $feIngBien = '11111111';

            }else{

              $feIngBien = $transporte->feIngBien;
              $feIngBien = date("dmY", strtotime($feIngBien));
            
            }

            /*====Especifique el Otro Estado del Bien====*/
           
            if($transporte->espOtroEdo == '1')
            {
              $espOtroEdo = 'noaplica';

            }else{

              $espOtroEdo=$transporte->espOtroEdo;
            }

            /*===Descripción del Estado del Bien===*/

            if($transporte->descEdoBien == '1')
            {
              $descEdoBien = 'xxx';

            }else{

              $descEdoBien=$transporte->descEdoBien;
            }

            /*===Especifique la Otra clase===*/

            if($transporte->espeClase == '1')
            {
              $espeClase = 'noaplica';

            }else{

              $espeClase=$transporte->espeClase;
            }

            /*===Año de Fabricación del Bien===*/

            if($transporte->anoFabriBien == '0')
            {
              $anoFabriBien = 'xxx';

            }else{

              $anoFabriBien=$transporte->anoFabriBien;
            }

            /*===Serial de Carrocería del Bien===*/

            if($transporte->serialCarro == '1')
            {
              $serialCarro = 'xxx';

            }else{

              $serialCarro=$transporte->serialCarro;
            }

            /*===Serial del Motor del Bien===*/

            if($transporte->serialMotor == '1')
            {
              $serialMotor = 'xxx';

            }else{

              $serialMotor=$transporte->serialMotor;
            }

            /*===Placas / Siglas del Bien===*/

            if($transporte->placaBien == '1')
            {
              $placaBien = 'xxx';

            }else{

              $placaBien=$transporte->placaBien;
            }

            /*===Número del Título de Propiedad===*/

            if($transporte->numTituPro == '1')
            {
              $numTituPro = 'xxx';

            }else{

              $numTituPro=$transporte->numTituPro;
            }


            /*===Especificación de Color===*/

            if($transporte->espeColor == '1')
            {
              $espeColor = 'noaplica';

            }else{

              $espeColor=$transporte->espeColor;
            }

            /*===Otras Especificaciones del Color===*/

            if($transporte->otraEspeColor == '1')
            {
              $otraEspeColor = 'xxx';

            }else{

              $otraEspeColor=$transporte->otraEspeColor;
            }

            /*===Capacidad del Bien===*/

            if($transporte->capacidadBien == '1')
            {
              $capacidadBien = 'xxx';

            }else{

              $capacidadBien=$transporte->capacidadBien;
            }

            /*===Nombre Dado al Bien===*/

            if($transporte->nomDadoBien == '1')
            {
              $nomDadoBien = 'xxx';

            }else{

              $nomDadoBien=$transporte->nomDadoBien;
            }

            /*===Uso del Bien===*/

            if($transporte->usoBien == '1')
            {
              $usoBien = 'xxx';

            }else{

              $usoBien=$transporte->usoBien;
            }

            /*===Especificaciones Técnicas del Bien===*/

            if($transporte->espeTecBien == '1')
            {
              $espeTecBien = 'xxx';

            }else{

              $espeTecBien=$transporte->espeTecBien;
            }

            /*===Otras Especificaciones de Descripción del Bien===*/

            if($transporte->otraEspeBien == '1')
            {
              $otraEspeBien = 'xxx';

            }else{

              $otraEspeBien=$transporte->otraEspeBien;
            }

            /*===Garantía===*/

            if($transporte->garantia == '1')
            {
              $garantia = 'xxx';

            }else{

              $garantia=$transporte->garantia;
            }

            /*===Unidad de Medida===*/

            if($transporte->unidadMedi == '1'){
            
              $unidadMedi = '1';

            }else if($transporte->unidadMedi == '2'){

              $unidadMedi = '2';

            }else if($transporte->unidadMedi == '3'){

              $unidadMedi = '3';

            }else{

              $unidadMedi = '99';
            }

            /*===Fecha de Ingreso del Bien===*/

            if($transporte->feIniGarantia == '1111-11-11')
            {
              $feIniGarantia = '11111111';

            }else{

              $feIniGarantia = $transporte->feIniGarantia;
              $feIniGarantia = date("dmY", strtotime($feIniGarantia));
     
            }

            /*===Fecha de Ingreso del Bien===*/

            if($transporte->feFinGarantia == '1111-11-11')
            {
              $feFinGarantia = '11111111';

            }else{

              $feFinGarantia = $transporte->feFinGarantia;
              $feFinGarantia = date("dmY", strtotime($feFinGarantia));
            }
            
            /*===Especificaciones Sistema de Rastreo Instalado===*/

            if($transporte->espeSistema == '1')
            {
              $espeSistema = 'xxx';

            }else{

              $espeSistema=$transporte->espeSistema;
            }

            /*===Código del Registro de Seguro===*/

            if($transporte->codRegSeguro == '1')
            {
              $codRegSeguro = 'xxx';

            }else{

              $codRegSeguro=$transporte->codRegSeguro;
            }


          $sheet->row($index+2, [
              $transporte->codBien, 
              $transporte->selectCatalogotran->codigo, 
              $transporte->selectUnidadtran->codUnidad,
              $sedeOrgano, 
              $codRespAdm, 
              $codResBien, 
              $codInterno, 
              $transporte->estatuBien, 
              $espOtroUso,
              $valorAdq,
              $transporte->moneda,
              $espeMoneda,
              $feAdqBien,
              $feIngBien,
              $transporte->edoBien,
              $espOtroEdo,
              $descEdoBien,
              $transporte->claseBien,
              $espeClase,
              $transporte->selectMarcatrans->codMarca,
              $transporte->selectModeltrans->codModel,
              $anoFabriBien,
              $serialCarro,
              $serialMotor,
              $placaBien,
              $numTituPro,
              $transporte->codColorBien,
              $espeColor,
              $otraEspeColor,
              $capacidadBien,
              $nomDadoBien,
              $usoBien,
              $espeTecBien,
              $otraEspeBien,
              $garantia,
              $unidadMedi,
              $feIniGarantia,
              $feFinGarantia,
              $transporte->selectComponentetr->opcion,
              $espeSistema,
              $transporte->selectAseguradotr->opcion,
              $transporte->selectSistematr->opcion,
              $codRegSeguro,

          ]);

        }

      });
   
      })->export('xlsx');
    }

}
