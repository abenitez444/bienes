<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS6;
use App\modeloS5;
use App\modeloS4;
use App\modeloBitacora;


class controladorS6 extends Controller
{	
	
    public function index()
    {

    	$cod_sede =  modeloS4::all();
    	$cod_unidad = modeloS5::all();

    	$array = array(
    		array("codSede","form-control buscador","Código de la Sede o Lugar de Ubicación"),
    		);

    	$array2 = array(
    		array("codUnidad","form-control buscador","Código de la Unidad Administrativa"),
    		);

    	return view('AnexosS.visUbicacion', compact('cod_sede','cod_unidad','array','array2'));
    }

    public function store(Request $request)
    {
    	$form_s6 = new modeloS6();
    	$form_s6->codSede = $request->codSede;
    	$form_s6->codUnidad = $request->codUnidad;
  		
    	if($form_s6->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos de Ubicación de las Unidades Administrativas';
          $bit->save();
         

        return back()->with('msj', 'Datos Registrados Exitosamente');
        }else {
        return back()->with('errormsj', 'Los datos no se guardaron');
        }

    }

     public function edit($id)

    {

        $form_s6 = modeloS6::find($id);
        $selectS6 = modeloS6::all();
        $selectS5 = modeloS5::all();
        $selectS4 = modeloS4::all();

        return view('layouts.ModificarAnexosS.modificarUbicacion', compact('form_s6','selectS6','selectS5','selectS4'));
    }


    public function update(Request $request, $id)
    {
        $form_s6 = modeloS6::find($id);
        $form_s6->codSede = $request->codSede;
        $form_s6->codUnidad = $request->codUnidad;
    

        if($form_s6->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Datos de Ubicación de las Unidades Administrativas';
          $bit->save();

         

        return back()->with('msj', 'Datos modificados exitosamente');
        }else {
        return back()->with('errormsj', 'Los datos no se modificaron');
        }
    }

    public function exportUbicacion()
    {

    \Excel::create('ubicacion', function($excel) {
        
          $ubicacion = modeloS6::all();
          
          $excel->sheet('ubicacion', function($sheet) use($ubicacion) {
     
          /*========CABECERA DE LA FILA N° 1========*/


          $sheet->row(1, [
              'CÓDIGO DE LA SEDE O LUGAR DE UBICACIÓN', 'CÓDIGO DE LA UNIDAD ADMINISTRATIVA'

          ]);

          $sheet->setWidth([
                        'A'     =>  60,
                        'B'     =>  60,
                    ]);

          $sheet->setHeight(1, 35);
                    $sheet->cell('A1:B4000',function($cell) 
                    {
                        $cell->setAlignment('center');    
                                           
                    });

          foreach($ubicacion  as $index => $ubicacion) 
        {

          $sheet->row($index+2, 
               [
                  $ubicacion->selectSedeS6->codSede, 
                  $ubicacion->selectUnidadS6->codUnidad, 

               ]); 
        }

      });
   
      })->export('xlsx');
    }
}
