<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloResponsables;
use App\sel_responsables;
use App\modeloS5;
use App\modeloBitacora;


class controladorResponsables extends Controller
{
 
    public function index()
    {

    # for/id/name label /placeholder tabla->t4 type="text">
        $infoSelect = sel_responsables::all();
        $unidad = modeloS5::all();
        $lastCod = modeloResponsables::select('codResp')->get()->last();

        $arrayt4=array(
            array("codResp","Código del Responsable:","Introduzca el código del responsable","10","col-md-pull-4","",""),         
            array("cedula","Cédula de Identidad:","Introduzca la cédula de identidad","8",'col-md-push-0',"return soloNum(event)",""),         
            array("nomRes","Nombre del Responsable:","Introduzca el nombre del responsable","100","","",""),         
            array("apeRes","Apellido del Responsable:","Introduzca el apellido del responsable","100","","",""),         
            array("telfRes","Teléfono del Responsable:","+58","10","","return soloNum(event)","telefono"),         
            array("cargoRes","Cargo del Responsable:","Introduzca el cargo del responsable","200","","",""),         
                    
            );
        
        $correo=array(
            array("correRes","Correo Electrónico del Responsable:","Ej: ejemplo123@gmail.com","200","","",""),
        );

        $select = array(
            array("tipoResp","Tipo de Responsable:","2","col-md-push-4"),
            );

        $select2 = array(
            array("codUnidad","Dependencia Administrativa:","2"),
            );

        return view('AnexosT.visResponsables', compact('infoSelect','unidad','arrayt4','correo','select','select2','lastCod'));
    }

    
    public function store(Request $request)
    {
        $form_t4 = new modeloResponsables();
        $form_t4->codResp = $request->codResp;
        $form_t4->tipoResp = $request->tipoResp;
        $form_t4->codUnidad = $request->codUnidad;
     
        $form_t4->cedula = $request->cedula;
        $form_t4->nomRes = $request->nomRes;  
        $form_t4->apeRes = $request->apeRes;  
        $form_t4->telfRes = $request->telfRes;  
        $form_t4->cargoRes = $request->cargoRes;  


        if($form_t4->correRes = $request->correRes == ''){
          $form_t4->correRes = '0';  
        }else{
          $form_t4->correRes = $request->correRes; 
        }



        if($form_t4->save()){
        $bit = new modeloBitacora();
        $bit->user = $_SESSION['id'];
        $bit->accion  = 1;
        $bit->referencia = 'Responsable de los Bienes';
        $bit->save();

            return back()->with('msj', 'Datos Registrados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
        
    }

    public function edit($id)
    {
        $form_t4 = modeloResponsables::find($id);
        $infoSelect = sel_responsables::all();
        $unidad = modeloS5::all();
      

        return view('layouts.ModificarAnexosT.modificarResponsables', compact('form_t4','unidad','infoSelect'));
    }

    
    public function update(Request $request, $id)
    {
        $form_t4 = modeloResponsables::find($id);
        $form_t4->codResp = $request->codResp;
        $form_t4->tipoResp = $request->tipoResp;
        $form_t4->codUnidad = $request->codUnidad;
        $form_t4->cedula = $request->cedula;
        $form_t4->nomRes = $request->nomRes;  
        $form_t4->apeRes = $request->apeRes;  
        $form_t4->telfRes = $request->telfRes;  
        $form_t4->cargoRes = $request->cargoRes;  


        if($form_t4->correRes = $request->correRes == ''){
          $form_t4->correRes = '0';  
        }else{
          $form_t4->correRes = $request->correRes; 
        }

        if($form_t4->save()){
        $bit = new modeloBitacora();
        $bit->user = $_SESSION['id'];
        $bit->accion  = 2;
        $bit->referencia = 'Responsable de los Bienes';
        $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }

    }

    public function exportRespon()
    {
         \Excel::create('responsables', function($excel) {
    
      $responsables = modeloResponsables::all();
      $relacion = sel_responsables::all();
      
      $excel->sheet('responsables', function($sheet) use($responsables) {
 
      /*========CABECERA DE LA FILA N° 1========*/


      $sheet->row(1, [
          'CÓDIGO DEL RESPONSABLE', 'TIPO DE RESPONSABLE', 'CÉDULA DE IDENTIDAD', 'NOMBRE DEL RESPONSABLE', 'APELLIDO DEL RESPONSABLE','TELÉFONO DEL RESPONSABLE','CARGO DEL RESPONSABLE','CORREO ELECTRÓNICO DEL RESPONSABLE','DEPENDENCIA ADMINISTRATIVA'

      ]);

      $sheet->setWidth([
                    'A'     =>  30,
                    'B'     =>  30,
                    'C'     =>  30,
                    'D'     =>  35,
                    'E'     =>  35,
                    'F'     =>  30,
                    'G'     =>  40,
                    'H'     =>  40,
                    'I'     =>  40,
                    
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:I6000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($responsables  as $index => $responsables) {
            
            

            /*========Tipo de Responsable======*/

            if($responsables->tipoResp == '1')
            {
              $tipoResp = 'D';

            }elseif($responsables->tipoResp == '1'){

              $tipoResp = 'U';

            }else{

              $tipoResp = 'C';
            }

            /*========Correo Responsable=======*/

            if($responsables->correRes == '0')
            {
              $correRes = 'xxx';

            }else{

              $correRes=$responsables->correRes;
            }


          $sheet->row($index+2, [
              $responsables->codResp, 
              $tipoResp, 
              $responsables->cedula, 
              $responsables->nomRes, 
              $responsables->apeRes, 
              $responsables->telfRes, 
              $responsables->cargoRes, 
              $correRes,
              $responsables->selectUnidad->codUnidad,
          
          
          ]);

        }

      });
   
      })->export('xlsx');
    }

}
