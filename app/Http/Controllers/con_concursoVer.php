<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloConcurso;
use App\modeloBitacora;

class con_concursoVer extends Controller
{
     public function index(){
     
     $verT2 = modeloConcurso::all();
     return view('RegistrosT.regConcurso', compact('verT2'));
    }

     public function selectId($id){

       $seleccion = modeloConcurso::find($id);
    
         $fechaCon = $seleccion->feConcurso;
         $fechaCon = date("d/m/Y", strtotime($fechaCon));
         $fechaCont = $seleccion->feContrato;
         $fechaCont = date("d/m/Y", strtotime($fechaCont));
         $fechaNot = $seleccion->feNotaEntre;
         $fechaNot = date("d/m/Y", strtotime($fechaNot));
         $fechaFac = $seleccion->feFactura;
         $fechaFac = date("d/m/Y", strtotime($fechaFac));

       return view('MuestraAnexosT.muestraConcurso',compact('seleccion','fechaCon','fechaCont','fechaNot','fechaFac'));
    }

     public function anularConcur($id)
    {
        $seleccion= modeloConcurso::find($id);
        
        if($seleccion->delete()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 3;
          $bit->referencia = 'Compra por Concurso';
          $bit->save();

         return redirect('regConcurso')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
         return redirect()->with('errormsj', 'Los datos no se guardaron');
        }
    }
}
