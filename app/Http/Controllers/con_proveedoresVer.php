<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloProveedores;
use App\modeloDirecta;
use App\modeloConcurso;
use App\modeloBitacora;

class con_proveedoresVer extends Controller
{
    public function index()
    {
        $verT1 = modeloProveedores::all();
        return view('RegistrosT.regProveedores',compact('verT1'));
    }

  
     /*FUNCIÓN selectId es para REGISTROS SIN REVISAR CUANDO CAMBIA A 0 YA EL REGISTRO SE ABRIO*/

    public function selectId($id) {

       $seleccion = modeloProveedores::find($id);

       return view('MuestraAnexosT.muestraProveedores',compact('seleccion'));

    }
    

     public function anularProvee($id)
    {   
        $concurso = modeloConcurso::all()->where('codProvee', $id)->count();
        $directa = modeloDirecta::all()->where('codProvee', $id)->count();
        
        if (empty($directa || $concurso)) {  

        $seleccion= modeloProveedores::find($id);
       
        
        if($seleccion->delete()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 3;
          $bit->referencia = 'Proveedores';
          $bit->save();

           return redirect('regProveedores')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
           return redirect()->with('errormsj', 'Los datos no se guardaron');
         }

        }
        else{
            return redirect('regProveedores')->with('errormsj', 'No puede eliminar éste proveedor, ya se encuentra registrado en otro formulario.');
        }  

    }


}
