<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloResponsables;
use App\modeloBienes;
use App\modeloBitacora;


class con_responsablesVer extends Controller
{
    public function index(){
        
        $verT4= modeloResponsables::all();

        return view('RegistrosT.regResponsables', compact('verT4'));

    }

    public function selectId($id){

       $seleccion = modeloResponsables::find($id);

       return view('MuestraAnexosT.muestraResponsables',compact('seleccion'));
    }

   
    public function anularRespon($id)
    {
    $datos = modeloBienes::all()->where('codUnidad', $id)->count();
      if(empty($datos)){
        $seleccion= modeloResponsables::find($id);
        
       if($seleccion->delete()){
       	$bit = new modeloBitacora();
        $bit->user = $_SESSION['id'];
        $bit->accion  = 3;
        $bit->referencia = 'Responsable de los Bienes';
        $bit->save();

         return redirect('regResponsables')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
         return redirect()->with('errormsj', 'Los datos no se guardaron');
         }
      }
       else{
            return redirect('regResponsables')->with('errormsj', 'No puede eliminar ésta unidad administrativa, ya se encuentra registrada en otro formulario.');
        }
    
    }
}
    