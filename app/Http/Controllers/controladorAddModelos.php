<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloMarcas;
use App\modeloModelos;
use App\sel_catalogo;
use App\modeloBitacora;

class controladorAddModelos extends Controller
{
    public function index()
    {

        $infoSelect = modeloMarcas::all();
        $catalogo = sel_catalogo::all();
        $lastCod = modeloModelos::select('codModel')->get()->last();

        $arrayT6 = array(
            array("codModel","Código del Modelo:","Introduzca el código del modelo","10","col-md-pull-0"),
            array("denModFab","Denominación del Modelo Según el Fabricante:","Indique la denominación según el fabricante","100","col-md-push-0"),
            );

        $selectCod = array(
             array("codMarca","Código de la Marca:","Introduzca el código de la marca","10","col-md-push-0"),
            );

        $arrayBien = array(
            array("codCata","Código del Bien Mueble Según Catalogo:"),
            );

        return view('añadir.añadirModelos', compact('arrayT6','catalogo','selectCod','infoSelect','arrayBien','lastCod'));
    }

    public function store(Request $request)
    {
        $form_t6 =  modeloModelos::where('codModel', $request->codModel)->get();
        
        
        if($form_t6 == '[]'){

        $form_t6 =new modeloModelos();
        $form_t6->codModel = $request->codModel;
        $form_t6->denModFab = $request->denModFab;
        $form_t6->codMarca = $request->codMarca;
        $form_t6->codCata = $request->codCata;

        if($form_t6->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Modelos';
          $bit->save();

        }

        return back()->with('msj', 'Datos Registrados Exitosamente');
            }else{
        return back()->with('errormsj', 'El Código del Modelo "#'.$request->codModel.'" ya existe, por favor siga el orden establecido e intente un modelo nuevo');
        }  
        
    }


    public function edit($id)
    {
        $form_t6 = modeloModelos::find($id);
        $infoSelect = modeloMarcas::all();
        $catalogo = sel_catalogo::all();
 
        return view('layouts.ModificarAnexosT.modificarModelos', compact('form_t6','infoSelect','catalogo'));
             
    }

    public function update(Request $request, $id)
    {

        $form_t6=modeloModelos::find($id);
        $form_t6->denModFab = $request->denModFab;
        $form_t6->codMarca = $request->codMarca;
        $form_t6->codCata = $request->codCata;
        
        if($form_t6->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Modelos';
          $bit->save();

          return back()->with('msj', 'Datos Registrados Exitosamente');
             }else {
          return back()->with('errormsj', 'Los datos no se guardaron');
        }

        
    }

    public function exportModelos()
    {
        \Excel::create('modelos', function($excel) {
    
      $modelos = modeloModelos::all();
      $relacion = modeloMarcas::all();
   
      $excel->sheet('modelos', function($sheet) use($modelos) {
 
      /*========CABECERA DE LA FILA N° 1========*/


      $sheet->row(1, [
          'CÓDIGO DEL MODELO', 'DENOMINACIÓN DEL MODELO SEGÚN EL FABRICANTE ', 'CÓDIGO DE LA MARCA','CÓDIGO DEL BIEN MUEBLE SEGÚN EL CATALOGO AL QUE PUEDE APLICAR EL MODELO'

      ]);

      $sheet->setWidth([
                    'A'     =>  30,
                    'B'     =>  50,
                    'C'     =>  30,
                    'D'     =>  80,
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:D6000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($modelos  as $index => $modelos) {
            
          $sheet->row($index+2, [
              $modelos->codModel, 
              $modelos->denModFab, 
              $modelos->selectMarca->codMarca, 
              $modelos->codSegModel, 
             

          ]);
        }

      });
   
      })->export('xlsx');
    }
    
}
