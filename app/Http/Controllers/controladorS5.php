<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS5;
use App\sel_unidades;
use App\sel_categoria;
use App\modeloBitacora;

class controladorS5 extends Controller
{
    public function index()
    {	
    	$lastCod = modeloS5::select('codUnidad')->get()->last();

    	$infoSelect = sel_categoria::all();

    	$coUnidad = array(
    			array("codUnidad","Código de la Unidad:","10","Introduzca el código de la unidad","Introduza el código de la unidad  administrativa"),
    		);

    	$desUnidad = array(
    			array("descUnidad","Descripción de la Unidad:","255","indique la denominación de la unidad","Describa la denominación de la unidad..."),
    		);

    	$sel_categoria = array(
    			array("categoria","Código de la Categoría de la Unidad:","2"),
    		);

    	$espeCategoria = array(
    			array("espeCatego","Especifique la Denominación de la Categoría:","100","Denominación de Categoría de la unidad"),
    		);

    	$uniAdscrita = array(
    			array("codAdscrita","Código de Unidad a la cual está Adscrita:","10","Indique el código a la cual está adscrita"),
    		);

    	return view('AnexosS.visUnidades', compact('lastCod','infoSelect','coUnidad','desUnidad','sel_categoria','espeCategoria','uniAdscrita'));

    }

    public function store(Request $request)
    {

    $duplicado = modeloS5::where('codUnidad', $request->codUnidad)->get();
       
       if($duplicado == '[]'){

        $form_s5 = new modeloS5();
        $form_s5->codUnidad = $request->codUnidad;
        $form_s5->descUnidad = $request->descUnidad;
        $form_s5->categoria = $request->categoria;

        if($form_s5->espeCatego = $request->espeCatego == '')
        {
            $form_s5->espeCatego = 1;
        }else{
            $form_s5->espeCatego = $request->espeCatego;
        }

        if($form_s5->codAdscrita = $request->codAdscrita == '')
        {
            $form_s5->codAdscrita = 1;
        }else{
            $form_s5->codAdscrita = $request->codAdscrita;
        }

        if($form_s5->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos de las Unidades Administrativa';
          $bit->save();
         
        }

        return back()->with('msj', 'Datos Registrados Exitosamente');
        }else{
      return back()->with('errormsj', 'El código de la unidad "'.$request->codUnidad.'" ya existe, por favor intente otro.');

        }   
    }
    public function edit($id)

    {

        $form_s5 = modeloS5::find($id);
        $lastCod = modeloS5::select('codUnidad')->get()->last();
        $categoria = sel_categoria::all();

        return view('layouts.ModificarAnexosS.modificarUnidades', compact('form_s5','lastCod','categoria'));
    }

    public function update(Request $request, $id)
    {
        $form_s5 = modeloS5::find($id);
        $form_s5->codUnidad = $request->codUnidad;
        $form_s5->descUnidad = $request->descUnidad;
        $form_s5->categoria = $request->categoria;

        if($form_s5->espeCatego = $request->espeCatego == '')
        {
            $form_s5->espeCatego = 1;
        }else{
            $form_s5->espeCatego = $request->espeCatego;
        }

        if($form_s5->codAdscrita = $request->codAdscrita == '')
        {
            $form_s5->codAdscrita = 1;
        }else{
            $form_s5->codAdscrita = $request->codAdscrita;
        }


        if($form_s5->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Datos de las Unidades Administrativas del Órgano o Ente';
          $bit->save();
 

        return back()->with('msj', 'Datos modificados exitosamente');
        }else {
        return back()->with('errormsj', 'Los datos no se modificaron');
        }
    }

    public function exportUnidades()
     {
         \Excel::create('unidades', function($excel) {
        
          $unidades = modeloS5::all();
          
          $excel->sheet('unidades', function($sheet) use($unidades) {
     
          /*========CABECERA DE LA FILA N° 1========*/


          $sheet->row(1, [
              'CÓDIGO DE LA UNIDAD', 'DESCRIPCIÓN DE LA UNIDAD', 'CÓDIGO DE LA CATEGORÍA DE LA UNIDAD', 'DENOMINACIÓN DE LA CATEGORÍA', 'CÓDIGO DE UNIDAD A LA CUAL ESTÁ ADSCRITA'

          ]);

          $sheet->setWidth([
                        'A'     =>  60,
                        'B'     =>  110,
                        'C'     =>  50,
                        'D'     =>  80,
                        'E'     =>  60,
                        
                      
                      
                    ]);

          $sheet->setHeight(1, 35);
                    $sheet->cell('A1:E4000',function($cell) 
                    {
                        $cell->setAlignment('center');    
                                           
                    });

          /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
        foreach($unidades  as $index => $unidades) 
        {
                


                /*===DESCRIPCIÓN DE LA UNIDAD===*/
                if($unidades->descUnidad == '1')
                {
                  $descUnidad = 'xxx';

                }else{
                  
                 $descUnidad=$unidades->descUnidad;
                }


                 /*===ESPECIFIQUE LA CATEGORÍA===*/
                if($unidades->espeCatego == '1')
                {
                  $espeCatego = 'noaplica';

                }else{
                  
                  $espeCatego=$unidades->espeCatego;
                }

                /*===CÓDIGO DE LA UNIDAD A LA CUAL ESTÁ ADSCRITA===*/
                if($unidades->codAdscrita == '1')
                {
                  $codAdscrita = '0';

                }else{
                  
                  $codAdscrita=$unidades->codAdscrita;
                }


            $sheet->row($index+2, 
               [
                  $unidades->codUnidad, 
                  $descUnidad, 
                  $unidades->categoria, 
                  $espeCatego,
                  $codAdscrita,
               

               ]); 
        }

      });
   
      })->export('xlsx');
    }
}
