<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloBienes;
use App\modeloDirecta;
use App\modeloS4;
use App\modeloS5;
use App\modeloResponsables;
use App\sel_estatusbien;
use App\sel_seguros2;
use App\sel_condicionbien;
use App\modeloMarcas;
use App\modeloModelos;
use App\sel_garantiabien;
use App\sel_seguros3;
use App\sel_colorbien;
use App\sel_catalogo;
use App\modeloBitacora;

class controladorBienes extends Controller
{
    public function index()
    {

     $lastCod = modeloBienes::select('codOt2_1')->get()->last();
     $catalogo = sel_catalogo::all();
     $unidad = modeloS5::all();
     $sede = modeloS4::all();
     $responsable = modeloResponsables::select('codResp')->get();
     $estatusBien = sel_estatusbien::all();
     $moneda = sel_seguros2::all();
     $condicion = sel_condicionbien::all();
     $marcas = modeloMarcas::all();
     $modelos = modeloModelos::all();
     $colorBien = sel_colorbien::all();
     $unidadGarantia = sel_garantiabien::all();
     $poseeComponente = sel_seguros3::all();
     $seguroBien = sel_seguros3::all();
          
     return view('AnexosT.visBienes', compact('lastCod','catalogo','sede','responsable','unidad','estatusBien','moneda','condicion','marcas','modelos','colorBien','unidadGarantia','poseeComponente','seguroBien'));

    }

    public function getBienes(Request $request, $id)
    {
    
      if($request->ajax()){
         $bienes1 = modeloBienes::selectBienes($id);
          return response()->json($bienes1);
      }
    }

     public function getBienesmodif(Request $request, $id)
    {
    
      if($request->ajax()){
         $bienes2 = modeloBienes::selectBienesmodif($id);
          return response()->json($bienes2);
      }
    }

    /* ===========SELECT DINAMICO SEDE Y DEPENDENCIA=========== */

     public function depenResponsable(Request $request, $id)
    {
    
      if($request->ajax()){
         $sede = modeloBienes::selectRespon($id);
          return response()->json($sede);
      }
    }


    public function store(Request $request)
    {
         $form_t8 = new modeloBienes();
         $form_t8->codOt2_1 = $request->codOt2_1;
         $form_t8->codCata = $request->codCata;
         $form_t8->codUnidad = $request->codUnidad;
         $form_t8->sedeOrgano = $request->sedeOrgano;
         $form_t8->estatuBien = $request->estatuBien;
         $form_t8->moneda = $request->moneda;
         $form_t8->edoBien = $request->edoBien;
         $form_t8->codMarca = $request->codMarca;
         $form_t8->codModel = $request->codModel;
         $form_t8->codColorBien = $request->codColorBien;
         $form_t8->unidadMedi = $request->unidadMedi;
         $form_t8->poseeCompo = $request->poseeCompo;
         $form_t8->seguroBien = $request->seguroBien;

         if($form_t8->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Por favor agregue el código del responsable administrativo y del uso directo del bien, para modificar el registro.');
         }else{
            $form_t8->codRespAdm = $request->codRespAdm;
         }

         if($form_t8->codResBien = $request->codResBien == '')
         {
          return back()->with('errormsj', 'Por favor agregue el código del responsable administrativo y del uso directo del bien, para modificar el registro.');

         }else{
          $form_t8->codResBien = $request->codResBien;
         }

         if($form_t8->codInterno = $request->codInterno == '')
         {
          $form_t8->codInterno = '1';

         }else{
          $form_t8->codInterno = $request->codInterno;
         }

         if($form_t8->espOtroUso = $request->espOtroUso == '')
         {
          $form_t8->espOtroUso = '1';

         }else{
          $form_t8->espOtroUso = $request->espOtroUso;
         }

         if($form_t8->valorAdq = $request->valorAdq == '')
         {
          $form_t8->valorAdq = '0';

         }else{
          $form_t8->valorAdq = $request->valorAdq;
         }

         if($form_t8->anoFabriBien = $request->anoFabriBien == '')
         {
          $form_t8->anoFabriBien = '0';

         }else{
          $form_t8->anoFabriBien = $request->anoFabriBien;
         }


         if($form_t8->espeMoneda = $request->espeMoneda == '')
         {
          $form_t8->espeMoneda = '1';

         }else{
          $form_t8->espeMoneda = $request->espeMoneda;
         }

         if($form_t8->feAdqBien = $request->feAdqBien == ''){
          $form_t8->feAdqBien = '11111111';
          }else{
          $form_t8->feAdqBien = $request->feAdqBien;  
        }

        if($form_t8->feIngBien = $request->feIngBien == ''){
          $form_t8->feIngBien = '11111111';
          }else{
          $form_t8->feIngBien = $request->feIngBien;  
        }

        if($form_t8->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t8->espOtroEdo = '1';

         }else{
          $form_t8->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t8->descEdoBien = $request->descEdoBien == '')
         {
          $form_t8->descEdoBien = '1';

         }else{
          $form_t8->descEdoBien = $request->descEdoBien;
        }

        if($form_t8->serialBien = $request->serialBien == '')
         {
          $form_t8->serialBien = '1';

         }else{
          $form_t8->serialBien = $request->serialBien;
        }


        if($form_t8->espeColor = $request->espeColor == '')
         {
          $form_t8->espeColor = '1';

         }else{
          $form_t8->espeColor = $request->espeColor;
        }

        if($form_t8->otraEspeColor = $request->otraEspeColor == '')
         {
          $form_t8->otraEspeColor = '1';

         }else{
          $form_t8->otraEspeColor = $request->otraEspeColor;
        }

        if($form_t8->espeTecBien = $request->espeTecBien == '')
         {
          $form_t8->espeTecBien = '1';

         }else{
          $form_t8->espeTecBien = $request->espeTecBien;
        }

        if($form_t8->otraDescBien = $request->otraDescBien == '')
         {
          $form_t8->otraDescBien = '1';

         }else{
          $form_t8->otraDescBien = $request->otraDescBien;
        }

        if($form_t8->garantia = $request->garantia == '')
         {
          $form_t8->garantia = '1';

         }else{
          $form_t8->garantia = $request->garantia;
        }

        if($form_t8->feIniGarantia = $request->feIniGarantia == ''){
          $form_t8->feIniGarantia = '11111111';
          }else{
          $form_t8->feIniGarantia = $request->feIniGarantia;  
        }

        if($form_t8->feFinGarantia = $request->feFinGarantia == ''){
          $form_t8->feFinGarantia = '11111111';
          }else{
          $form_t8->feFinGarantia = $request->feFinGarantia;  
        }

        if($form_t8->codRegSeguro = $request->codRegSeguro == ''){
          $form_t8->codRegSeguro = '1';
          }else{
          $form_t8->codRegSeguro = $request->codRegSeguro;  
        }

        if($form_t8->save()){

          //Función para mostrar la bitacora de las acciones de los usuarios tabla BD: bitacora , acciones.
          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Bienes';
          $bit->save();


            return back()->with('msj', 'Datos Registrados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }

    }

    public function edit($id)
    {
          $form_t8 = modeloBienes::find($id);
          $lastCod = modeloBienes::select('codOt2_1')->get()->last();
          $catalogo = sel_catalogo::all();
          $unidad = modeloS5::all();
          $responsable = modeloResponsables::select('codResp')->get();
          $estatusBien = sel_estatusbien::all();
          $moneda = sel_seguros2::all();
          $condicion = sel_condicionbien::all();
          $marcas = modeloMarcas::all();
          $modelos = modeloModelos::all();
          $colorBien = sel_colorbien::all();
          $unidadGarantia = sel_garantiabien::all();
          $poseeComponente = sel_seguros3::all();
          $seguroBien = sel_seguros3::all();
          $sede = modeloS4::all();

        return view('layouts.ModificarAnexosT.modificarBienes', compact('form_t8','lastCod','catalogo','unidad','responsable','estatusBien','moneda','condicion','marcas','modelos','colorBien','unidadGarantia','poseeComponente','seguroBien','sede'));
    }

     public function update(Request $request, $id)
    {
         $form_t8=modeloBienes::find($id);
         $form_t8->codOt2_1 = $request->codOt2_1;
         $form_t8->codCata = $request->codCata;
         $form_t8->codUnidad = $request->codUnidad;
         $form_t8->sedeOrgano = $request->sedeOrgano;
         $form_t8->estatuBien = $request->estatuBien;
         $form_t8->moneda = $request->moneda;
         $form_t8->edoBien = $request->edoBien;
         $form_t8->codMarca = $request->codMarca;
         $form_t8->codModel = $request->codModel;
         $form_t8->codColorBien = $request->codColorBien;
         $form_t8->unidadMedi = $request->unidadMedi;
         $form_t8->poseeCompo = $request->poseeCompo;
         $form_t8->seguroBien = $request->seguroBien;

         if($form_t8->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Debe agregar el código del responsable administrativo y del uso directo del bien, para modificar el registro.');
         }else{
            $form_t8->codRespAdm = $request->codRespAdm;
         }

         if($form_t8->codResBien = $request->codResBien == '')
         {
          return back()->with('errormsj', 'Debe seleccionar el código del responsable administrativo y del uso directo del bien, para modificar el registro.');

         }else{
          $form_t8->codResBien = $request->codResBien;
         }

         if($form_t8->codInterno = $request->codInterno == '')
         {
          $form_t8->codInterno = '1';

         }else{
          $form_t8->codInterno = $request->codInterno;
         }

         if($form_t8->espOtroUso = $request->espOtroUso == '')
         {
          $form_t8->espOtroUso = '1';

         }else{
          $form_t8->espOtroUso = $request->espOtroUso;
         }

         if($form_t8->valorAdq = $request->valorAdq == '')
         {
          $form_t8->valorAdq = '0';

         }else{
          $form_t8->valorAdq = $request->valorAdq;
         }

         if($form_t8->espeMoneda = $request->espeMoneda == '')
         {
          $form_t8->espeMoneda = '1';

         }else{
          $form_t8->espeMoneda = $request->espeMoneda;
         }

         if($form_t8->anoFabriBien = $request->anoFabriBien == ''){
          $form_t8->anoFabriBien = '0';
          
          }else{

          $form_t8->anoFabriBien = $request->anoFabriBien;  
        }

         if($form_t8->feAdqBien = $request->feAdqBien == ''){
          $form_t8->feAdqBien = '11111111';
          
          }else{

          $form_t8->feAdqBien = $request->feAdqBien;  
        }

        if($form_t8->feIngBien = $request->feIngBien == ''){
          $form_t8->feIngBien = '11111111';
          }else{
          $form_t8->feIngBien = $request->feIngBien;  
        }

        if($form_t8->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t8->espOtroEdo = '1';

         }else{
          $form_t8->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t8->descEdoBien = $request->descEdoBien == '')
         {
          $form_t8->descEdoBien = '1';

         }else{
          $form_t8->descEdoBien = $request->descEdoBien;
        }

        if($form_t8->serialBien = $request->serialBien == '')
         {
          $form_t8->serialBien = '1';

         }else{
          $form_t8->serialBien = $request->serialBien;
        }


        if($form_t8->espeColor = $request->espeColor == '')
         {
          $form_t8->espeColor = '1';

         }else{
          $form_t8->espeColor = $request->espeColor;
        }

        if($form_t8->otraEspeColor = $request->otraEspeColor == '')
         {
          $form_t8->otraEspeColor = '1';

         }else{
          $form_t8->otraEspeColor = $request->otraEspeColor;
        }

        if($form_t8->espeTecBien = $request->espeTecBien == '')
         {
          $form_t8->espeTecBien = '1';

         }else{
          $form_t8->espeTecBien = $request->espeTecBien;
        }

        if($form_t8->otraDescBien = $request->otraDescBien == '')
         {
          $form_t8->otraDescBien = '1';

         }else{
          $form_t8->otraDescBien = $request->otraDescBien;
        }

        if($form_t8->garantia = $request->garantia == '')
         {
          $form_t8->garantia = '1';

         }else{
          $form_t8->garantia = $request->garantia;
        }

        if($form_t8->feIniGarantia = $request->feIniGarantia == ''){
          $form_t8->feIniGarantia = '11111111';
          }else{
          $form_t8->feIniGarantia = $request->feIniGarantia;  
        }

        if($form_t8->feFinGarantia = $request->feFinGarantia == ''){
          $form_t8->feFinGarantia = '11111111';
          }else{
          $form_t8->feFinGarantia = $request->feFinGarantia;  
        }

        if($form_t8->codRegSeguro = $request->codRegSeguro == ''){
          $form_t8->codRegSeguro = '1';
          }else{
          $form_t8->codRegSeguro = $request->codRegSeguro;  
        }

        if($form_t8->save()){

          $bit = new modeloBitacora();
            $bit->user = $_SESSION['id'];
            $bit->accion  = 2;
            $bit->referencia = 'Bienes';
            $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }

    public function exportBienes()
    {

      \Excel::create('bienes', function($excel) {
    
      $bienes = modeloBienes::all();
          $unidad = modeloS5::all();
          $estatusBien = sel_estatusbien::all();
          $moneda = sel_seguros2::all();
          $condicion = sel_condicionbien::all();
          $marcas = modeloMarcas::all();
          $modelos = modeloModelos::all();
          $colorBien = sel_colorbien::all();
          $unidadGarantia = sel_garantiabien::all();
          $poseeComponente = sel_seguros3::all();
      
      $excel->sheet('bienes', function($sheet) use($bienes) {
 
      /*========CABECERA DE LA FILA N° 1========*/

      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN DEL BIEN', 'CÓDIGO SEGÚN EL CATALOGO', 'DEPENDENCIA ADMINISTRATIVA', 'SEDE DEL ÓRGANO O ENTE DONDE SE ENCUENTRA EL BIEN', 'CÓDIGO DEL RESPONSABLE ADMINISTRATIVO','CÓDIGO DEL RESPONSABLE DEL USO DIRECTO DEL BIEN', 'CÓDIGO INTERNO DEL BIEN', 'ESTATUS DEL USO DEL BIEN', 'ESPECIFIQUE EL OTRO USO', 'VALOR DE ADQUISICIÓN DEL BIEN', 'MONEDA', 'ESPECIFIQUE LA OTRA MONEDA', 'ESTADO DEL BIEN', 'ESPECIFIQUE EL OTRO ESTADO DEL BIEN', 'SERIAL DEL BIEN', 'FECHA DE ADQUISICIÓN DEL BIEN', 'FECHA DE INGRESO DEL BIEN', 'DESCRIPCIÓN DEL ESTADO DEL BIEN', 'CÓDIGO DE LA MARCA DEL BIEN', 'CÓDIGO DEL MODELO DEL BIEN', 'AÑO DE FABRICACIÓN DEL BIEN', 'CÓDIGO DEL COLOR DEL BIEN', 'ESPECIFICACIÓN DE COLOR', 'OTRAS ESPECIFICACIONES DEL COLOR', 'ESPECIFICACIONES TÉCNICAS DEL BIEN', 'OTRAS ESPECIFICACIONES DE DESCRIPCIÓN DEL BIEN', 'GARANTÍA', 'UNIDAD DE MEDIDA DE LA GARANTÍA', 'FECHA DE INICIO DE LA GARANTÍA', 'FECHA FIN DE LA GARANTÍA', 'POSEE COMPONENTES', 'SE ENCUENTRA ASEGURADO EL BIEN', 'CÓDIGO DEL REGISTRO DE SEGURO',

      ]);

      $sheet->setWidth([
                    'A'     =>  40, /*==Código del Origen del Bien==*/
                    'B'     =>  40, /*==Código según el catalogo==*/
                    'C'     =>  40, /*==Dependencia Administrativa==*/
                    'D'     =>  70, /*==Sede del Órgano o Ente Donde se Encuentra el Bien==*/
                    'E'     =>  60, /*==Código del Responsable Administrativo==*/
                    'F'     =>  60, /*==Código del Responsable del uso directo del Bien==*/
                    'G'     =>  40, /*==Código interno del Bien==*/
                    'H'     =>  35, /*==Estatus del uso del Bien==*/
                    'I'     =>  40, /*==Especifique el otro uso==*/
                    'J'     =>  40, /*==Valor de Adquisición==*/
                    'K'     =>  35, /*==Moneda==*/
                    'L'     =>  40, /*==Especifique Moneda==*/
                    'M'     =>  35, /*==Estado del Bien==*/
                    'N'     =>  40, /*==Especifique el Otro Estado del Bien==*/
                    'O'     =>  40, /*==Serial del Bien==*/
                    'P'     =>  35, /*==Fecha de Adquisición del Bien==*/
                    'Q'     =>  35, /*==Fecha de Ingreso del Bien==*/
                    'R'     =>  200, /*==Descripción del Estado del Bien==*/
                    'S'     =>  40, /*==Código de la Marca==*/
                    'T'     =>  40, /*==Código del Modelo==*/
                    'U'     =>  40, /*==Año de Fabricación==*/
                    'V'     =>  40, /*==Código del Color del Bien==*/
                    'W'     =>  40, /*==Especificación de Color==*/
                    'X'     =>  200, /*==Otras Especificaciones del Color==*/
                    'Y'     =>  200, /*==Especificaciones Técnicas del Bien==*/
                    'Z'     =>  200, /*==Otras Especificaciones de Descripción del Bien==*/
                    'AA'     =>  40, /*==Garantía==*/
                    'AB'     =>  40, /*==Unidad de Medida de la Garantía==*/
                    'AC'     =>  40, /*==Fecha Inicio de la Garantía==*/
                    'AD'     =>  40, /*==Fecha Fin de la Garantía==*/
                    'AE'     =>  40, /*==Posee Componentes==*/
                    'AF'     =>  40, /*==Se encuentra Asegurado el Bien==*/
                    'AG'     =>  40, /*==Código del Registro de Seguro==*/
                    
                   
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:AG5000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($bienes  as $index => $bienes) {
              
       

            /*========Sede del Órgano o Ente Donde se Encuentra el Bien=======*/

            if($bienes->sedeOrgano == '1')
            {
              $sedeOrgano = 'xxx';

            }else{

              $sedeOrgano=$bienes->sedeOrgano;
            }

            if($bienes->codInterno == '1')
            {
              $codInterno = 'xxx';

            }else{

              $codInterno=$bienes->codInterno;
            }

            /*====Especifique el otro uso====*/
           
            if($bienes->espOtroUso == '1')
            {
              $espOtroUso = 'noaplica';

            }else{

              $espOtroUso=$bienes->espOtroUso;
            }

            /*====Valor de Adquisición====*/
           
            if($bienes->valorAdq == '0')
            {
              $valorAdq = '99.99';

            }else{

              $valorAdq=$bienes->valorAdq;
            }

            /*====Especifique la Otra Moneda====*/
           
            if($bienes->espeMoneda == '1')
            {
              $espeMoneda = 'noaplica';

            }else{

              $espeMoneda=$bienes->espeMoneda;
            }

            /*====Especifique el Otro Estado del Bien====*/
           
            if($bienes->espOtroEdo == '1')
            {
              $espOtroEdo = 'noaplica';

            }else{

              $espOtroEdo=$bienes->espOtroEdo;
            }

            /*====Serial del Bien====*/
           
            if($bienes->serialBien == '1')
            {
              $serialBien = 'xxx';

            }else{

              $serialBien=$bienes->serialBien;
            }

            /*===Fecha de Adquisición del Bien===*/

            if($bienes->feAdqBien == '1111-11-11')
            {
              $feAdqBien = '11111111';

            }else{

              $feAdqBien = $bienes->feAdqBien;
              $feAdqBien = date("dmY", strtotime($feAdqBien));
            }

            /*===Fecha de Ingreso del Bien===*/

            if($bienes->feIngBien == '1111-11-11')
            {
              $feIngBien = '11111111';

            }else{

              $feIngBien = $bienes->feAdqBien;
              $feIngBien = date("dmY", strtotime($feIngBien));
              
            }

            /*===Descripción del Estado del Bien===*/

            if($bienes->descEdoBien == '1')
            {
              $descEdoBien = 'xxx';

            }else{

              $descEdoBien=$bienes->descEdoBien;
            }

            /*===Año de Fabricación del Bien===*/

            if($bienes->anoFabriBien == '0')
            {
              $anoFabriBien = 'xxx';

            }else{

              $anoFabriBien=$bienes->anoFabriBien;
            }

            /*===Especificación de Color===*/

            if($bienes->espeColor == '1')
            {
              $espeColor = 'noaplica';

            }else{

              $espeColor=$bienes->espeColor;
            }

            /*===Otras Especificaciones del Color===*/

            if($bienes->otraEspeColor == '1')
            {
              $otraEspeColor = 'xxx';

            }else{

              $otraEspeColor=$bienes->otraEspeColor;
            }

            /*===Especificaciones Técnicas del Bien===*/

            if($bienes->espeTecBien == '1')
            {
              $espeTecBien = 'xxx';

            }else{

              $espeTecBien=$bienes->espeTecBien;
            }

            /*===Otras Especificaciones de Descripción del Bien===*/

            if($bienes->otraDescBien == '1')
            {
              $otraDescBien = 'xxx';

            }else{

              $otraDescBien=$bienes->otraDescBien;
            }

            /*===Garantía===*/

            if($bienes->garantia == '1')
            {
              $garantia = 'xxx';

            }else{

              $garantia=$bienes->garantia;
            }

            /*===Fecha de Ingreso del Bien===*/

            if($bienes->feIniGarantia == '1111-11-11')
            {
              $feIniGarantia = '11111111';

            }else{

              $feIniGarantia = $bienes->feIniGarantia;
              $feIniGarantia = date("dmY", strtotime($feIniGarantia));
             
            }

            /*===Fecha de Ingreso del Bien===*/

            if($bienes->feFinGarantia == '1111-11-11')
            {
              $feFinGarantia = '11111111';

            }else{

              $feFinGarantia = $bienes->feFinGarantia;
              $feFinGarantia = date("dmY", strtotime($feFinGarantia));
             
            }

            /*===Código del Registro de Seguro===*/

            if($bienes->codRegSeguro == '1')
            {
              $codRegSeguro = 'xxx';

            }else{

              $codRegSeguro=$bienes->codRegSeguro;
            }


          $sheet->row($index+2, [
              $bienes->codOt2_1, 
              $bienes->selectCatalogo->codigo, 
              $bienes->selectUnidadbienes->codUnidad,
              $bienes->selectResponsable->codResp,
              $bienes->ResponsableDirecto->codResp,
              $sedeOrgano, 
              $codInterno, 
              $bienes->selectEstatus->opcion, 
              $espOtroUso,
              $valorAdq,
              $bienes->selectSeguros->opcion, 
              $espeMoneda,
              $bienes->selectCondicion->opcion,
              $espOtroEdo,
              $serialBien,
              $feAdqBien,
              $feIngBien,
              $descEdoBien,
              $bienes->selectMarcabien->codMarca,
              $bienes->selectModelbien->codModel,
              $anoFabriBien,
              $bienes->codColorBien,
              $espeColor,
              $otraEspeColor,
              $espeTecBien,
              $otraDescBien,
              $garantia,
              $bienes->selectUnidad->opcion,
              $feIniGarantia,
              $feFinGarantia,
              $bienes->selectComponente->opcion,
              $bienes->selectAsegurado->opcion,
              $codRegSeguro,

          ]);

        }

      });
   
      })->export('xlsx');
    }

}
