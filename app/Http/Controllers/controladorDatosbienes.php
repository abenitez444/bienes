<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloDatosbienes;
use App\modeloMarcas;
use App\modeloModelos;
use App\modeloComponentes;
use App\modeloBitacora;

class controladorDatosbienes extends Controller
{
    public function index()
    {
    	$lastCod = modeloDatosbienes::select('codBien')->get()->last();
		  $marcas = modeloMarcas::all();
     	$modelos = modeloModelos::all();
      $componente = modeloComponentes::all();

     	return view('AnexosT.visDatosbienes', compact('lastCod','marcas','modelos','componente'));
    }

    public function store(Request $request)
    {
        	$form_t11 = new modeloDatosbienes();
        	$form_t11->codBien = $request->codBien;
        	$form_t11->codMarca = $request->codMarca;
          $form_t11->codModel = $request->codModel;
        	$form_t11->codigo = $request->codigo;

    	  if($form_t11->codCompo = $request->codCompo == ''){
            $form_t11->codCompo = '1';

            }else{
            $form_t11->codCompo = $request->codCompo;    
            }

        if($form_t11->serialCompo = $request->serialCompo == ''){
            $form_t11->serialCompo = '1';

            }else{
            $form_t11->serialCompo = $request->serialCompo;    
            }

        if($form_t11->descCompo = $request->descCompo == ''){
            $form_t11->descCompo = '1';

            }else{
            $form_t11->descCompo = $request->descCompo;    
            }

        if($form_t11->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos de los Bienes';
          $bit->save();
          
          }
            return back()->with('msj', 'Datos Registrados Exitosamente');
    }

     public function edit($id)
    {
          $form_t11 = modeloDatosbienes::find($id);
          $lastCod = modeloDatosbienes::select('codBien')->get()->last();
          $marcas = modeloMarcas::all();
          $modelos = modeloModelos::all();
     	  $componente = modeloComponentes::all();
         

        return view('layouts.ModificarAnexosT.modificarDatosbienes', compact('form_t11','lastCod','marcas','modelos','componente'));
    }

     public function update(Request $request, $id)
    {
	      $form_t11 = modeloDatosbienes::find($id);
	      $form_t11->codBien = $request->codBien;
	      $form_t11->codMarca = $request->codMarca;
        $form_t11->codModel = $request->codModel;
	      $form_t11->codigo = $request->codigo;

    	  if($form_t11->codCompo = $request->codCompo == ''){
            $form_t11->codCompo = '1';

            }else{
            $form_t11->codCompo = $request->codCompo;    
            }

        if($form_t11->serialCompo = $request->serialCompo == ''){
            $form_t11->serialCompo = '1';

            }else{
            $form_t11->serialCompo = $request->serialCompo;    
            }

        if($form_t11->descCompo = $request->descCompo == ''){
            $form_t11->descCompo = '1';

            }else{
            $form_t11->descCompo = $request->descCompo;    
            }

        if($form_t11->save()){

            $bit = new modeloBitacora();
            $bit->user = $_SESSION['id'];
            $bit->accion  = 2;
            $bit->referencia = 'Datos de los Bienes';
            $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }

    public function exportMuebles()
    {
       \Excel::create('datosBienes', function($excel) {
    
      $datosBienes = modelodatosBienes::all();
      
      $excel->sheet('datosBienes', function($sheet) use($datosBienes) {
 
      /*========CABECERA DE LA FILA N° 1========*/


      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN DEL BIEN', 'CÓDIGO INTERNO DEL COMPONENTE', 'SERIAL DEL COMPONENTE', 'CÓDIGO DEL TIPO DE COMPONENTE', 'CÓDIGO DE LA MARCA DEL BIEN', 'CÓDIGO DEL MODELO DEL BIEN', 'DESCRICIÓN DEL COMPONENTE'

      ]);

      $sheet->setWidth([
                    'A'     =>  40,
                    'B'     =>  40,
                    'C'     =>  40,
                    'D'     =>  40,
                    'E'     =>  40,
                    'F'     =>  40,
                    'G'     =>  70,
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:G5000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($datosBienes  as $index => $datosBienes) {
            
            

            /*========Nombre del Propietario Anterior=======*/

            if($datosBienes->codCompo == '1')
            {
              $codCompo = 'xxx';

            }else{

              $codCompo=$datosBienes->codCompo;
            }

            /*========Nombre del Beneficiario=======*/

            if($datosBienes->serialCompo == '1')
            {
              $serialCompo = 'xxx';

            }else{

              $serialCompo=$datosBienes->serialCompo;
            }

            /*========Descripción del Componente=======*/

            if($datosBienes->descCompo == '1')
            {
              $descCompo = 'xxx';

            }else{

              $descCompo=$datosBienes->descCompo;
            }


          $sheet->row($index+2, [
              $datosBienes->codBien, 
              $codCompo, 
              $serialCompo, 
              $datosBienes->selectCompoDatosB->codigo, 
              $datosBienes->selectMarcadatosb->codMarca, 
              $datosBienes->selectModeldatosb->codModel, 
              $datosBienes->descCompo, 
          
          ]);

        }

      });
   
      })->export('xlsx');
    }
}
