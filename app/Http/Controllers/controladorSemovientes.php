<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloSemovientes;
use App\sel_catalogo;
use App\modeloS4;
use App\modeloS5;
use App\modeloResponsables;
use App\sel_estatusbien;
use App\sel_seguros2;
use App\sel_condicionbien;
use App\sel_genero;
use App\sel_colorbien;
use App\sel_seguros3;
use App\sel_tipoanimal;
use App\sel_proposito;
use App\sel_medidapeso;
use App\modeloBitacora;

class controladorSemovientes extends Controller
{
    public function index()
    {
    	$lastCod = modeloSemovientes::select('codBien')->get()->last();
      $catalogo = sel_catalogo::all();
    	$unidad = modeloS5::all();
      $sede = modeloS4::all();
      $responsable = modeloResponsables::select('codResp')->get();
    	$estatusBien = sel_estatusbien::all();
    	$moneda = sel_seguros2::all();
    	$condicion = sel_condicionbien::all();
      $genero = sel_genero::all();
      $colorBien = sel_colorbien::all();
      $seguroBien = sel_seguros3::all();
      $tiposAnimal = sel_tipoanimal::all();
      $proposito = sel_proposito::all();
      $peso = sel_medidapeso::all();


        return view('AnexosT.visSemovientes', compact('lastCod','catalogo','unidad','responsable','estatusBien','moneda','condicion','genero','colorBien','seguroBien','tiposAnimal','proposito','peso','sede'));
    }

    public function depenResponsemo(Request $request, $id)
    {
    
      if($request->ajax()){
         $dependencia = modeloSemovientes::selectRespAdm($id);
          return response()->json($dependencia);
      }
    }

     public function store(Request $request)
    {
         $form_t10 = new modeloSemovientes();
         $form_t10->codBien = $request->codBien;
         $form_t10->codCata = $request->codCata;
         $form_t10->sedeOrgano = $request->sedeOrgano;
         $form_t10->codUnidad = $request->codUnidad;
         $form_t10->estatuBien = $request->estatuBien;
         $form_t10->moneda = $request->moneda;
         $form_t10->edoBien = $request->edoBien;
         $form_t10->genero = $request->genero;
         $form_t10->tipoAnimal = $request->tipoAnimal;
         $form_t10->proposito = $request->proposito;
         $form_t10->codColorBien = $request->codColorBien;
         $form_t10->seguroBien = $request->seguroBien;

         if($form_t10->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Por favor agregue el código del responsable administrativo y del uso directo del bien, para modificar el registro.');
         }else{
            $form_t10->codRespAdm = $request->codRespAdm;
         }

         if($form_t10->codResBien = $request->codResBien == '')
         {
          return back()->with('errormsj', 'Por favor agregue el código del responsable administrativo y del uso directo del bien, para modificar el registro.');

         }else{
          $form_t10->codResBien = $request->codResBien;
         }

         if($form_t10->codInterno = $request->codInterno == '')
         {
          $form_t10->codInterno = '1';

         }else{
          $form_t10->codInterno = $request->codInterno;
         }

         if($form_t10->espOtroUso = $request->espOtroUso == '')
         {
          $form_t10->espOtroUso = '1';

         }else{
          $form_t10->espOtroUso = $request->espOtroUso;
         }

         if($form_t10->valorAdq = $request->valorAdq == '')
         {
          $form_t10->valorAdq = '0';

         }else{
          $form_t10->valorAdq = $request->valorAdq;
         }
  
        if($form_t10->peso = $request->peso == '')
         {
          $form_t10->peso = '0';

         }else{
          $form_t10->peso = $request->peso;
         }
         
         if($form_t10->espeMoneda = $request->espeMoneda == '')
         {
          $form_t10->espeMoneda = '1';

         }else{
          $form_t10->espeMoneda = $request->espeMoneda;
         }

         if($form_t10->feAdqBien = $request->feAdqBien == ''){
          $form_t10->feAdqBien = '11111111';
          }else{
          $form_t10->feAdqBien = $request->feAdqBien;  
        }

        if($form_t10->feIngBien = $request->feIngBien == ''){
          $form_t10->feIngBien = '11111111';
          }else{
          $form_t10->feIngBien = $request->feIngBien;  
        }

        if($form_t10->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t10->espOtroEdo = '1';

         }else{
          $form_t10->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t10->descEdoBien = $request->descEdoBien == '')
         {
          $form_t10->descEdoBien = '1';

         }else{
          $form_t10->descEdoBien = $request->descEdoBien;
        }

        if($form_t10->raza = $request->raza == '')
         {
          $form_t10->raza = '1';

         }else{
          $form_t10->raza = $request->raza;
        }

        if($form_t10->espeOtroTipo = $request->espeOtroTipo == '')
         {
          $form_t10->espeOtroTipo = '1';

         }else{
          $form_t10->espeOtroTipo = $request->espeOtroTipo;
        }

        if($form_t10->espeOtroPro = $request->espeOtroPro == '')
         {
          $form_t10->espeOtroPro = '1';

         }else{
          $form_t10->espeOtroPro = $request->espeOtroPro;
        }

        if($form_t10->espeColor = $request->espeColor == '')
         {
          $form_t10->espeColor = '1';

         }else{
          $form_t10->espeColor = $request->espeColor;
        }

        if($form_t10->otraEspeColor = $request->otraEspeColor == '')
         {
          $form_t10->otraEspeColor = '1';

         }else{
          $form_t10->otraEspeColor = $request->otraEspeColor;
        }

        if($form_t10->unidadPeso = $request->unidadPeso == '')
         {
          $form_t10->unidadPeso = '1';

         }else{
          $form_t10->unidadPeso = $request->unidadPeso;
        }

        if($form_t10->feNacimiento = $request->feNacimiento == '')
         {
          $form_t10->feNacimiento = '11111111';

         }else{
          $form_t10->feNacimiento = $request->feNacimiento;
        }

        if($form_t10->seParticulares = $request->seParticulares == '')
         {
          $form_t10->seParticulares = '1';

         }else{
          $form_t10->seParticulares = $request->seParticulares;
        }

        if($form_t10->otrasEspecifi = $request->otrasEspecifi == '')
         {
          $form_t10->otrasEspecifi = '1';

         }else{
          $form_t10->otrasEspecifi = $request->otrasEspecifi;
        }

        if($form_t10->numHierro = $request->numHierro == '')
         {
          $form_t10->numHierro = '1';

         }else{
          $form_t10->numHierro = $request->numHierro;
        }

        if($form_t10->codRegSeguro = $request->codRegSeguro == '')
         {
          $form_t10->codRegSeguro = '1';

         }else{
          $form_t10->codRegSeguro = $request->codRegSeguro;
        }

        if($form_t10->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Semovientes';
          $bit->save();

        }

        return back()->with('msj', 'Datos Registrados Exitosamente');

    }

     public function edit($id)
    {     
          $form_t10 = modeloSemovientes::find($id);
          $lastCod = modeloSemovientes::select('codBien')->get()->last();
          $sede = modeloS4::all();
          $responsable = modeloResponsables::select('codResp')->get();
          $catalogo = sel_catalogo::all();
          $unidad = modeloS5::all();
          $estatusBien = sel_estatusbien::all();
          $moneda = sel_seguros2::all();
          $condicion = sel_condicionbien::all();
          $genero = sel_genero::all();
          $colorBien = sel_colorbien::all();
          $seguroBien = sel_seguros3::all();
          $tiposAnimal = sel_tipoanimal::all();
          $proposito = sel_proposito::all();
          $peso = sel_medidapeso::all();

        return view('layouts.ModificarAnexosT.modificarSemovientes', compact('form_t10','lastCod','catalogo','unidad','sede','responsable','estatusBien','moneda','condicion','genero','colorBien','seguroBien','tiposAnimal','proposito','peso'));
    }

    public function update(Request $request, $id)
    {
         $form_t10=modeloSemovientes::find($id); 
         $form_t10->codBien = $request->codBien;
         $form_t10->codCata = $request->codCata;
         $form_t10->codUnidad = $request->codUnidad;
         $form_t10->sedeOrgano = $request->sedeOrgano;
         $form_t10->estatuBien = $request->estatuBien;
         $form_t10->moneda = $request->moneda;
         $form_t10->edoBien = $request->edoBien;
         $form_t10->genero = $request->genero;
         $form_t10->tipoAnimal = $request->tipoAnimal;
         $form_t10->proposito = $request->proposito;
         $form_t10->codColorBien = $request->codColorBien;
         $form_t10->peso = $request->peso;
         $form_t10->seguroBien = $request->seguroBien;
        
         if($form_t10->codRespAdm = $request->codRespAdm == ''){
          return back()->with('errormsj', 'Debe agregar el código del responsable administrativo y del uso directo del bien, para modificar el registro.');
         }else{
            $form_t10->codRespAdm = $request->codRespAdm;
         }

         if($form_t10->codResBien = $request->codResBien == '')
         {
          return back()->with('errormsj', 'Debe seleccionar el código del responsable administrativo y del uso directo del bien, para modificar el registro.');

         }else{
          $form_t10->codResBien = $request->codResBien;
         }

         if($form_t10->codResBien = $request->codResBien == '')
         {
          $form_t10->codResBien = '1';

         }else{
          $form_t10->codResBien = $request->codResBien;
         }

         if($form_t10->codInterno = $request->codInterno == '')
         {
          $form_t10->codInterno = '1';

         }else{
          $form_t10->codInterno = $request->codInterno;
         }

         if($form_t10->espOtroUso = $request->espOtroUso == '')
         {
          $form_t10->espOtroUso = '1';

         }else{
          $form_t10->espOtroUso = $request->espOtroUso;
         }

         if($form_t10->valorAdq = $request->valorAdq == '')
         {
          $form_t10->valorAdq = '0';

         }else{
          $form_t10->valorAdq = $request->valorAdq;
         }

         if($form_t10->espeMoneda = $request->espeMoneda == '')
         {
          $form_t10->espeMoneda = '1';

         }else{
          $form_t10->espeMoneda = $request->espeMoneda;
         }

         if($form_t10->feAdqBien = $request->feAdqBien == ''){
          $form_t10->feAdqBien = '11111111';
          }else{
          $form_t10->feAdqBien = $request->feAdqBien;  
        }

        if($form_t10->feIngBien = $request->feIngBien == ''){
          $form_t10->feIngBien = '11111111';
          }else{
          $form_t10->feIngBien = $request->feIngBien;  
        }

        if($form_t10->espOtroEdo = $request->espOtroEdo == '')
         {
          $form_t10->espOtroEdo = '1';

         }else{
          $form_t10->espOtroEdo = $request->espOtroEdo;
        }

        if($form_t10->descEdoBien = $request->descEdoBien == '')
         {
          $form_t10->descEdoBien = '1';

         }else{
          $form_t10->descEdoBien = $request->descEdoBien;
        }


        if($form_t10->raza = $request->raza == '')
         {
          $form_t10->raza = '1';

         }else{
          $form_t10->raza = $request->raza;
        }

        if($form_t10->espeOtroTipo = $request->espeOtroTipo == '')
         {
          $form_t10->espeOtroTipo = '1';

         }else{
          $form_t10->espeOtroTipo = $request->espeOtroTipo;
        }

        if($form_t10->espeOtroPro = $request->espeOtroPro == '')
         {
          $form_t10->espeOtroPro = '1';

         }else{
          $form_t10->espeOtroPro = $request->espeOtroPro;
        }

        if($form_t10->espeColor = $request->espeColor == '')
         {
          $form_t10->espeColor = '1';

         }else{
          $form_t10->espeColor = $request->espeColor;
        }

        if($form_t10->otraEspeColor = $request->otraEspeColor == '')
         {
          $form_t10->otraEspeColor = '1';

         }else{
          $form_t10->otraEspeColor = $request->otraEspeColor;
        }

        if($form_t10->unidadPeso = $request->unidadPeso == '')
         {
          $form_t10->unidadPeso = '1';

         }else{
          $form_t10->unidadPeso = $request->unidadPeso;
        }

        if($form_t10->feNacimiento = $request->feNacimiento == '')
         {
          $form_t10->feNacimiento = '11111111';

         }else{
          $form_t10->feNacimiento = $request->feNacimiento;
        }

        if($form_t10->seParticulares = $request->seParticulares == '')
         {
          $form_t10->seParticulares = '1';

         }else{
          $form_t10->seParticulares = $request->seParticulares;
        }

        if($form_t10->otrasEspecifi = $request->otrasEspecifi == '')
         {
          $form_t10->otrasEspecifi = '1';

         }else{
          $form_t10->otrasEspecifi = $request->otrasEspecifi;
        }

        if($form_t10->numHierro = $request->numHierro == '')
         {
          $form_t10->numHierro = '1';

         }else{
          $form_t10->numHierro = $request->numHierro;
        }

        if($form_t10->codRegSeguro = $request->codRegSeguro == '')
         {
          $form_t10->codRegSeguro = '1';

         }else{
          $form_t10->codRegSeguro = $request->codRegSeguro;
        }

        if($form_t10->save()){

            $bit = new modeloBitacora();
            $bit->user = $_SESSION['id'];
            $bit->accion  = 2;
            $bit->referencia = 'Bienes';
            $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
          }
    }

    public function exportSemo()
     {

      \Excel::create('semovientes', function($excel) {
    
          $semovientes = modeloSemovientes::all();
          $unidad = modeloS5::all();
          $estatusBien = sel_estatusbien::all();
          $moneda = sel_seguros2::all();
          $condicion = sel_condicionbien::all();
          $colorBien = sel_colorbien::all();
          $poseeComponente = sel_seguros3::all();
          $tieneSistem = sel_seguros3::all();
      
      $excel->sheet('semovientes', function($sheet) use($semovientes) {
 
      /*========CABECERA DE LA FILA N° 1========*/

      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN',
          'CÓDIGO SEGÚN EL CATALOGO',
          'DEPENDENCIA ADMINISTRATIVA',
          'SEDE DEL ÓRGANO O ENTE DONDE SE ENCUENTRA EL BIEN',
          'CÓDIGO DEL RESPONSABLE ADMINISTRATIVO',
          'CÓDIGO DEL RESPONSABLE DEL USO DIRECTO DEL BIEN',
          'CÓDIGO INTERNO DEL BIEN', 
          'ESTATUS DEL USO DEL BIEN',
          'ESPECIFIQUE EL OTRO USO',
          'VALOR DE ADQUISICIÓN DEL BIEN',
          'MONEDA', 
          'ESPECIFIQUE LA OTRA MONEDA',
          'FECHA DE ADQUISICIÓN DEL BIEN',
          'FECHA DE INGRESO DEL BIEN', 
          'ESTADO DEL BIEN', 
          'ESPECIFIQUE EL OTRO ESTADO DEL BIEN', 
          'DESCRIPCIÓN DEL ESTADO DEL BIEN', 
          'RAZA',
          'GENERO',
          'TIPO DE ANIMAL',
          'ESPECIFIQUE EL OTRO TIPO',
          'PROPÓSITO',
          'ESPECIFIQUE EL OTRO PROPÓSITO',
          'CÓDIGO DEL COLOR DEL BIEN',
          'ESPECIFICACIÓN DEL OTRO COLOR',
          'OTRAS ESPECIFICACIONES DEL COLOR',
          'PESO',
          'UNIDAD DE MEDIDA DEL PESO',
          'FECHA DE NACIMIENTO',
          'SEÑA PARTICULAR',
          'OTRAS ESPECIFICACIONES',
          'NÚMERO DE HIERRO',
          'SE ENCUENTRA ASEGURADO EL BIEN',
          'CÓDIGO DEL REGISTRO DE SEGURO',

      ]);

      $sheet->setWidth([
                    'A'     =>  40, /*==Código del Origen==*/
                    'B'     =>  40, /*==Código según el catalogo==*/
                    'C'     =>  40, /*==Dependencia Administrativa==*/
                    'D'     =>  70, /*==Sede del Órgano o Ente Donde se Encuentra el Bien==*/
                    'E'     =>  60, /*==Código del Responsable Administrativo==*/
                    'F'     =>  70, /*==Código del Responsable del uso Directo del Bien==*/
                    'G'     =>  40, /*==Código Interno del Bien==*/
                    'H'     =>  40, /*==Estatus del Uso del Bien==*/
                    'I'     =>  40, /*==Especifique el Otro Uso==*/
                    'J'     =>  40, /*==Valor de Adquisición del Bien==*/
                    'K'     =>  40, /*==Moneda==*/
                    'L'     =>  40, /*==Especifique la Otra Moneda==*/
                    'M'     =>  40, /*==Fecha de Adquisición del Bien==*/
                    'N'     =>  40, /*==Fecha de Ingreso del Bien==*/
                    'O'     =>  40, /*==Estado del Bien==*/
                    'P'     =>  70, /*==Especifique el Otro Estado del Bien==*/
                    'Q'     =>  70, /*==Descripción del Estado del Bien==*/
                    'R'     =>  35, /*==Raza==*/
                    'S'     =>  35, /*==Genero==*/
                    'T'     =>  35, /*==Tipo de Animal==*/
                    'U'     =>  40, /*==Especifique el Otro Tipo==*/
                    'V'     =>  35, /*==Propósito==*/
                    'W'     =>  40, /*==Especifique el Otro Propósito==*/
                    'X'     =>  40, /*==Código del Color del Bien==*/
                    'Y'     =>  40, /*==Especificación del Otro Color==*/
                    'Z'     =>  40, /*==Otras Especificaciones del Color==*/
                    'AA'     =>  40, /*==Peso==*/
                    'AB'     =>  40, /*==Unidad de Medida del Peso==*/
                    'AC'     =>  40, /*==Fecha de Nacimiento==*/
                    'AD'     =>  40, /*==Seña Particular==*/
                    'AE'     =>  40, /*==Otras Especificaciones==*/
                    'AF'     =>  40, /*==Número de Hierro==*/
                    'AG'     =>  40, /*==Se encuentra asegurado el bien==*/
                    'AH'     =>  40, /*==Código del Registro de Seguro==*/

                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:AH3000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($semovientes  as $index => $semovientes) {
            
            /*========Sede del Órgano o Ente Donde se Encuentra el Bien=======*/

            if($semovientes->sedeOrgano == '1')
            {
              $sedeOrgano = 'xxx';

            }else{

              $sedeOrgano=$semovientes->sedeOrgano;
            }

            /*========Código del Responsable Administrativo=======*/

            if($semovientes->codRespAdm == '1')
            {
              $codRespAdm = 'xxx';

            }else{

              $codRespAdm=$semovientes->codRespAdm;
            }

            /*====Código del Responsable del uso directo del Bien====*/
           
            if($semovientes->codResBien == '1')
            {
              $codResBien = 'xxx';

            }else{

              $codResBien=$semovientes->codResBien;
            }

            /*====Código del Responsable del uso directo del Bien====*/
           
            if($semovientes->codInterno == '1')
            {
              $codInterno = 'xxx';

            }else{

              $codInterno=$semovientes->codInterno;
            }

            /*====Especifique el otro uso====*/
           
            if($semovientes->espOtroUso == '1')
            {
              $espOtroUso = 'noaplica';

            }else{

              $espOtroUso=$semovientes->espOtroUso;
            }

            /*====Valor de Adquisición====*/
           
            if($semovientes->valorAdq == '0')
            {
              $valorAdq = '99.99';

            }else{

              $valorAdq=$semovientes->valorAdq;
            }

            /*====Especifique la Otra Moneda====*/
           
            if($semovientes->espeMoneda == '1')
            {
              $espeMoneda = 'noaplica';

            }else{

              $espeMoneda=$semovientes->espeMoneda;
            }

            /*===Fecha de Adquisición del Bien===*/

            if($semovientes->feAdqBien == '1111-11-11')
            {
              $feAdqBien = '11111111';

            }else{

              $feAdqBien = $semovientes->feAdqBien;
              $feAdqBien = date("dmY", strtotime($feAdqBien));
            }

            /*===Fecha de Ingreso del Bien===*/

            if($semovientes->feIngBien == '1111-11-11')
            {
              $feIngBien = '11111111';

            }else{

              $feIngBien = $semovientes->feIngBien;
              $feIngBien = date("dmY", strtotime($feIngBien));
            }

            /*====Especifique el Otro Estado del Bien====*/
           
            if($semovientes->espOtroEdo == '1')
            {
              $espOtroEdo = 'noaplica';

            }else{

              $espOtroEdo=$semovientes->espOtroEdo;
            }

            /*===Descripción del Estado del Bien===*/

            if($semovientes->descEdoBien == '1')
            {
              $descEdoBien = 'xxx';

            }else{

              $descEdoBien=$semovientes->descEdoBien;
            }

            /*===Raza===*/

            if($semovientes->raza == '1')
            {
              $raza = 'xxx';

            }else{

              $raza=$semovientes->raza;
            }

            /*===Genero===*/

            if($semovientes->genero == '1')
            {
              $genero = 'H';

            }else{

              $genero= 'M';
            }

             /*===Especifique el Otro Tipo===*/

            if($semovientes->espeOtroTipo == '1')
            {
              $espeOtroTipo = 'noaplica';

            }else{

              $espeOtroTipo=$semovientes->espeOtroTipo;
            }

            /*===Especifique el Otro Propósito===*/

            if($semovientes->espeOtroPro == '1')
            {
              $espeOtroPro = 'noaplica';

            }else{

              $espeOtroPro=$semovientes->espeOtroPro;
            }

            /*===Especificación del Otro Color===*/

            if($semovientes->espeColor == '1')
            {
              $espeColor = 'noaplica';

            }else{

              $espeColor=$semovientes->espeColor;
            }

            /*===Otras Especificaciones del Color===*/

            if($semovientes->otraEspeColor == '1')
            {
              $otraEspeColor = 'xxx';

            }else{

              $otraEspeColor=$semovientes->otraEspeColor;
            }

            /*===Otras Especificaciones del Color===*/

            if($semovientes->peso == '0')
            {
              $peso = '99,99';

            }else{

              $peso=$semovientes->peso;
            }

            /*===Fecha de Nacimiento===*/

            if($semovientes->feNacimiento == '1111-11-11')
            {
               $feNacimiento = '11111111';

            }else{

               $feNacimiento = $semovientes->feNacimiento;
               $feNacimiento = date("dmY", strtotime($feNacimiento));
            }

            /*===Seña Particular===*/

            if($semovientes->seParticulares == '1')
            {
              $seParticulares = 'xxx';

            }else{

              $seParticulares=$semovientes->seParticulares;
            }

            /*===Otras Especificaciones===*/

            if($semovientes->otrasEspecifi == '1')
            {
              $otrasEspecifi = 'xxx';

            }else{

              $otrasEspecifi=$semovientes->otrasEspecifi;
            }

            /*===Número de Hierro===*/

            if($semovientes->numHierro == '1')
            {
              $numHierro = 'xxx';

            }else{

              $numHierro=$semovientes->numHierro;
            }

            /*===Número de Hierro===*/

            if($semovientes->codRegSeguro == '1')
            {
              $codRegSeguro = 'xxx';

            }else{

              $codRegSeguro=$semovientes->codRegSeguro;
            }

          $sheet->row($index+2, [
              $semovientes->codBien, 
              $semovientes->selectCatalogosem->codigo,
              $semovientes->selectUnidadsemo->codUnidad,
              $sedeOrgano, 
              $codRespAdm, 
              $codResBien, 
              $codInterno, 
              $semovientes->estatuBien, 
              $espOtroUso,
              $valorAdq,
              $semovientes->moneda,
              $espeMoneda,
              $feAdqBien,
              $feIngBien,
              $semovientes->edoBien,
              $espOtroEdo,
              $descEdoBien,
              $raza,
              $genero,
              $semovientes->tipoAnimal,
              $espeOtroTipo,
              $semovientes->proposito,
              $espeOtroPro,
              $semovientes->codColorBien,
              $espeColor,
              $otraEspeColor,
              $peso,
              $semovientes->unidadPeso,
              $feNacimiento,
              $seParticulares,
              $otrasEspecifi,
              $numHierro,
              $semovientes->selectSegurobiensemo->opcion,
              $codRegSeguro,
          ]);

        }

      });
   
      })->export('xlsx');
    }
}
