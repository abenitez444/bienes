<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS4;
use App\modeloBienes;
use App\modeloEqtransporte;
use App\modeloSemovientes;
use App\modeloInmuebles;
use App\modeloBitacora;


class con_s4Ver extends Controller
{
     public function index(){
        
        $verS4= modeloS4::all();

        return view('RegistrosS.regSedes', compact('verS4'));

    }

    public function selectId($id){

       $seleccion = modeloS4::find($id);

       return view('MuestraAnexosS.muestraSedes',compact('seleccion'));
    }

   
    public function anularSedes($id)
    {

        $datos = modeloBienes::where('sedeOrgano', $id)->count();    
        $datos2 = modeloEqtransporte::where('sedeOrgano', $id)->count();    
        $datos3 = modeloSemovientes::where('sedeOrgano', $id)->count();    
        $datos4 = modeloInmuebles::where('sedeOrgano', $id)->count();    
        
        if(empty($datos || $datos2 || $datos3 || $datos4)){

        $sedes= modeloS4::find($id);
        
        if($sedes->delete()){
        $bit = new modeloBitacora();
        $bit->user = $_SESSION['id'];
        $bit->accion  = 3;
        $bit->referencia = 'Datos de las Sedes y Similares del Ente';
        $bit->save();

             return redirect('regSedes')->with('msj', 'Registro Eliminado Exitosamente');
           } else {
                return redirect()->with('errormsj', 'Los datos no se guardaron');
           }
        }else{
             return redirect('regSedes')->with('errormsj', 'No puede eliminar éste registro, ya se encuentra registrada en otro formulario Ej:(Bienes, Transportes, Semovientes o Inmuebles.');
        }
     }

}  