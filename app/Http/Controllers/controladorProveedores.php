<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloProveedores;
use App\sel_proveedores;
use App\modeloBitacora;

class controladorProveedores extends Controller
{
      public function index()
    {
      $sel_proveedores = sel_proveedores::all();

      //ULTIMO REGISTRO DE UN CAMPO EN ESPECIFICO
    
        $lastCod = modeloProveedores::select('codProvee')->get()->last();
        
        $input1=array(
            array("codProvee"," Código del Proveedor:","Introduzca el código de proveedor","10","col-md-pull-4"),
            array("descProvee","Descripción de Proveedor:","Descripción del proveedor","100","col-md-push-0"),
          );

        $input2=array(
            array("otraDesc","Otra Descripción:","Introduzca otra descripción","200",""),
          );

        $input3= array(
          array("rifProvee","RIF:","Introduzca el número de RIF","20",""),
          );

        $selectt1=array(
               array("tipProvee","Tipo de Proveedor:","select","1","col-md-push-4"),
          );

        $selectt2 = array(
          array("grupo","grupoProve","1","col-md-push-4","J","V","G"),
          );

        return view('AnexosT.visProveedores', compact('input1','input2','input3','sel_proveedores','selectt1','selectt2','lastCod'));

    } 
          
    public function store(Request $request)
    {
      $duplicado = modeloProveedores::where('codProvee', $request->codProvee)->get();

      if($duplicado == '[]'){

        $form_t1= new modeloProveedores();
        $form_t1->codProvee = $request->codProvee;
        $form_t1->descProvee = $request->descProvee;
        $form_t1->tipProvee = $request->tipProvee;
        $form_t1->grupo = $request->grupo;
        $form_t1->rifProvee = $request->rifProvee;

        if ($form_t1->otraDesc = $request->otraDesc == '') {
        $form_t1->otraDesc = '0';
      
        }else{
          $form_t1->otraDesc = $request->otraDesc;
        }

         if($form_t1->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Proveedores';
          $bit->save();

         }

        return back()->with('msj', 'Datos Registrados Exitosamente');
            }else{
        return back()->with('errormsj', 'El Código de Proveedor "#'.$request->codProvee.'" ya existe, por favor siga el orden establecido e intente un código nuevo');
        
        } 
    }

    public function edit($id)
    {
        $form_t1 = modeloProveedores::find($id);
        $sel_proveedores = sel_proveedores::all();
        
        return view('layouts.ModificarAnexosT.modificarProveedores', compact('form_t1','sel_proveedores'));
    }

    public function update(Request $request, $id)
      {
        $form_t1 = modeloProveedores::find($id);
        $form_t1->codProvee = $request->codProvee;
        $form_t1->descProvee = $request->descProvee;
        $form_t1->tipProvee = $request->tipProvee;
        $form_t1->grupo = $request->grupo;
        $form_t1->rifProvee = $request->rifProvee;
        
        if ($form_t1->otraDesc = $request->otraDesc == '') {
        $form_t1->otraDesc = '0';
      
        }else{
          $form_t1->otraDesc = $request->otraDesc;
        }
        $form_t1->save();

        if($form_t1->save()){
          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Proveedores';
          $bit->save();
    
        
            return back()->with('msj', 'Datos modificados exitosamente');
        } else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }

      }

      /*===========Exportar tabla proveedores a archivo .CSV===========*/

    public function exportProvee(){

      \Excel::create('proveedores', function($excel) {
    
      $proveedores = modeloProveedores::all();
      $relacion = sel_proveedores::all();
      
      $excel->sheet('proveedores', function($sheet) use($proveedores) {
 
      /*========CABECERA DE LA FILA N° 1========*/


      $sheet->row(1, [
          'CÓDIGO DEL PROVEEDOR', 'DESCRIPCIÓN DEL PROVEEDOR ', 'TIPO DE PROVEEDOR', 'RIF ', 'OTRA DESCRIPCIÓN'

      ]);

      $sheet->setWidth([
                    'A'     =>  30,
                    'B'     =>  35,
                    'C'     =>  25,
                    'D'     =>  25,
                    'E'     =>  25,
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:E6000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($proveedores  as $index => $proveedores) {
            if($proveedores->grupo == 1 )
            {
              $grupo = 'J-'; 
            }
            elseif($proveedores->grupo == 2)
            {
              $grupo = 'V-'; 

            }else{
              
              $grupo = 'G-';
            }

            /*========TIPO DE PROVEEDOR=====*/
            if($proveedores->tipProvee == 1)
            {
              $tipProvee = 'N';

            }else{
              
              $tipProvee = 'I';
            }

           /*========OTRA DESCRIPCIÓN=======*/
            
            if($proveedores->otraDesc == '0')
            {
              $otraDesc = 'xxx';

            }else{

              $otraDesc=$proveedores->otraDesc;
            }



          $sheet->row($index+2, [
              $proveedores->codProvee, 
              $proveedores->descProvee, 
              $tipProvee, 
              $grupo ."". $proveedores->rifProvee,
              $otraDesc,

          ]);
        }

      });
   
      })->export('xlsx');
   }
}
     