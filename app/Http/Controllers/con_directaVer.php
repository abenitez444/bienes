<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloDirecta;
use App\modeloBitacora;
use App\sel_directa;

class con_directaVer extends Controller
{
    public function index(){
      	
      	$verT21= modeloDirecta::all();
        

      	return view('RegistrosT.regDirecta', compact('verT21'));

    }

    public function idDirecta($id){

        $infoSelect = sel_directa::all();
      	$seleccion = modeloDirecta::find($id);
          $fechaCom = $seleccion->feCom;
          $fechaCom = date("d/m/Y", strtotime($fechaCom));
          $fechaNot = $seleccion->feNota;
          $fechaNot = date("d/m/Y", strtotime($fechaNot));
          $fechaFac = $seleccion->feFac;
          $fechaFac = date("d/m/Y", strtotime($fechaFac));

       return view('MuestraAnexosT.muestraDirecta',compact('seleccion','infoSelect','fechaCom','fechaNot','fechaFac'));
    }

     /*FUNCIÓN anularDirec es para simular la eliminacion del registro en el datatable CUANDO ESTA EN 0 SE MUESTRA Y CUANDO CAMBIA A 1 EL REGISTRO NO SE MUESTRA*/

    public function anularDirec($id)
    {
        $seleccion= modeloDirecta::find($id);
        
        
       if($seleccion->delete()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 3;
          $bit->referencia = 'Compra Directa';
          $bit->save();

         return redirect('regDirecta')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
         return redirect()->with('errormsj', 'Los datos no se guardaron');
       }
    }
}
