<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloDonacion;
use App\sel_donacion;
use App\modeloBitacora;


class controladorDonacion extends Controller
{
     public function index()
    {
        
        $infoSelect = sel_donacion::all();

        $lastCod = modeloDonacion::select('codOt2_4')->get()->last();

        $arrayT24 = array(
            array("codOt2_4","Código de Origen:","Introduzca número consecutivo. Ej: E-2; E-3;","12","col-md-pull-4",""),
            #array("codAdq","CÓDIGO DE LA FORMA DE ADQUISICIÓN","Introduzca el N° el código de origen","12","col-md-pull-4"),
            array("nomDona","Nombre del Donante:","Introduzca nombre del donante","100","col-md-push-0",""),
            array("nomBen","Nombre del Beneficiario:","Introduzca nombre del beneficiario","100","",""),
            array("numConac","Número del Contrato o Acta:","Introduzca el nombre contrato o acta","30","",""),
            array("nomRegn","Nombre del Registro o Notaría:","Introduzca nombre del registro o notaría","100","",""),
            array("tomo","Tomo:","Introduzca el tomo del registro","20","",""),
            array("folio","Folio:","Introduzca el N° de folio","6","","return soloNum(event)"),
            );

        $selectT24= array(
            array("codAdq","Código de Adquisición:","col-md-push-4"),
            );

        $dateT24=array(
            array("feConac","Fecha del Contrato o Acta:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
            );

        $date2T24=array(
            array("feReg","Fecha de Registro:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
            );

        return view('AnexosT.visDonacion', compact('infoSelect','arrayT24','selectT24','dateT24','date2T24','lastCod'));
    }

    public function store(Request $request)
    {

        $form_t24=new modeloDonacion();
        $form_t24->codAdq = $request->codAdq;

        if($form_t24->codOt2_4 =$request->codOt2_4 == ''){
          $form_t24->codOt2_4 = 'E-1';
          }else{
          $form_t24->codOt2_4 = $request->codOt2_4;  
        }
        
        if($form_t24->nomDona =$request->nomDona == ''){
          $form_t24->nomDona = '1';
          }else{
          $form_t24->nomDona = $request->nomDona;  
        }

        if($form_t24->nomBen = $request->nomBen == ''){
          $form_t24->nomBen = '1';
          }else{
          $form_t24->nomBen = $request->nomBen;  
        }

        if($form_t24->numConac = $request->numConac == ''){
          $form_t24->numConac = '0';
          }else{
          $form_t24->numConac = $request->numConac;  
        }

        if($form_t24->nomRegn = $request->nomRegn == ''){
          $form_t24->nomRegn = '1';
          }else{
          $form_t24->nomRegn = $request->nomRegn;  
        }

        if($form_t24->tomo = $request->tomo == ''){
          $form_t24->tomo = '1';
          }else{
          $form_t24->tomo = $request->tomo;  
        }

        if($form_t24->folio = $request->folio == ''){
          $form_t24->folio = '0';
          }else{
          $form_t24->folio = $request->folio;  
        }

        if($form_t24->feConac = $request->feConac == ''){
          $form_t24->feConac = '11111111';
          }else{
          $form_t24->feConac = $request->feConac;  
        }

        if($form_t24->feReg = $request->feReg == ''){
          $form_t24->feReg = '11111111';
          }else{
          $form_t24->feReg = $request->feReg;  
        }

        if($form_t24->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Donación';
          $bit->save();

          }

          return back()->with('msj', 'Datos Registrados Exitosamente');
             
        }
  


    public function edit($id)
    {
        $form_t24 = modeloDonacion::find($id);
        $infoSelect = sel_donacion::all();

       return view('layouts.ModificarAnexosT.modificarDonacion',compact('form_t24','infoSelect'));
    }

    public function update(Request $request, $id)
    {
        $form_t24= modeloDonacion::find($id);
        $form_t24->codOt2_4 = $request->codOt2_4;
        $form_t24->codAdq = $request->codAdq;


        if($form_t24->nomDona =$request->nomDona == ''){
          $form_t24->nomDona = '1';
          }else{
          $form_t24->nomDona = $request->nomDona;  
        }

        if($form_t24->nomBen = $request->nomBen == ''){
          $form_t24->nomBen = '1';
          }else{
          $form_t24->nomBen = $request->nomBen;  
        }

        if($form_t24->numConac = $request->numConac == ''){
          $form_t24->numConac = '0';
          }else{
          $form_t24->numConac = $request->numConac;  
        }

        if($form_t24->nomRegn = $request->nomRegn == ''){
          $form_t24->nomRegn = '1';
          }else{
          $form_t24->nomRegn = $request->nomRegn;  
        }

        if($form_t24->tomo = $request->tomo == ''){
          $form_t24->tomo = '1';
          }else{
          $form_t24->tomo = $request->tomo;  
        }

        if($form_t24->folio = $request->folio == ''){
          $form_t24->folio = '0';
          }else{
          $form_t24->folio = $request->folio;  
        }

        if($form_t24->feConac = $request->feConac == ''){
          $form_t24->feConac = '11111111';
          }else{
          $form_t24->feConac = $request->feConac;  
        }

        if($form_t24->feReg = $request->feReg == ''){
          $form_t24->feReg = '11111111';
          }else{
          $form_t24->feReg = $request->feReg;  
        }

        if($form_t24->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Donación';
          $bit->save();

            return back()->with('msj', 'Datos Registrados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }


    public function exportDonacion()
    {
       \Excel::create('donación', function($excel) {
    
      $donacion = modelodonacion::all();
      $relacion = sel_donacion::all();
      
      $excel->sheet('donacion', function($sheet) use($donacion) {
 
      /*========CABECERA DE LA FILA N° 1========*/


      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN', 'CÓDIGO DE LA FORMA DE ADQUISICIÓN', 'NOMBRE DEL DONANTE', 'NOMBRE DEL BENEFICIARIO', 'NÚMERO DEL CONTRATO O ACTA', 'FECHA DEL CONTRATO O ACTA', 'NOMBRE DEL REGISTRO O NOTARÍA', 'TOMO', 'FOLIO', 'FECHA DEL REGISTRO'

      ]);

      $sheet->setWidth([
                    'A'     =>  25,
                    'B'     =>  35,
                    'C'     =>  25,
                    'D'     =>  25,
                    'E'     =>  35,
                    'F'     =>  35,
                    'G'     =>  35,
                    'H'     =>  35,
                    'I'     =>  35,
                    'J'     =>  35,
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:J6000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($donacion  as $index => $donacion) {
            
            /*========Código de Origen============*/
            if ($donacion->codOt2_4 == '') {
              $codOt2_4 = 'E-1';
              
            }else{
              $codOt2_4=$donacion->codOt2_4;
            }
            
            /*========Código de Adquisición=======*/

            if($donacion->codAdq == '1')
            {
              $codAdq = '4';

            }

            /*========Nombre del Donante======*/

            if($donacion->nomDona == '1')
            {
              $nomDona = 'xxx';

            }else{

              $nomDona=$donacion->nomDona;
            }

            /*========Número del Beneficiario=======*/

            if($donacion->nomBen == '1')
            {
              $nomBen = 'xxx';

            }else{

              $nomBen=$donacion->nomBen;
            }

            /*========Número del Contrato o Acta=======*/

            if($donacion->nomBen == '1')
            {
              $nomBen = 'noaplica';

            }else{

              $nomBen=$donacion->nomBen;
            }

            /*========Número del Contrato o Acta=======*/

            if($donacion->numConac == '0')
            {
              $numConac = 'xxx';

            }else{

              $numConac=$donacion->numConac;
            }

            /*========Fecha del Contrato=======*/

            if($donacion->feConac == '1111-11-11')
            {
              $feConac = '11111111';

            }else{
        
            $feConac = $donacion->feConac;
            $feConac = date("dmY", strtotime($feConac));
       
            }

            /*========Nombre del Registro o Notaría=======*/

            if($donacion->nomRegn == '1')
            {
              $nomRegn = 'xxx';

            }else{

              $nomRegn=$donacion->nomRegn;
            }

            /*========Tomo=======*/

            if($donacion->tomo == '1')
            {
              $tomo = 'xxx';

            }else{

              $tomo=$donacion->tomo;
            }

             /*========Folio=======*/

            if($donacion->folio == '0')
            {
              $folio = '99';

            }else{

              $folio=$donacion->folio;
            }


            

            /*========Fecha del Registro=======*/

            if($donacion->feReg == '1111-11-11')
            {
              $feReg = '11111111';

            }else{

              $feReg = $donacion->feReg;
              $feReg = date("dmY", strtotime($feReg));
            }


          $sheet->row($index+2, [
              $codOt2_4, 
              $codAdq, 
              $nomDona, 
              $nomBen, 
              $numConac, 
              $feConac, 
              $nomRegn, 
              $tomo, 
              $folio, 
              $feReg, 
          
          ]);

        }

      });
   
      })->export('xlsx');
    }

}
