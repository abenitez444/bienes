<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloDacion;
use App\modeloBitacora;


class con_dacionVer extends Controller
{
      public function index(){
      	
      	$verT23= modeloDacion::all();

      	return view('RegistrosT.regDacion', compact('verT23'));

    }

    public function selectId($id){

      	$seleccion = modeloDacion::find($id);
          $fechaFini = $seleccion->feFin;
          $fechaFini = date("d/m/Y", strtotime($fechaFini));
          $fechaReg = $seleccion->feReg;
          $fechaReg = date("d/m/Y", strtotime($fechaReg));

       return view('MuestraAnexosT.muestraDacion',compact('seleccion','fechaFini','fechaReg'));
    }

    public function anularDacion($id)
    {
        $seleccion= modeloDacion::find($id);
        
        
       if($seleccion->delete()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 3;
          $bit->referencia = 'Dación en Pago';
          $bit->save();

         return redirect('regDacion')->with('msj', 'Registro Eliminado Exitosamente');
         } else {
         return redirect()->with('errormsj', 'Los datos no se guardaron');
       }
    }
}
