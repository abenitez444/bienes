<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\freeipa\Freeipa;
use Illuminate\Support\Facades\Crypt;
use App\User;

class controladorLogin extends Controller
{
	public function login(Request $request) 
	{

		$user = request('user');
		$pass  = request('pass');
		$clave = Crypt::encrypt($pass);
		/* REFERENCIA A LA TABLA DE BD DEL FREEIPA */
		$auth = Freeipa::Login($user, $clave);

		if ($auth) {

				/* REFERENCIA A LA TABLA DE BD DE USERS EN POSTGRES (BD LOCALHOST) */
				$permiso = User::where('name', $user)->get();

			if (sizeof($permiso) > 0) {

				$nombre  = $auth->user()->findBy('uid', $user);
				$_SESSION['intervalo']  = 1; //tiempo de vida de la sesión en minutos
				$_SESSION['inicio'] = time();
				$_SESSION['nombre'] = $nombre[0]->cn[0];
				$_SESSION['user'] = $user;
				$_SESSION['rol'] = $permiso[0]->rol;
				$_SESSION['id'] = $permiso[0]->id;
				
				return redirect('/home');

			} else {
				
				return back()->with('msj', '¡No tienes permiso para ingresar a este sistema!');
			}	

			} else {

		 		return back()->with('errormsj', 'Usuario o contraseña incorrectos');

			}	

	} 

	public function logout()
		{
				session_destroy(); 
				return redirect('/');
		}


	public function recurrent($str1,$str2,$index1,$index2,$var)
	{
		$index1++;
		$index2++;
		$arr = "";

		if($index1 < sizeof($str1)){
			if($index2 < sizeof($str2)){
				
				if($str1[$index1] == $str2[$index2]){
					
					$cont = $var.$str1[$index1];
					$this->recurrent($str1,$str2,$index1,$index2,$cont);

				}else{

					return $this->var = $var;
					
				}	
			
			}else{	
				
				return $this->var = $var;
				
			}
		}else{
	
			return $this->var = $var;
			
		}
		
	}

	public function prueba()
		{
			$str1 = "Doorraas";
			$str2 = "Kanrrgaroo";
			$arr = [];
			$arr1 = str_split(strtolower($str1));
			$arr2 = str_split(strtolower($str2));

			for ($index1=-1; $index1 < sizeof($arr1); $index1++) { 
				$var = "";

				for ($index2=-1; $index2 < sizeof($arr2); $index2++) { 

					$response = new controladorLogin();
					$response->recurrent($arr1,$arr2,$index1,$index2,$var);
					
					if (strlen($response->var) > 1) {
						array_push($arr, $response->var);
					}
					

				}
			}

			print_r($arr);

		}



	
}