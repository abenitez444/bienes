<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS6;
use App\modeloBitacora;
use Carbon\Carbon;

class con_s6Ver extends Controller
{
       public function index(){
        
        $verS6= modeloS6::all();

        return view('RegistrosS.regUbicacion', compact('verS4','verS5','verS6'));

    }

   	 public function selectId($id)
   	 {
   	 	
   	 	$seleccion = modeloS6::find($id);

   		return view('MuestraAnexosS.muestraUbicacion',compact('seleccion'));
   	 }


      public function anularUbicacion($id)
      {

      $seleccion= modeloS6::find($id);
          
         if($seleccion->delete()){
          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 3;
          $bit->referencia = 'Datos de Ubicación de las Unidades Administrativas del Órgano o Ente';
          $bit->save();

           return redirect('regUbicacion')->with('msj', 'Registro Eliminado Exitosamente');
           } else {
           return redirect()->with('errormsj', 'Los datos no se guardaron');
         }
      }
}
