<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloPermuta;
use App\sel_permuta;
use App\modeloBitacora;

class controladorPermuta extends Controller
{
    public function index()
    {
        $infoSelect = sel_permuta::all();

        $lastCod = modeloPermuta::select('codOt2_6')->get()->last();

        $arrayT26 = array(
            array("codOt2_6","Código de Origen:","Introduzca número consecutivo. Ej: G-2; G-3;","12","col-md-pull-4",""),
            array("nomCope","Nombre del Copermutante:","Introduzca nombre del copermutante","100","col-md-push-0",""),
            array("nomBen","Nombre del Beneficiario:","Introduzca nombre del beneficiario","100","",""),
            array("nomLic","Nombre de la Licitación:","Introduzca nombre de la licitación","255","",""),
            array("numLic","Número de la Licitación:","Introduzca el N° licitación","30","",""),
            array("numCon","Número del Contrato:","Introduzca el N° de contrato","30","",""),
            array("nomRegn","Nombre del Registro o Notaría:","Introduzca nombre del registro o notaría","100","",""),
            array("tomo","Tomo:","Introduzca el tomo del registro","20","",""),
            array("folio","Folio:","Introduzca el N° de folio","6","","return soloNum(event)"),
            );

        $selectT26 = array(
        array("codAdq","Código de Adquisición:","col-md-push-4"),
        );

        $dateT26= array(
        array("feLic","Fecha de Licitación:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        $date2T26= array(
        array("feCon","Fecha del Contrato:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        $date3T26= array(
        array("feReg","Fecha de Registro:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        return view('AnexosT.visPermuta', compact('infoSelect','arrayT26','selectT26','dateT26','date2T26','date3T26','lastCod'));
    }

    public function store(Request $request)
    {

        $form_t26= new modeloPermuta();
        $form_t26->codAdq = $request->codAdq;

        if($form_t26->codOt2_6 =$request->codOt2_6 == ''){
          $form_t26->codOt2_6 = 'G-1';
          }else{
          $form_t26->codOt2_6 = $request->codOt2_6;  
        }

        if($form_t26->nomCope =$request->nomCope == ''){
          $form_t26->nomCope = '1';
          }else{
          $form_t26->nomCope = $request->nomCope;  
        }

        if($form_t26->nomBen = $request->nomBen == ''){
          $form_t26->nomBen = '1';
          }else{
          $form_t26->nomBen = $request->nomBen;  
        }

        if($form_t26->nomLic = $request->nomLic == ''){
          $form_t26->nomLic = '1';
          }else{
          $form_t26->nomLic = $request->nomLic;  
        }

        if($form_t26->numLic = $request->numLic == ''){
          $form_t26->numLic = '0';
          }else{
          $form_t26->numLic = $request->numLic;  
        }

        if($form_t26->numCon = $request->numCon == ''){
          $form_t26->numCon = '0';
          }else{
          $form_t26->numCon = $request->numCon;  
        }

        if($form_t26->nomRegn = $request->nomRegn == ''){
          $form_t26->nomRegn = '1';
          }else{
          $form_t26->nomRegn = $request->nomRegn;  
        }

        if($form_t26->tomo = $request->tomo == ''){
          $form_t26->tomo = '1';
          }else{
          $form_t26->tomo = $request->tomo;  
        }

        if($form_t26->folio = $request->folio == ''){
          $form_t26->folio = '0';
          }else{
          $form_t26->folio = $request->folio;  
        }

        if($form_t26->feLic = $request->feLic == ''){
          $form_t26->feLic = '11111111';
          }else{
          $form_t26->feLic = $request->feLic;  
        }

        if($form_t26->feCon = $request->feCon == ''){
          $form_t26->feCon = '11111111';
          }else{
          $form_t26->feCon = $request->feCon;  
        }

        if($form_t26->feReg = $request->feReg == ''){
          $form_t26->feReg = '11111111';
          }else{
          $form_t26->feReg = $request->feReg;  
        }

        if($form_t26->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Permuta';
          $bit->save();

          }

          return back()->with('msj', 'Datos Registrados Exitosamente');
            
    }

 
    public function edit($id)
    {
        $form_t26 = modeloPermuta::find($id);
        $infoSelect = sel_permuta::all();

        return view('layouts.ModificarAnexosT.modificarPermuta', compact('form_t26','infoSelect'));
    }

 
    public function update(Request $request, $id)
    {
        $form_t26= modeloPermuta::find($id);
        $form_t26->codOt2_6 = $request->codOt2_6;
        $form_t26->codAdq = $request->codAdq;

        if($form_t26->nomCope =$request->nomCope == ''){
          $form_t26->nomCope = '1';
          }else{
          $form_t26->nomCope = $request->nomCope;  
        }

        if($form_t26->nomBen = $request->nomBen == ''){
          $form_t26->nomBen = '1';
          }else{
          $form_t26->nomBen = $request->nomBen;  
        }

        if($form_t26->nomLic = $request->nomLic == ''){
          $form_t26->nomLic = '1';
          }else{
          $form_t26->nomLic = $request->nomLic;  
        }

        if($form_t26->numLic = $request->numLic == ''){
          $form_t26->numLic = '0';
          }else{
          $form_t26->numLic = $request->numLic;  
        }

        if($form_t26->numCon = $request->numCon == ''){
          $form_t26->numCon = '0';
          }else{
          $form_t26->numCon = $request->numCon;  
        }

        if($form_t26->nomRegn = $request->nomRegn == ''){
          $form_t26->nomRegn = '1';
          }else{
          $form_t26->nomRegn = $request->nomRegn;  
        }

        if($form_t26->tomo = $request->tomo == ''){
          $form_t26->tomo = '1';
          }else{
          $form_t26->tomo = $request->tomo;  
        }

        if($form_t26->folio = $request->folio == ''){
          $form_t26->folio = '0';
          }else{
          $form_t26->folio = $request->folio;  
        }

        if($form_t26->feLic = $request->feLic == ''){
          $form_t26->feLic = '11111111';
          }else{
          $form_t26->feLic = $request->feLic;  
        }

        if($form_t26->feCon = $request->feCon == ''){
          $form_t26->feCon = '11111111';
          }else{
          $form_t26->feCon = $request->feCon;  
        }

        if($form_t26->feReg = $request->feReg == ''){
          $form_t26->feReg = '11111111';
          }else{
          $form_t26->feReg = $request->feReg;  
        }

        if($form_t26->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Permuta';
          $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }

    public function exportPermuta()
    {
      \Excel::create('permuta', function($excel) {
    
      $permuta = modelopermuta::all();
      $relacion = sel_permuta::all();
      
      $excel->sheet('permuta', function($sheet) use($permuta) {
 
      /*========CABECERA DE LA FILA N° 1========*/


      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN', 'CÓDIGO DE LA FORMA DE ADQUISICIÓN', 'NOMBRE DEL COPERMUTANTE', 'NOMBRE DEL BENEFICIARIO', 'NOMBRE DE LA LICITACIÓN','NÚMERO DE LA LICITACIÓN', 'FECHA DE LICITACIÓN', 'NÚMERO DEL CONTRATO', 'FECHA DEL CONTRATO', 'NOMBRE DEL REGISTRO O NOTARÍA','TOMO','FOLIO', 'FECHA DE REGISTRO'

      ]);

      $sheet->setWidth([
                    'A'     =>  20,
                    'B'     =>  35,
                    'C'     =>  30,
                    'D'     =>  25,
                    'E'     =>  25,
                    'F'     =>  25,
                    'G'     =>  25,
                    'H'     =>  35,
                    'I'     =>  30,
                    'J'     =>  35,
                    'K'     =>  20,
                    'L'     =>  25,
                    'M'     =>  25,
                  
                ]);

      $sheet->setHeight(1, 35);
                $sheet->cell('A1:M6000',function($cell) 
                {
                    $cell->setAlignment('center');    
                                       
                });

      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
      foreach($permuta  as $index => $permuta) {
            
            /*========Código del origen======*/

            if($permuta->codOt2_6 == '1')
            {
              $codOt2_6 = 'G-1';

            }else{

              $codOt2_6=$permuta->codOt2_6;
            }
          
            /*========Código de Adquisición=======*/

            if($permuta->codAdq == '1')
            {
              $codAdq = '2';

            }

            /*========Nombre del Copermutante======*/

            if($permuta->nomCope == '1')
            {
              $nomCope = 'xxx';

            }else{

              $nomCope=$permuta->nomCope;
            }

            /*========Nombre del Beneficiario=======*/

            if($permuta->nomBen == '1')
            {
              $nomBen = 'xxx';

            }else{

              $nomBen=$permuta->nomBen;
            }

            /*========Nombre de la Licitación=======*/

            if($permuta->nomLic == '1')
            {
              $nomLic = 'xxx';

            }else{

              $nomLic=$permuta->nomLic;
            }

            /*========Número de la Licitación=======*/

            if($permuta->numLic == '0')
            {
              $numLic = 'xxx';

            }else{

              $numLic=$permuta->numLic;
            }

            /*========Número del Contrato=======*/

            if($permuta->numCon == '0')
            {
              $numCon = 'xxx';

            }else{

              $numCon=$permuta->numCon;
            }

            /*========Nombre del Registro o Notaría=======*/

            if($permuta->nomRegn == '1')
            {
              $nomRegn = 'xxx';

            }else{

              $nomRegn=$permuta->nomRegn;
            }


            /*========Tomo=======*/

            if($permuta->tomo == '1')
            {
              $tomo = 'xxx';

            }else{

              $tomo=$permuta->tomo;
            }

             /*========Folio=======*/

            if($permuta->folio == '0')
            {
              $folio = '99';

            }else{

              $folio=$permuta->folio;
            }


            /*========Fecha de Licitación=======*/

            if($permuta->feLic == '1111-11-11')
            {
              $feLic = '11111111';

            }else{
                
              $feLic = $permuta->feLic;
              $feLic = date("dmY", strtotime($feLic));
              
            }

            /*========Fecha del Contrato=======*/

            if($permuta->feCon == '1111-11-11')
            {
              $feCon = '11111111';

            }else{

              $feCon = $permuta->feCon;
              $feCon = date("dmY", strtotime($feCon));
            
            }

            /*========Fecha de Registro=======*/

            if($permuta->feReg == '1111-11-11')
            {
              $feReg = '11111111';

            }else{

              $feReg = $permuta->feReg;
              $feReg = date("dmY", strtotime($feReg));
         
            }


          $sheet->row($index+2, [
              
              $codOt2_6, 
              $codAdq, 
              $nomCope, 
              $nomBen, 
              $nomLic, 
              $numLic,
              $feLic,  
              $numCon, 
              $feCon, 
              $nomRegn, 
              $tomo, 
              $folio, 
              $feReg, 
          
          ]);

        }

      });
   
      })->export('xlsx');
    }
}
