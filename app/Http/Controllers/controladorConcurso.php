<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloConcurso;
use App\modeloProveedores;
use App\sel_concurso;
use App\modeloModalidades;
use App\modeloBitacora;

class controladorConcurso extends Controller
{
    public function index()
    {
      $conta = modeloConcurso::all();

      $infoSelect=sel_concurso::all();
      $infoProvee=modeloProveedores::all();
      $lastCod = modeloConcurso::select('codOrigen')->get()->last();

 
        $codigoOrigen=array(
          array("codOrigen","Código de Origen:","Introduzca número consecutivo. Ej: A-2; A-3;","12"),
        );
        
        $codigoAdq=array(
          array("codAdquisicion","Código de la Forma de adquisición:","2"),
        );

        $nombreConcu= array(
          array("nomConcurso","Nombre del Concurso:","¡Si se desconoce, deje el campo en blanco!","255"),
        );

        $numeroConcu= array(
          array("numConcurso","Número del Concurso:","¡Si se desconoce, deje el campo en blanco!","30"),
        );

        $fechaConcu=array(
          array("feConcurso","Fecha de Concurso:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        $codProveedor = array(
          array("codProvee","Código de Proveedor:","2"),
        );

        $numContra = array(
            array("numContrato","Número del Contrato:","Introduzca número del contrato","30"),
          );
        
        $fechaContra=array(
          array("feContrato","Fecha del Contrato:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        $numEntrega = array(
           array("numNotaEntre","Número de la Nota de Entrega:","Introduzca el número de la nota de entrega","30"),
        );

        $fechaEntrega=array(
          array("feNotaEntre","Fecha de la Nota de Entrega:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        $numeroFactura= array(
          array("numFactura","Número de la Factura:","Introduzca el número de factura","30"),
        );

        $fechaFactura= array(
           array("feFactura","Fecha de la Factura:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
        );

        return view('AnexosT.visConcurso', compact('conta','infoSelect','infoProvee','codigoOrigen','codigoAdq','nombreConcu','numeroConcu','fechaConcu','codProveedor','numContra','fechaContra','numEntrega','fechaEntrega','numeroFactura','fechaFactura','lastCod'));
    }

    public function store(Request $request)
    {

        $form_t2= new modeloConcurso();
        $form_t2->codAdquisicion = $request->codAdquisicion; 

#Si el campo en el fomulario se deja en blanco cumple la condición
        
        $consulta = modeloModalidades::find(1);
        $cont = $consulta->cont;


        if($form_t2->codOrigen = $request->codOrigen == ''){
            $form_t2->codOrigen = 'A-1';

        }else{
            $form_t2->codOrigen = $request->codOrigen;    
        }  

        if ($form_t2->nomConcurso = $request->nomConcurso == ''){
        $form_t2->nomConcurso = 'xxx';

            }else{
            $form_t2->nomConcurso = $request->nomConcurso;  
        }

        if ($form_t2->numConcurso = $request->numConcurso == ''){
        $form_t2->numConcurso = 'xxx';    
        
            }else{
            $form_t2->numConcurso = $request->numConcurso;  
        }

        if ($form_t2->feConcurso = $request->feConcurso == '') {
        $form_t2->feConcurso = '11111111';
      
            }else{
             $form_t2->feConcurso = $request->feConcurso;
            }

        $form_t2->codProvee = $request->codProvee;

        if ($form_t2->numContrato = $request->numContrato == ''){
        $form_t2->numContrato = 'xxx';

            }else{
            $form_t2->numContrato = $request->numContrato;
        }

        if ($form_t2->feContrato = $request->feContrato == '') {
        $form_t2->feContrato = '11111111';
      
            }else{
             $form_t2->feContrato = $request->feContrato;
            }

        if($form_t2->numNotaEntre = $request->numNotaEntre == ''){
        $form_t2->numNotaEntre = 'xxx';

            }else{
            $form_t2->numNotaEntre = $request->numNotaEntre;
        }

        if ($form_t2->feNotaEntre = $request->feNotaEntre == '') {
        $form_t2->feNotaEntre = '11111111';
      
             }else{
             $form_t2->feNotaEntre = $request->feNotaEntre;
             }

        if ($form_t2->numFactura = $request->numFactura == ''){
        $form_t2->numFactura = 'xxx';

             }else{
             $form_t2->numFactura = $request->numFactura;
        }

        if ($form_t2->feFactura = $request->feFactura == '') {
        $form_t2->feFactura = '11111111';
      
             }else{
             $form_t2->feFactura = $request->feFactura;
             }

        if($form_t2->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Compra por Concurso';
          $bit->save();

        }

        return back()->with('msj', 'Datos Registrados Exitosamente');
            

    }

    public function edit($id)
    {
        $form_t2 = modeloConcurso::find($id);
        $infoSelect = sel_concurso::all();
        $infoProvee = modeloProveedores::all();

        return view('layouts.ModificarAnexosT.modificarConcurso', compact('form_t2','infoSelect','infoProvee'));
    }

    public function update(Request $request, $id)
      {
        $form_t2= modeloConcurso::find($id);
        $form_t2->codOrigen = $request->codOrigen; 
        $form_t2->codAdquisicion = $request->codAdquisicion; 
#Si el campo en el fomulario se deja en blanco cumple la condición

    
        if ($form_t2->nomConcurso = $request->nomConcurso == ''){
        $form_t2->nomConcurso = 'xxx';

            }else{
            $form_t2->nomConcurso = $request->nomConcurso;  
        }

        if ($form_t2->numConcurso = $request->numConcurso == ''){
        $form_t2->numConcurso = 'xxx';    
        
            }else{
            $form_t2->numConcurso = $request->numConcurso;  
        }

        if ($form_t2->feConcurso = $request->feConcurso == '') {
        $form_t2->feConcurso = '11111111';
      
            }else{
             $form_t2->feConcurso = $request->feConcurso;
            }

        $form_t2->codProvee = $request->codProvee;
    
        if ($form_t2->numContrato = $request->numContrato == ''){
        $form_t2->numContrato = 'xxx';

            }else{
            $form_t2->numContrato = $request->numContrato;
        }

        if ($form_t2->feContrato = $request->feContrato == '') {
        $form_t2->feContrato = '11111111';
      
            }else{
             $form_t2->feContrato = $request->feContrato;
            }

        if($form_t2->numNotaEntre = $request->numNotaEntre == ''){
        $form_t2->numNotaEntre = 'xxx';

            }else{
            $form_t2->numNotaEntre = $request->numNotaEntre;
        }

        if ($form_t2->feNotaEntre = $request->feNotaEntre == '') {
        $form_t2->feNotaEntre = '11111111';
      
             }else{
             $form_t2->feNotaEntre = $request->feNotaEntre;
             }

        if ($form_t2->numFactura = $request->numFactura == ''){
        $form_t2->numFactura = 'xxx';

             }else{
             $form_t2->numFactura = $request->numFactura;
        }

        if ($form_t2->feFactura = $request->feFactura == '') {
        $form_t2->feFactura = '11111111';
      
             }else{
             $form_t2->feFactura = $request->feFactura;
             }
    
        if($form_t2->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Compra por Concurso';
          $bit->save();

            return back()->with('msj', 'Datos modificados exitosamente');
        } else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }

      }

       /*===========Exportar tabla concurso a archivo .CSV===========*/

      public function exportConcurso(){

      \Excel::create('concurso', function($excel) {
    
      $concurso = modeloConcurso::all();
      $relacion = sel_concurso::all();
      
      
      $excel->sheet('concurso', function($sheet) use($concurso) {
 
      /*========CABECERA DE LA FILA N° 1========*/

     
      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN', 'CÓDIGO DE LA FORMA DE ADQUISICIÓN ', 'NOMBRE DEL CONCURSO', 'NÚMERO DEL CONCURSO ', 'FECHA DEL CONCURSO', 'CÓDIGO DEL PROVEEDOR','NÚMERO DEL CONTRATO', 'FECHA DEL CONTRATO', 'NÚMERO DE NOTA DE ENTREGA', 'FECHA DE LA NOTA DE ENTREGA', 'NÚMERO DE LA FACTURA', 'FECHA DE LA FACTURA'


      ]);

      $sheet->setWidth([
                    'A'     =>  30,
                    'B'     =>  35,
                    'C'     =>  25,
                    'D'     =>  25,
                    'E'     =>  25,
                    'F'     =>  25,
                    'G'     =>  25,
                    'H'     =>  25,
                    'I'     =>  30,
                    'J'     =>  30,
                    'K'     =>  23,
                    'L'     =>  23
                ]);

                $sheet->setHeight(1, 35);
                $sheet->cell('A1:L6000',function($cell) 
                {
                    $cell->setAlignment('center');                
                    
                });
                
      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
     foreach($concurso  as $index => $concurso) {
            
           /*========CÓDIGO DE ADQUISICIÓN=======*/
          
            if($concurso->codAdquisicion == '1')
            {
              $codAdquisicion = '8';

            }else{

              $codAdquisicion= '9';
            }

            /*====Fecha Concurso====*/
            
            if($concurso->feConcurso == '1111-11-11')
            {
              $feConcurso = '11111111';

            }else{
              $feConcurso = $concurso->feConcurso;
              $feConcurso = date("dmY", strtotime($feConcurso));
            }

            /*====Fecha Contrato====*/

            if($concurso->feContrato == '1111-11-11')
            {
              $feContrato = '11111111';

            }else{
              $feContrato = $concurso->feContrato;
              $feContrato = date("dmY", strtotime($feContrato));
            }

            /*====Fecha de la Nota de Entrega====*/
            
            if($concurso->feNotaEntre == '1111-11-11')
            {
              $feNotaEntre = '11111111';

            }else{
              $feNotaEntre = $concurso->feNotaEntre;
              $feNotaEntre = date("dmY", strtotime($feNotaEntre));
            }

            /*====Fecha de la Factura====*/
            
            if($concurso->feFactura == '1111-11-11')
            {
              $feFactura = '11111111';

            }else{
              $feFactura = $concurso->feFactura;
              $feFactura = date("dmY", strtotime($feFactura));
            }

          $sheet->row($index+2, [
              $concurso->codOrigen, 
              $codAdquisicion, 
              $concurso->nomConcurso, 
              $concurso->numConcurso, 
              $feConcurso, 
              $concurso->selectProveeConcu->codProvee, 
              $concurso->numContrato, 
              $feContrato, 
              $concurso->numNotaEntre, 
              $feNotaEntre, 
              $concurso->numFactura, 
              $feFactura, 
                    

          ]);

        }


      });
   
      })->export('xlsx');
   }
 
}
