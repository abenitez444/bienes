<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloS1;
use App\modeloBitacora;


class controladorS1 extends Controller
{
    public function index()
    {
    	$lastCod = modeloS1::select('codSigecof')->get()->last();

    	$codSige = array(
    		array("codSigecof","Código Sigecof:","Introduzca el código sigecof","17"),
    		);

    	$siglas = array(
    		array("siglas","Siglas:","Introduzca las siglas","15"),
    		);

    	$grupo = array(
          array("grupo","","1","col-md-pull-0","J","V","G"),
          	);

    	$rif= array(
          array("rifProvee","Introduzca el número de RIF:","Introduzca el número de RIF","20"),
          	);

    	$razonSo = array(
    		array("razonSocial","Razón Social:","Descripción de la razón social...","255","4"),
    		);

    	$telefono = array(
    	 	array("telfEnte","Teléfono:","Introduzca el número de teléfono","20","","telefono")
    	 	);

    	$direcWeb = array(
    	 	array("direcWeb","Dirección Web:","Ej: www.vicepresidencia.gob.ve","100")
    	 	);

    	$correEnte = array(
    	 	array("correEnte","Correo Electrónico:","Introduzca el correo electrónico","200"),
    	 	);

    	$fechaGaceta = array(
    		array("feGaceta","Fecha de Gaceta:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
    		);

    	$numeroGaceta = array(
    		array("numGaceta","Número de Gaceta:","Introduzca el número de gaceta","10"),
    		);

    	return view('AnexosS.visBasicos', compact('lastCod','codSige','siglas','grupo','rif','razonSo','telefono','direcWeb','correEnte','fechaGaceta','numeroGaceta'));
    }

    public function store(Request $request)
    {
    	$form_s1 = new modeloS1();
    	$form_s1->codSigecof = $request->codSigecof;
    	$form_s1->grupo = $request->grupo;
    	$form_s1->rifProvee = $request->rifProvee;

    	if($form_s1->siglas = $request->siglas == '')
    	{
    		$form_s1->siglas == '1';
    	}else{
    		$form_s1->siglas = $request->siglas;
    	}

    	if($form_s1->razonSocial = $request->razonSocial == '')
    	{
    		$form_s1->razonSocial = '1';
    	}else{
    		$form_s1->razonSocial = $request->razonSocial;
    	}

    	if($form_s1->telfEnte = $request->telfEnte == '')
    	{
    		$form_s1->telfEnte = '0';
    	}else{
    		$form_s1->telfEnte = $request->telfEnte;
    	}

    	if($form_s1->direcWeb = $request->direcWeb == '')
    	{
    		$form_s1->direcWeb = '1';
    	}else{
    		$form_s1->direcWeb = $request->direcWeb;
    	}

    	if($form_s1->correEnte = $request->correEnte == '')
    	{
    		$form_s1->correEnte = '1';
    	}else{
    		$form_s1->correEnte = $request->correEnte;
    	}

    	if($form_s1->feGaceta = $request->feGaceta == '')
    	{
    		$form_s1->feGaceta = '11111111';
    	}else{
    		$form_s1->feGaceta = $request->feGaceta;
    	}

    	if($form_s1->numGaceta = $request->numGaceta == '')
    	{
    		$form_s1->numGaceta = '1';
    	}else{
    		$form_s1->numGaceta = $request->numGaceta;
    	}

    	if($form_s1->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Datos Básicos';
          $bit->save();

         

    	return back()->with('msj', 'Datos Registrados Exitosamente');
    	}else {
        return back()->with('errormsj', 'Los datos no se guardaron');
    	}
    }

    public function edit($id)
    {
        $form_s1 = modeloS1::find($id);
        $lastCod = modeloS1::select('codSigecof')->get()->last();
        
        return view('layouts.ModificarAnexosS.modificarBasicos', compact('form_s1','lastCod'));
    }

    public function update(Request $request, $id)
      {
        $form_s1 = modeloS1::find($id);
        $form_s1->codSigecof = $request->codSigecof;
        $form_s1->grupo = $request->grupo;
        $form_s1->rifProvee = $request->rifProvee;
        
        if ($form_s1->siglas = $request->siglas == '') {
        $form_s1->siglas = '1';
      
        }else{
          $form_s1->siglas = $request->siglas;
        }

        if ($form_s1->razonSocial = $request->razonSocial == '') {
        $form_s1->razonSocial = '1';
      
        }else{
          $form_s1->razonSocial = $request->razonSocial;
        }

        if ($form_s1->telfEnte = $request->telfEnte == '') {
        $form_s1->telfEnte = '0';
      
        }else{
          $form_s1->telfEnte = $request->telfEnte;
        }

        if ($form_s1->direcWeb = $request->direcWeb == '') {
        $form_s1->direcWeb = '1';
      
        }else{
          $form_s1->direcWeb = $request->direcWeb;
        }

        if ($form_s1->correEnte = $request->correEnte == '') {
        $form_s1->correEnte = '1';
      
        }else{
          $form_s1->correEnte = $request->correEnte;
        }

        if ($form_s1->feGaceta = $request->feGaceta == '') {
        $form_s1->feGaceta = '1111-11-11';
      
        }else{
          $form_s1->feGaceta = $request->feGaceta;
        }

        if ($form_s1->numGaceta = $request->numGaceta == '') {
        $form_s1->numGaceta = '1';
      
        }else{
          $form_s1->numGaceta = $request->numGaceta;
        }


        $form_s1->save();

        if($form_s1->save()){
          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 2;
          $bit->referencia = 'Datos Básicos de los Inmuebles';
          $bit->save();
    
        
            return back()->with('msj', 'Datos modificados exitosamente');
        } else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }

      }

    public function exporBasicos()
    {
            \Excel::create('basicos', function($excel) {
        
          $basicos = modeloS1::all();
          
          $excel->sheet('basicos', function($sheet) use($basicos) {
     
          /*========CABECERA DE LA FILA N° 1========*/


          $sheet->row(1, [
              'CÓDIGO SIGECOF', 'SIGLAS ', 'RIF ', 'RAZÓN SOCIAL', 'TELÉFONO', 'DIRECCIÓN WEB', 'CORREO ELECTROŃICO', 'FECHA GACETA', 'NÚMERO DE GACETA'

          ]);

          $sheet->setWidth([
                        'A'     =>  50,
                        'B'     =>  50,
                        'C'     =>  50,
                        'D'     =>  130,
                        'E'     =>  50,
                        'F'     =>  70,
                        'G'     =>  70,
                        'H'     =>  50,
                        'I'     =>  50,
                      
                    ]);

          $sheet->setHeight(1, 35);
                    $sheet->cell('A1:I4000',function($cell) 
                    {
                        $cell->setAlignment('center');    
                                           
                    });

          /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
        foreach($basicos  as $index => $basicos) 
        {
                /*====GRUPO====*/
                if($basicos->grupo == '1')
                {
                  $grupo = 'J-'; 
                }
                elseif($basicos->grupo == '2')
                {
                  $grupo = 'V-'; 

                }else{
                  
                  $grupo = 'G-';
                }

                /*======SIGLAS=====*/
                if($basicos->siglas == '1')
                {
                  $siglas = 'N';

                }else{
                  
                 $siglas=$basicos->siglas;
                }

                /*====RAZÓN SOCIAL=====*/
                if($basicos->razonSocial == '1')
                {
                  $razonSocial = 'xxx';

                }else{
                  
                 $razonSocial=$basicos->razonSocial;
                }

                /*====CORREO=====*/
                if($basicos->correEnte == '1')
                {
                  $correEnte = 'xxx';

                }else{
                  
                 $correEnte=$basicos->correEnte;
                }

                /*===FECHA GACETA===*/
                if($basicos->feGaceta == '1111-11-11')
                {
                  $feGaceta = '11111111';

                }else{
                 $feGaceta = $basicos->feGaceta;
                 $feGaceta = date("dmY", strtotime($feGaceta));
              
                }

                /*====NÚMERO DE GACETA=====*/
                if($basicos->numGaceta == '1')
                {
                  $numGaceta = 'xxx';

                }else{
                  
                 $numGaceta=$basicos->numGaceta;
                }


            $sheet->row($index+2, 
               [
                  $basicos->codSigecof, 
                  $siglas, 
                  $grupo ."". $basicos->rifProvee,
                  $razonSocial,
                  $basicos->telfEnte,
                  $basicos->direcWeb,
                  $correEnte,
                  $feGaceta,
                  $numGaceta,

               ]);
        }

      });
   
      })->export('xlsx');
    }
}
