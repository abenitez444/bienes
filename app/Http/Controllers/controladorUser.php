<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\freeipa\Freeipa;
use App\User;
use App\rol;
use App\modeloBitacora;
use Illuminate\Support\Facades\Crypt;

class controladorUser extends Controller
{
  public function index()
    {
        $roles = Rol::all();
        return view('Admin.agregarUser', compact('roles'));
    }

    public function searchUser(Request $request)
    {
       $ipa = Freeipa::Consultas();
        $r = $ipa->user()->findBy('employeenumber', $request->cedula);

        if ($r) {
            $user = User::where('name', $r[0]->uid[0])->get();

            if (sizeof($user) > 0)  {
                  return \Response::json(['message' => 'Usuario ya registrado'], 400);
                
            }else{
               return \Response::json($r);
            }
        }else{
             return \Response::json(['message' => 'Usuario no encontrado'], 400);
        }
        
    }

    public function validatePass(Request $request)
    {
        $user = $_SESSION['user'];
       
        $pass = Crypt::encrypt($request->pass);
        $auth = Freeipa::Login($user, $pass);

        if ($auth) {
            return \Response::json(200);
            
        }else{
             return \Response::json(['message' => 'Contraseña incorrecta'], 400);
        }
    }

    public function addUser(Request $request)
    {

        $user = new User;
        $user->name = $request->uid;
        $user->rol = $request->rol;

        if($user->save()){

          $bit = new modeloBitacora();
          $bit->user = $_SESSION['id'];
          $bit->accion  = 1;
          $bit->referencia = 'Usuario';
          $bit->save();

          return \Response::json(200);
       
        }

    }

}
