<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modeloAdjudicacion;
use App\sel_adjudicacion;
use App\modeloBitacora;


class controladorAdjudicacion extends Controller
{
     	
	public function index()
	{
     	$infoSelect=sel_adjudicacion::all();

      $lastCod = modeloAdjudicacion::select('codOt2_8')->get()->last();

        $arrayT28 = array(
            array("codOt2_8","Código de Origen:","Introduzca número consecutivo. Ej: I-2; I-3;","12","col-md-pull-4"),
            array("nomProan","Nombre del Propietario Anterior:","Introduzca nombre del propietario anterior","100","col-md-push-0"),
            array("nomBen","Nombre del Beneficiario:","Introduzca nombre del beneficiario","100",""),
            array("nomAuto","Nombre de la Autoridad:","Introduzca nombre de la autoridad","100",""),
            array("numSeAdm","Número de Sentencia Administrativo:","Introduzca el número de sentencia o acto administrativo","30",""),
            array("nomRegn","Nombre del Registro o Notaría:","Introduzca nombre del registro o notaría","100",""),
            array("tomo","Tomo:","Introduzca el tomo del registro","20",""),
            array("folio","Folio:","Introduzca el N° de folio","6",""),
            );


        $selectT28 = array(
            array("codAdq","Código de Adquisición:","col-md-push-4"),
            );

        $dateT28= array(
            array("feSeAdm","Fecha de Sentencia o Acto:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
            );

        $date2T28= array(
            array("feReg","Fecha de Registro:","¡Si se desconoce, deje el campo en blanco!","input-group","input-group-addon","inputGroupprimary3Status"),
            );

        return view('AnexosT.visAdjudicacion', compact('infoSelect','arrayT28','selectT28','dateT28','date2T28','lastCod'));
    }

    
    
    public function store(Request $request)
    {

        $form_t28= new modeloAdjudicacion();
        $form_t28->codAdq = $request->codAdq;

        if($form_t28->codOt2_8 =$request->codOt2_8 == ''){
          $form_t28->codOt2_8 = 'I-1';
          }else{
          $form_t28->codOt2_8 = $request->codOt2_8;  
        }

        if($form_t28->nomProan =$request->nomProan == ''){
          $form_t28->nomProan = '1';
          }else{
          $form_t28->nomProan = $request->nomProan;  
        }

        if($form_t28->nomBen = $request->nomBen == ''){
          $form_t28->nomBen = '1';
          }else{
          $form_t28->nomBen = $request->nomBen;  
        }

        if($form_t28->nomAuto = $request->nomAuto == ''){
          $form_t28->nomAuto = '1';
          }else{
          $form_t28->nomAuto = $request->nomAuto;  
        }

        if($form_t28->numSeAdm = $request->numSeAdm == ''){
          $form_t28->numSeAdm = '0';
          }else{
          $form_t28->numSeAdm = $request->numSeAdm;  
        }

        if($form_t28->nomRegn = $request->nomRegn == ''){
          $form_t28->nomRegn = '1';
          }else{
          $form_t28->nomRegn = $request->nomRegn;  
        }

        if($form_t28->tomo = $request->tomo == ''){
          $form_t28->tomo = '1';
          }else{
          $form_t28->tomo = $request->tomo;  
        }

        if($form_t28->folio = $request->folio == ''){
          $form_t28->folio = '0';
          }else{
          $form_t28->folio = $request->folio;  
        }

        if($form_t28->feSeAdm = $request->feSeAdm == ''){
          $form_t28->feSeAdm = '11111111';
          }else{
          $form_t28->feSeAdm = $request->feSeAdm;  
        }


        if($form_t28->feReg = $request->feReg == ''){
          $form_t28->feReg = '11111111';
          }else{
          $form_t28->feReg = $request->feReg;  
        }

        if($form_t28->save()){
            $bit = new modeloBitacora();
            $bit->user = $_SESSION['id'];
            $bit->accion  = 1;
            $bit->referencia = 'Adjudicación';
            $bit->save();

          }

          return back()->with('msj', 'Datos Registrados Exitosamente');
            
    }

  
    public function edit($id)
    {
        $form_t28 = modeloAdjudicacion::find($id);
        $infoSelect = sel_adjudicacion::all();

        return view('layouts.ModificarAnexosT.modificarAdjudicacion', compact('form_t28','infoSelect'));
    }

  
    public function update(Request $request, $id)
    {
        $form_t28=modeloAdjudicacion::find($id);
        $form_t28->codOt2_8 = $request->codOt2_8;
        $form_t28->codAdq = $request->codAdq;
       
        if($form_t28->nomProan =$request->nomProan == ''){
          $form_t28->nomProan = '1';
          }else{
          $form_t28->nomProan = $request->nomProan;  
        }

        if($form_t28->nomBen = $request->nomBen == ''){
          $form_t28->nomBen = '1';
          }else{
          $form_t28->nomBen = $request->nomBen;  
        }

        if($form_t28->nomAuto = $request->nomAuto == ''){
          $form_t28->nomAuto = '1';
          }else{
          $form_t28->nomAuto = $request->nomAuto;  
        }

        if($form_t28->numSeAdm = $request->numSeAdm == ''){
          $form_t28->numSeAdm = '0';
          }else{
          $form_t28->numSeAdm = $request->numSeAdm;  
        }

        if($form_t28->nomRegn = $request->nomRegn == ''){
          $form_t28->nomRegn = '1';
          }else{
          $form_t28->nomRegn = $request->nomRegn;  
        }

        if($form_t28->tomo = $request->tomo == ''){
          $form_t28->tomo = '1';
          }else{
          $form_t28->tomo = $request->tomo;  
        }

        if($form_t28->folio = $request->folio == ''){
          $form_t28->folio = '0';
          }else{
          $form_t28->folio = $request->folio;  
        }

        if($form_t28->feSeAdm = $request->feSeAdm == ''){
          $form_t28->feSeAdm = '11111111';
          }else{
          $form_t28->feSeAdm = $request->feSeAdm;  
        }


        if($form_t28->feReg = $request->feReg == ''){
          $form_t28->feReg = '11111111';
          }else{
          $form_t28->feReg = $request->feReg;  
        }

        if($form_t28->save()){

        	$bit = new modeloBitacora();
            $bit->user = $_SESSION['id'];
            $bit->accion  = 2;
            $bit->referencia = 'Adjudicación';
            $bit->save();

            return back()->with('msj', 'Datos Modificados Exitosamente');
             }else {
            return back()->with('errormsj', 'Los datos no se guardaron');
        }
    }

    public function exportAdjud()
    {
       \Excel::create('adjudicacion', function($excel) {
    
      $adjudicacion = modeloAdjudicacion::all();
      $relacion = sel_adjudicacion::all();
      
      
      $excel->sheet('adjudicacion', function($sheet) use($adjudicacion) {
 
      /*========CABECERA DE LA FILA N° 1========*/

     
      $sheet->row(1, [
          'CÓDIGO DEL ORIGEN','CÓDIGO DE LA FORMA DE ADQUISICIÓN ','NOMBRE DEL PROPIETARIO ANTERIOR', 'NOMBRE DEL BENEFICIARIO ','NOMBRE DE LA AUTORIDAD','NÚMERO DE SENTENCIA ADMINISTRATIVO','NOMBRE DEL REGISTRO O NOTARÍA','TOMO','FOLIO','FECHA DE SENTENCIA O ACTO','FECHA DE REGISTRO'


      ]);

      $sheet->setWidth([
                    'A'     =>  30,
                    'B'     =>  35,
                    'C'     =>  35,
                    'D'     =>  35,
                    'E'     =>  35,
                    'F'     =>  43,
                    'G'     =>  43,
                    'H'     =>  25,
                    'I'     =>  25,
                    'J'     =>  30,
                    'K'     =>  30,
                    
                ]);

                $sheet->setHeight(1, 35);
                $sheet->cell('A1:K6000',function($cell) 
                {
                    $cell->setAlignment('center');                
                    
                });
                
      /*========CUERPO DE LA FILA N° 2 HASTA N...========*/
     foreach($adjudicacion  as $index => $adjudicacion) {
            
           /*========CÓDIGO DE ADQUISICIÓN=======*/
          
            if($adjudicacion->codAdq == '1')
            {
              $codAdq = '10';

            }

            /*====NOMBRE DE DEL PROPIETARIO ANTERIOR====*/
            
            if($adjudicacion->nomProan == '1')
            {
              $nomProan = 'xxx';

            }else{

              $nomProan=$adjudicacion->nomProan;
            }

            /*====NOMBRE DEL BENEFICIARIO====*/
            
            if($adjudicacion->nomBen == '1')
            {
              $nomBen = 'noaplica';

            }else{

              $nomBen=$adjudicacion->nomBen;
            }


            /*====NOMBRE DE LA AUTORIDAD====*/
            
            if($adjudicacion->nomAuto == '0')
            {
              $nomAuto = 'xxx';

            }else{

              $nomAuto=$adjudicacion->nomAuto;
            }

            /*====NÚMERO DE SENTENCIA ADMINISTRATIVO====*/

            if($adjudicacion->numSeAdm == '1')
            {
              $numSeAdm = 'xxx';

            }else{

              $numSeAdm=$adjudicacion->numSeAdm;
            }


            /*====NOMBRE DEL REGISTRO O NOTARÍA====*/

            if($adjudicacion->nomRegn == '1')
            {
              $nomRegn = 'xxx';

            }else{

              $nomRegn=$adjudicacion->nomRegn;
            }

            /*====TOMO====*/

            if($adjudicacion->tomo == '1')
            {
              $tomo = 'xxx';

            }else{

              $tomo=$adjudicacion->tomo;
            }

            /*====FOLIO====*/

            if($adjudicacion->folio == '0')
            {
              $folio = '99';

            }else{

              $folio=$adjudicacion->folio;
            }
        

        
            /*========FECHA DE SENTENCIA O ACTO=======*/

            if($adjudicacion->feSeAdm == '1111-11-11')
            {
              $feSeAdm = '11111111';

            }else{

              $feSeAdm = $adjudicacion->feSeAdm;
              $feSeAdm = date("dmY", strtotime($feSeAdm));
      
            }
                        
            /*========FECHA DE REGISTRO=======*/

            if($adjudicacion->feReg == '1111-11-11')
            {
              $feReg = '11111111';

            }else{

              $feReg = $adjudicacion->feReg;
              $feReg = date("dmY", strtotime($feReg));
             
            }

          $sheet->row($index+2, [
              $adjudicacion->codOt2_8, 
              $codAdq, 
              $nomProan, 
              $nomBen, 
              $nomAuto, 
              $numSeAdm, 
              $nomRegn, 
              $tomo, 
              $folio, 
              $feSeAdm, 
              $feReg, 

          ]);

        }


      });
   
      })->export('xlsx');
    }

}
