<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class sel_unidades extends Model
{
 use SoftDeletes;

    protected $table = 'sel_unidades';
    protected $dates = ['deleted_at'];
    protected $fillable = ['codigo','unidad'];

    public function selectUnidad()
    {
        return $this->hasOne('App\modeloResponsables', 'codUnidad');
    }

    public function selectUnidadtran()
    {
        return $this->hasOne('App\modeloEqtransporte', 'codUnidad');
    }

    public function selectUnidadsemo()
    {
        return $this->hasOne('App\modeloSemovientes', 'codUnidad');
    }

    public function selectUnidadinmu()
    {
        return $this->hasOne('App\sel_unidades', 'codUnidad');
    }

}
