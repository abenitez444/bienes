<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modeloUser extends Model
{
    protected $table = 'users';
    protected $fillable = ['name', 'rol','registrar','modificar','eliminar'];
}
