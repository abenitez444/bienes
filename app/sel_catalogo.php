<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sel_catalogo extends Model
{
    protected $table = 'sel_catalogo';
    protected $fillable = ['codigo', 'descripcion'];


      public function selectCatalogo()
    {
        return $this->hasOne('App\modeloBienes', 'codigo');
    }

    public function selectCatalogotran()
    {
        return $this->hasOne('App\sel_catalogo', 'codCata');
    }

    public function selectCatalogosem()
    {
        return $this->hasOne('App\sel_catalogo', 'codCata');
    }

    public function selectCatalogoinm()
    {
        return $this->hasOne('App\sel_catalogo', 'codCata');
    }
}

