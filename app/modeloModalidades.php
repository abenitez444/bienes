<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modeloModalidades extends Model
{
    protected $table = 'cont_origen';

    protected $fillable = ['modalidades','cont'];
}
