<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sel_categoria extends Model
{
    protected $table = 'sel_categoria';
    protected $fillable = ['categoria'];


      public function selectCategoria()
    {
        return $this->hasOne('App\modeloS5', 'categoria');
    }
}
