<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class modeloSemovientes extends Model
{
use SoftDeletes;

    protected $table = 'semovientes';
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['codBien','codCata','codUnidad','sedeOrgano','codRespAdm','codResBien','codInterno','estatuBien','espOtroUso','valorAdq','moneda','espeMoneda','feAdqBien','feIngBien','edoBien','espOtroEdo','descEdoBien','raza','genero','tipoAnimal','espeOtroTipo','proposito','espeOtroPro','codColorBien','espeColor','otraEspeColor','peso','unidadPeso','feNacimiento','numHierro','seParticulares','otrasEspecifi','seguroBien','codRegSeguro'];

    public static function selectRespAdm($id){
         return modeloResponsables::where('codUnidad', $id)->get();
    }
    
    public function selectResponsables()
    {
        return $this->belongsTo('App\modeloResponsables', 'codRespAdm');
    }

    public function ResponsableDirectosemo()
    {
        return $this->belongsTo('App\modeloResponsables', 'codResBien');
    }

     public function selectUnidadsemo()
    {
        return $this->belongsTo('App\modeloS5', 'codUnidad');
    }

    public function selectCatalogosem()
    {
        return $this->belongsTo('App\sel_catalogo', 'codCata');
    }

     public function selectOrganosemo()
    {
        return $this->belongsTo('App\modeloS4', 'sedeOrgano');
    }

    public function selectEstatusemo()
    {
        return $this->belongsTo('App\sel_estatusbien', 'estatuBien');
    }

    public function selectSegurosemo()
    {
        return $this->belongsTo('App\sel_seguros2', 'moneda');
    }

    public function selectAnimalsemo()
    {
        return $this->belongsTo('App\sel_tipoanimal', 'tipoAnimal');
    }

    public function selectPropositosemo()
    {
        return $this->belongsTo('App\sel_proposito', 'proposito');
    }

    public function selectCondicionsemo()
    {
        return $this->belongsTo('App\sel_condicionbien', 'edoBien');
    }

    public function selectGenerosemo()
    {
        return $this->belongsTo('App\sel_genero', 'genero');
    }

    public function selectColorbiensemo()
    {
        return $this->belongsTo('App\sel_colorbien', 'codColorBien');
    }

    public function selectPesosemo()
    {
        return $this->belongsTo('App\sel_medidapeso', 'unidadPeso');
    }

   public function selectSegurobiensemo()
    {
        return $this->belongsTo('App\sel_seguros3', 'seguroBien');
    }

}
