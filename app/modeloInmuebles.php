<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class modeloInmuebles extends Model
{
 use SoftDeletes;
    protected $table = 'datosinmuebles';
    protected $dates = ['deleted_at'];


     /*===========Registrar==============*/
    public static function paisInmueble(){
         return sel_paises::all();
     }

    public static function parroInmueble(){
         return sel_parroquias::all();
     }

    public static function ciudadInmueble(){
         return sel_ciudad::all();
     }

     /*===========Modificar==============*/
    public static function modiPais(){
         return sel_paises::all();
     }

    public static function modiParroquia(){
         return sel_parroquias::all();
     }

    public static function modiCiudad(){
         return sel_ciudad::all();
     }

    /*======================================*/

      /*=======SELECT DINAMICOS SEDES-DEPENDENCIA =========*/
     
    public static function selectResponinmu($id){
         return modeloResponsables::where('codUnidad', $id)->get();
    }

            /*=====================*/

    public function selectUnidadinmu()
    {
        return $this->belongsTo('App\modeloS5', 'codUnidad');
    }

    public function selectCatalogoinm()
    {
        return $this->belongsTo('App\sel_catalogo', 'codCata');
    }

    public function selectCorresinmu()
    {
        return $this->belongsTo('App\sel_seguros3', 'corresBien');
    }

    public function selectResponsableinmu()
    {
        return $this->belongsTo('App\modeloResponsables', 'codRespAdm')->withTrashed();
    }

    public function selectOrganoinmu()
    {
        return $this->belongsTo('App\modeloS4', 'sedeOrgano');
    }

    public function selectEstatuinmu()
    {
        return $this->belongsTo('App\sel_estatusbien', 'estatuBien');
    }

    public function selectLocalinmu()
    {
        return $this->belongsTo('App\sel_proveedores', 'localizacion');
    }

    public function selectPaisesinmu()
    {
        return $this->belongsTo('App\sel_paises', 'codPais');
    }

    public function selectParroinmu()
    {
        return $this->belongsTo('App\sel_parroquias', 'codParroquia');
    }

    public function selectCiudadinmu()
    {
        return $this->belongsTo('App\sel_ciudad', 'codCiudad');
    }

    public function selectMonedainmu()
    {
        return $this->belongsTo('App\sel_seguros2', 'moneda');
    }

    public function selectCondicioninmu()
    {
        return $this->belongsTo('App\sel_condicionbien', 'edoBien');
    }

    public function selectUsoninmu()
    {
        return $this->belongsTo('App\sel_usos', 'usoBienInmu');
    }

    public function selectContrucinmu()
    {
        return $this->belongsTo('App\sel_medidapeso', 'unidadConstru');
    }

    public function selectTerrenoinmu()
    {
        return $this->belongsTo('App\sel_medidapeso', 'unidadTerreno');
    }

    public function selectSeguroinmu()
    {
        return $this->belongsTo('App\sel_seguros3', 'seguroBien');
    }

    
}
