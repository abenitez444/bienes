<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class modeloS5 extends Model
{
 use SoftDeletes;
    protected $table = 'unidades';
    protected $dates = ['deleted_at'];
    protected $fillable = ['codUnidad','descUnidad','categoria','espeCatego','codAdscrita'];

      public function selectUnidad()
    {
        return $this->hasMany('App\modeloResponsables', 'codUnidad');
    }

    public function selectUnidadbienes()
    {
        return $this->hasMany('App\modeloBienes', 'codUnidad');
    }

    public function selectResponsables()
    {
        return $this->hasMany('App\modeloResponsables', 'codUnidad');
    }

     public function selectUnidadsemo()
    {
        return $this->hasMany('App\modeloSemovientes', 'codUnidad');
    }

    public function selectUnidadtran()
    {
        return $this->hasMany('App\modeloEqtransporte', 'codUnidad');
    }

    public function selectUnidadinmu()
    {
        return $this->hasMany('App\modeloInmuebles', 'codUnidad');
    }

      public function selectCategoria()
    {
        return $this->belongsTo('App\sel_categoria', 'categoria');
    }
}
