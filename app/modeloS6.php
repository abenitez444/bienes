<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class modeloS6 extends Model
{
 use SoftDeletes;  
    protected $table = 'ubicacion'; 
    protected $dates = ['deleted_at'];
    protected $fillable = ['codSede','codUnidad'];


     public function selectSedeS6()
    {
        return $this->belongsTo('App\modeloS4', 'codSede');
    }

    public function selectUnidadS6()
    {
        return $this->belongsTo('App\modeloS5', 'codUnidad');
    }

}
